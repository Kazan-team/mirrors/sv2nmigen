# %{
# /*
#  * Copyright (c) 1998-2017 Stephen Williams (steve@icarus.com)
#  * Copyright CERN 2012-2013 / Stephen Williams (steve@icarus.com)
#  *
#  *    This source code is free software; you can redistribute it
#  *    and/or modify it in source code form under the terms of the GNU
#  *    General Public License as published by the Free Software
#  *    Foundation; either version 2 of the License, or (at your option)
#  *    any later version.
#  *
#  *    This program is distributed in the hope that it will be useful,
#  *    but WITHOUT ANY WARRANTY; without even the implied warranty of
#  *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  *    GNU General Public License for more details.
#  *
#  *    You should have received a copy of the GNU General Public License
#  *    along with this program; if not, write to the Free Software
#  *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#  */

import lexor
from ply import yacc, lex
from lib2to3.pytree import Node, Leaf
from lib2to3.pgen2 import token
from lib2to3.pygram import python_symbols as syms

yacc1_debug = 0
yacc2_debug = 0
parse_debug = 1


#from parse_tokens import tokens
tokens = lexor.tokens  # list(set(lexor.tokens).union(set(tokens)))
literals = lexor.literals

precedence = [
    ('right', 'K_PLUS_EQ', 'K_MINUS_EQ', 'K_MUL_EQ', 'K_DIV_EQ',
              'K_MOD_EQ', 'K_AND_EQ', 'K_OR_EQ'),
    ('right', 'K_XOR_EQ', 'K_LS_EQ', 'K_RS_EQ', 'K_RSS_EQ'),
    ('right', '?', ':', 'K_inside'),
    ('left', 'K_LOR'),
    ('left', 'K_LAND'),
    ('left', '|'),
    ('left', '^', 'K_NXOR', 'K_NOR'),
    ('left', '&', 'K_NAND'),
    ('left', 'K_EQ', 'K_NE', 'K_CEQ', 'K_CNE', 'K_WEQ', 'K_WNE'),
    ('left', 'K_GE', 'K_LE', '<', '>'),
    ('left', 'K_LS', 'K_RS', 'K_RSS'),
    ('left', '+', '-'),
    ('left', '*', '/', '%'),
    ('left', 'K_POW'),
    ('left', 'UNARY_PREC'),
    ('nonassoc', 'less_than_K_else'),
    ('nonassoc', 'K_else'),
    ('nonassoc', '('),
    ('nonassoc', 'K_exclude'),
    ('nonassoc', 'no_timeunits_declaration'),
    ('nonassoc', 'one_timeunits_declaration'),
    ('nonassoc', 'K_timeunit', 'K_timeprecision')
]


IVL_VT_NO_TYPE = 'VT_NO_TYPE'
IVL_VT_BOOL = 'VT_BOOL'
IVL_VT_LOGIC = 'VT_LOGIC'
"""
      IVL_VT_VOID    = 0,  /* Not used */
      IVL_VT_NO_TYPE = 1,  /* Place holder for missing/unknown type. */
      IVL_VT_REAL    = 2,
      IVL_VT_BOOL    = 3,
      IVL_VT_LOGIC   = 4,
      IVL_VT_STRING  = 5,
      IVL_VT_DARRAY  = 6,  /* Array (esp. dynamic array) */
      IVL_VT_CLASS   = 7,  /* SystemVerilog class instances */
      IVL_VT_QUEUE   = 8,  /* SystemVerilog queue instances */
      IVL_VT_VECTOR = IVL_VT_LOGIC /* For compatibility */
"""

NN_NONE = 'NONE'
NN_IMPLICIT = 'IMPLICIT'
NN_IMPLICIT_REG = 'IMPLICIT_REG'
NN_INTEGER = 'INTEGER'
NN_WIRE = 'WIRE'
NN_TRI = 'TRI'
NN_TRI1 = 'TRI1'
NN_SUPPLY0 = 'SUPPLY0'
NN_SUPPLY1 = 'SUPPLY1'
NN_WAND = 'WAND'
NN_TRIAND = 'TRIAND'
NN_TRI0 = 'TRI0'
NN_WOR = 'WOR'
NN_TRIOR = 'TRIOR'
NN_REG = 'REG'
NN_UNRESOLVED_WIRE = 'UNRESOLVED_WIRE'

NP_NOT_A_PORT = 'NOT_A_PORT'
NP_PIMPLICIT = 'PIMPLICIT'
NP_PINPUT = 'PINPUT'
NP_POUTPUT = 'POUTPUT'
NP_PINOUT = 'PINOUT'
NP_PREF = 'PREF'


class DataType:
    def __init__(self, typ, signed):
        self.typ = typ
        self.signed = signed


class StatementList:
    def __init__(self):
        self.statements = []

    def add_statement(self, s):
        self.statements += [s]


# -------------- RULES ----------------
()


def p_source_text_1(p):
    '''source_text : timeunits_declaration_opt _embed0_source_text description_list '''
    if(parse_debug > 2):
        print('source_text', list(p))


()


def p_source_text_2(p):
    '''source_text :  '''
    if(parse_debug):
        print('source_text', list(p))


()


def p__embed0_source_text(p):
    '''_embed0_source_text : '''


    # { pform_set_scope_timescale(yyloc); }
()


def p_assertion_item_1(p):
    '''assertion_item : concurrent_assertion_item '''
    if(parse_debug):
        print('assertion_item_1', list(p))


()


def p_assignment_pattern_1(p):
    '''assignment_pattern : K_LP expression_list_proper '}' '''
    if(parse_debug):
        print('assignment_pattern_1', list(p))


    # { PEAssignPattern*tmp = new PEAssignPattern(*p[2]);
    # 	FILE_NAME(tmp, @1);
    # 	delete p[2];
    # 	p[0] = tmp;
    #       }
()


def p_assignment_pattern_2(p):
    '''assignment_pattern : K_LP '}' '''
    if(parse_debug):
        print('assignment_pattern_2', list(p))


    # { PEAssignPattern*tmp = new PEAssignPattern;
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_block_identifier_opt_1(p):
    '''block_identifier_opt : IDENTIFIER ':' '''
    if(parse_debug):
        print('block_identifier_opt_1', list(p))


()


def p_block_identifier_opt_2(p):
    '''block_identifier_opt :  '''
    if(parse_debug):
        print('block_identifier_opt_2', list(p))


()


def p_class_declaration_1(p):
    '''class_declaration : K_virtual_opt K_class lifetime_opt class_identifier class_declaration_extends_opt ';' _embed0_class_declaration class_items_opt K_endclass _embed1_class_declaration class_declaration_endlabel_opt '''
    if(parse_debug):
        print('class_declaration_1', list(p))


    # { // Wrap up the class.
    # 	if (p[11] && p[4] && p[4]->name != p[11]) {
    # 	      yyerror(@11, "error: Class end label doesn't match class name.");
    # 	      delete[]p[11];
    # 	}
    #       }
()


def p__embed0_class_declaration(p):
    '''_embed0_class_declaration : '''


    # { pform_start_class_declaration(@2, p[4], p[5].type, p[5].exprs, p[3]); }
()


def p__embed1_class_declaration(p):
    '''_embed1_class_declaration : '''


    # { // Process a class.
    # 	pform_end_class_declaration(@9);
    #       }
()


def p_class_constraint_1(p):
    '''class_constraint : constraint_prototype '''
    if(parse_debug):
        print('class_constraint_1', list(p))


()


def p_class_constraint_2(p):
    '''class_constraint : constraint_declaration '''
    if(parse_debug):
        print('class_constraint_2', list(p))


()


def p_class_identifier_1(p):
    '''class_identifier : IDENTIFIER '''
    if(parse_debug):
        print('class_identifier_1', list(p))


    # { // Create a synthetic typedef for the class name so that the
    # 	// lexor detects the name as a type.
    # 	perm_string name = lex_strings.make(p[1]);
    # 	class_type_t*tmp = new class_type_t(name);
    # 	FILE_NAME(tmp, @1);
    # 	pform_set_typedef(name, tmp, NULL);
    # 	delete[]p[1];
    # 	p[0] = tmp;
    #       }
()


def p_class_identifier_2(p):
    '''class_identifier : TYPE_IDENTIFIER '''
    if(parse_debug):
        print('class_identifier_2', list(p))


    # { class_type_t*tmp = dynamic_cast<class_type_t*>(p[1].type);
    # 	if (tmp == 0) {
    # 	      yyerror(@1, "Type name \"%s\"is not a predeclared class name.", p[1].text);
    # 	}
    # 	delete[]p[1].text;
    # 	p[0] = tmp;
    #       }
()


def p_class_declaration_endlabel_opt_1(p):
    '''class_declaration_endlabel_opt : ':' TYPE_IDENTIFIER '''
    if(parse_debug):
        print('class_declaration_endlabel_opt_1', list(p))


    # { class_type_t*tmp = dynamic_cast<class_type_t*> (p[2].type);
    # 	if (tmp == 0) {
    # 	      yyerror(@2, "error: class declaration endlabel \"%s\" is not a class name\n", p[2].text);
    # 	      p[0] = None
    # 	} else {
    # 	      p[0] = strdupnew(tmp->name.str());
    # 	}
    # 	delete[]p[2].text;
    #       }
()


def p_class_declaration_endlabel_opt_2(p):
    '''class_declaration_endlabel_opt : ':' IDENTIFIER '''
    if(parse_debug):
        print('class_declaration_endlabel_opt_2', list(p))
    p[0] = p[2]


()


def p_class_declaration_endlabel_opt_3(p):
    '''class_declaration_endlabel_opt :  '''
    if(parse_debug):
        print('class_declaration_endlabel_opt_3', list(p))


    # { p[0] = None }
()


def p_class_declaration_extends_opt_1(p):
    '''class_declaration_extends_opt : K_extends TYPE_IDENTIFIER '''
    if(parse_debug):
        print('class_declaration_extends_opt_1', list(p))


    # { p[0].type = p[2].type;
    # 	p[0].exprs= 0;
    # 	delete[]p[2].text;
    #       }
()


def p_class_declaration_extends_opt_2(p):
    '''class_declaration_extends_opt : K_extends TYPE_IDENTIFIER '(' expression_list_with_nuls ')' '''
    if(parse_debug):
        print('class_declaration_extends_opt_2', list(p))


    # { p[0].type  = p[2].type;
    # 	p[0].exprs = p[4];
    # 	delete[]p[2].text;
    #       }
()


def p_class_declaration_extends_opt_3(p):
    '''class_declaration_extends_opt :  '''
    if(parse_debug):
        print('class_declaration_extends_opt_3', list(p))


    # { p[0].type = 0; p[0].exprs = 0; }
()


def p_class_items_opt_1(p):
    '''class_items_opt : class_items '''
    if(parse_debug):
        print('class_items_opt_1', list(p))


()


def p_class_items_opt_2(p):
    '''class_items_opt :  '''
    if(parse_debug):
        print('class_items_opt_2', list(p))


()


def p_class_items_1(p):
    '''class_items : class_items class_item '''
    if(parse_debug):
        print('class_items_1', list(p))


()


def p_class_items_2(p):
    '''class_items : class_item '''
    if(parse_debug):
        print('class_items_2', list(p))


()


def p_class_item_1(p):
    '''class_item : method_qualifier_opt K_function K_new _embed0_class_item '(' tf_port_list_opt ')' ';' function_item_list_opt statement_or_null_list_opt K_endfunction endnew_opt '''
    if(parse_debug):
        print('class_item_1', list(p))


    # { current_function->set_ports(p[6]);
    # 	pform_set_constructor_return(current_function);
    # 	pform_set_this_class(@3, current_function);
    # 	current_function_set_statement(@3, p[10]);
    # 	pform_pop_scope();
    # 	current_function = 0;
    #       }
()


def p_class_item_2(p):
    '''class_item : property_qualifier_opt data_type list_of_variable_decl_assignments ';' '''
    if(parse_debug):
        print('class_item_2', list(p))


    # { pform_class_property(@2, p[1], p[2], p[3]); }
()


def p_class_item_3(p):
    '''class_item : K_const class_item_qualifier_opt data_type list_of_variable_decl_assignments ';' '''
    if(parse_debug):
        print('class_item_3', list(p))


    # { pform_class_property(@1, p[2] | property_qualifier_t::make_const(), p[3], p[4]); }
()


def p_class_item_4(p):
    '''class_item : method_qualifier_opt task_declaration '''
    if(parse_debug):
        print('class_item_4', list(p))


    # { /* The task_declaration rule puts this into the class */ }
()


def p_class_item_5(p):
    '''class_item : method_qualifier_opt function_declaration '''
    if(parse_debug):
        print('class_item_5', list(p))


    # { /* The function_declaration rule puts this into the class */ }
()


def p_class_item_6(p):
    '''class_item : K_extern method_qualifier_opt K_function K_new ';' '''
    if(parse_debug):
        print('class_item_6', list(p))


    # { yyerror(@1, "sorry: External constructors are not yet supported."); }
()


def p_class_item_7(p):
    '''class_item : K_extern method_qualifier_opt K_function K_new '(' tf_port_list_opt ')' ';' '''
    if(parse_debug):
        print('class_item_7', list(p))


    # { yyerror(@1, "sorry: External constructors are not yet supported."); }
()


def p_class_item_8(p):
    '''class_item : K_extern method_qualifier_opt K_function data_type_or_implicit_or_void IDENTIFIER ';' '''
    if(parse_debug):
        print('class_item_8', list(p))


    # { yyerror(@1, "sorry: External methods are not yet supported.");
    # 	delete[] p[5];
    #       }
()


def p_class_item_9(p):
    '''class_item : K_extern method_qualifier_opt K_function data_type_or_implicit_or_void IDENTIFIER '(' tf_port_list_opt ')' ';' '''
    if(parse_debug):
        print('class_item_9', list(p))


    # { yyerror(@1, "sorry: External methods are not yet supported.");
    # 	delete[] p[5];
    #       }
()


def p_class_item_10(p):
    '''class_item : K_extern method_qualifier_opt K_task IDENTIFIER ';' '''
    if(parse_debug):
        print('class_item_10', list(p))


    # { yyerror(@1, "sorry: External methods are not yet supported.");
    # 	delete[] p[4];
    #       }
()


def p_class_item_11(p):
    '''class_item : K_extern method_qualifier_opt K_task IDENTIFIER '(' tf_port_list_opt ')' ';' '''
    if(parse_debug):
        print('class_item_11', list(p))


    # { yyerror(@1, "sorry: External methods are not yet supported.");
    # 	delete[] p[4];
    #       }
()


def p_class_item_12(p):
    '''class_item : class_constraint '''
    if(parse_debug):
        print('class_item_12', list(p))


()


def p_class_item_13(p):
    '''class_item : property_qualifier_opt data_type error ';' '''
    if(parse_debug):
        print('class_item_13', list(p))


    # { yyerror(@3, "error: Errors in variable names after data type.");
    # 	yyerrok;
    #       }
()


def p_class_item_14(p):
    '''class_item : property_qualifier_opt IDENTIFIER error ';' '''
    if(parse_debug):
        print('class_item_14', list(p))


    # { yyerror(@3, "error: %s doesn't name a type.", p[2]);
    # 	yyerrok;
    #       }
()


def p_class_item_15(p):
    '''class_item : method_qualifier_opt K_function K_new error K_endfunction endnew_opt '''
    if(parse_debug):
        print('class_item_15', list(p))


    # { yyerror(@1, "error: I give up on this class constructor declaration.");
    # 	yyerrok;
    #       }
()


def p_class_item_16(p):
    '''class_item : error ';' '''
    if(parse_debug):
        print('class_item_16', list(p))


    # { yyerror(@2, "error: invalid class item.");
    # 	yyerrok;
    #       }
()


def p__embed0_class_item(p):
    '''_embed0_class_item : '''


    # { assert(current_function==0);
    # 	current_function = pform_push_constructor_scope(@3);
    #       }
()


def p_class_item_qualifier_1(p):
    '''class_item_qualifier : K_static '''
    if(parse_debug):
        print('class_item_qualifier_1', list(p))


    # { p[0] = property_qualifier_t::make_static(); }
()


def p_class_item_qualifier_2(p):
    '''class_item_qualifier : K_protected '''
    if(parse_debug):
        print('class_item_qualifier_2', list(p))


    # { p[0] = property_qualifier_t::make_protected(); }
()


def p_class_item_qualifier_3(p):
    '''class_item_qualifier : K_local '''
    if(parse_debug):
        print('class_item_qualifier_3', list(p))


    # { p[0] = property_qualifier_t::make_local(); }
()


def p_class_item_qualifier_list_1(p):
    '''class_item_qualifier_list : class_item_qualifier_list class_item_qualifier '''
    if(parse_debug):
        print('class_item_qualifier_list_1', list(p))


    # { p[0] = p[1] | p[2]; }
()


def p_class_item_qualifier_list_2(p):
    '''class_item_qualifier_list : class_item_qualifier '''
    if(parse_debug):
        print('class_item_qualifier_list_2', list(p))
    p[0] = p[1]


()


def p_class_item_qualifier_opt_1(p):
    '''class_item_qualifier_opt : class_item_qualifier_list '''
    if(parse_debug):
        print('class_item_qualifier_opt_1', list(p))
    p[0] = p[1]


()


def p_class_item_qualifier_opt_2(p):
    '''class_item_qualifier_opt :  '''
    if(parse_debug):
        print('class_item_qualifier_opt_2', list(p))


    # { p[0] = property_qualifier_t::make_none(); }
()


def p_class_new_1(p):
    '''class_new : K_new '(' expression_list_with_nuls ')' '''
    if(parse_debug):
        print('class_new_1', list(p))


    # { list<PExpr*>*expr_list = p[3];
    # 	strip_tail_items(expr_list);
    # 	PENewClass*tmp = new PENewClass(*expr_list);
    # 	FILE_NAME(tmp, @1);
    # 	delete p[3];
    # 	p[0] = tmp;
    #       }
()


def p_class_new_2(p):
    '''class_new : K_new hierarchy_identifier '''
    if(parse_debug):
        print('class_new_2', list(p))


    # { PEIdent*tmpi = new PEIdent(*p[2]);
    # 	FILE_NAME(tmpi, @2);
    # 	PENewCopy*tmp = new PENewCopy(tmpi);
    # 	FILE_NAME(tmp, @1);
    # 	delete p[2];
    # 	p[0] = tmp;
    #       }
()


def p_class_new_3(p):
    '''class_new : K_new '''
    if(parse_debug):
        print('class_new_3', list(p))


    # { PENewClass*tmp = new PENewClass;
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_concurrent_assertion_item_1(p):
    '''concurrent_assertion_item : block_identifier_opt K_assert K_property '(' property_spec ')' statement_or_null '''
    if(parse_debug):
        print('concurrent_assertion_item_1', list(p))


    # { /* */
    # 	if (gn_assertions_flag) {
    # 	      yyerror(@2, "sorry: concurrent_assertion_item not supported."
    # 		      " Try -gno-assertion to turn this message off.");
    # 	}
    #       }
()


def p_concurrent_assertion_item_2(p):
    '''concurrent_assertion_item : block_identifier_opt K_assert K_property '(' error ')' statement_or_null '''
    if(parse_debug):
        print('concurrent_assertion_item_2', list(p))


    # { yyerrok;
    #         yyerror(@2, "error: Error in property_spec of concurrent assertion item.");
    #       }
()


def p_constraint_block_item_1(p):
    '''constraint_block_item : constraint_expression '''
    if(parse_debug):
        print('constraint_block_item_1', list(p))


()


def p_constraint_block_item_list_1(p):
    '''constraint_block_item_list : constraint_block_item_list constraint_block_item '''
    if(parse_debug):
        print('constraint_block_item_list_1', list(p))


()


def p_constraint_block_item_list_2(p):
    '''constraint_block_item_list : constraint_block_item '''
    if(parse_debug):
        print('constraint_block_item_list_2', list(p))


()


def p_constraint_block_item_list_opt_1(p):
    '''constraint_block_item_list_opt :  '''
    if(parse_debug):
        print('constraint_block_item_list_opt_1', list(p))


()


def p_constraint_block_item_list_opt_2(p):
    '''constraint_block_item_list_opt : constraint_block_item_list '''
    if(parse_debug):
        print('constraint_block_item_list_opt_2', list(p))


()


def p_constraint_declaration_1(p):
    '''constraint_declaration : K_static_opt K_constraint IDENTIFIER '{' constraint_block_item_list_opt '}' '''
    if(parse_debug):
        print('constraint_declaration_1', list(p))


    # { yyerror(@2, "sorry: Constraint declarations not supported."); }
()


def p_constraint_declaration_2(p):
    '''constraint_declaration : K_static_opt K_constraint IDENTIFIER '{' error '}' '''
    if(parse_debug):
        print('constraint_declaration_2', list(p))


    # { yyerror(@4, "error: Errors in the constraint block item list."); }
()


def p_constraint_expression_1(p):
    '''constraint_expression : expression ';' '''
    if(parse_debug):
        print('constraint_expression_1', list(p))


()


def p_constraint_expression_2(p):
    '''constraint_expression : expression K_dist '{' '}' ';' '''
    if(parse_debug):
        print('constraint_expression_2', list(p))


()


def p_constraint_expression_3(p):
    '''constraint_expression : expression K_TRIGGER constraint_set '''
    if(parse_debug):
        print('constraint_expression_3', list(p))


()


def p_constraint_expression_4(p):
    '''constraint_expression : K_if '(' expression ')' constraint_set %prec less_than_K_else '''
    if(parse_debug):
        print('constraint_expression_4', list(p))


()


def p_constraint_expression_5(p):
    '''constraint_expression : K_if '(' expression ')' constraint_set K_else constraint_set '''
    if(parse_debug):
        print('constraint_expression_5', list(p))


()


def p_constraint_expression_6(p):
    '''constraint_expression : K_foreach '(' IDENTIFIER '[' loop_variables ']' ')' constraint_set '''
    if(parse_debug):
        print('constraint_expression_6', list(p))


()


def p_constraint_expression_list_1(p):
    '''constraint_expression_list : constraint_expression_list constraint_expression '''
    if(parse_debug):
        print('constraint_expression_list_1', list(p))


()


def p_constraint_expression_list_2(p):
    '''constraint_expression_list : constraint_expression '''
    if(parse_debug):
        print('constraint_expression_list_2', list(p))


()


def p_constraint_prototype_1(p):
    '''constraint_prototype : K_static_opt K_constraint IDENTIFIER ';' '''
    if(parse_debug):
        print('constraint_prototype_1', list(p))


    # { yyerror(@2, "sorry: Constraint prototypes not supported."); }
()


def p_constraint_set_1(p):
    '''constraint_set : constraint_expression '''
    if(parse_debug):
        print('constraint_set_1', list(p))


()


def p_constraint_set_2(p):
    '''constraint_set : '{' constraint_expression_list '}' '''
    if(parse_debug):
        print('constraint_set_2', list(p))


()


def p_data_declaration_1(p):
    '''data_declaration : attribute_list_opt data_type_or_implicit list_of_variable_decl_assignments ';' '''
    if(parse_debug):
        print('data_declaration_1', list(p))


    # { data_type_t*data_type = p[2];
    # 	if (data_type == 0) {
    # 	      data_type = new vector_type_t(IVL_VT_LOGIC, false, 0);
    # 	      FILE_NAME(data_type, @2);
    # 	}
    # 	pform_makewire(@2, 0, str_strength, p[3], NetNet::IMPLICIT_REG, data_type);
    #       }
()


def p_data_type_1(p):
    '''data_type : integer_vector_type unsigned_signed_opt dimensions_opt '''
    if(parse_debug):
        print('data_type_1', list(p))
    use_vtype = p[1]
    reg_flag = False
    if (use_vtype == IVL_VT_NO_TYPE):
        use_vtype = IVL_VT_LOGIC
        reg_flag = True
    dt = DataType(use_vtype, signed=p[2])
    dt.dims = p[3]
    dt.reg_flag = reg_flag
    p[0] = dt


    # { ivl_variable_type_t use_vtype = p[1];
    # 	bool reg_flag = false;
    # 	if (use_vtype == IVL_VT_NO_TYPE) {
    # 	      use_vtype = IVL_VT_LOGIC;
    # 	      reg_flag = true;
    # 	}
    # 	vector_type_t*tmp = new vector_type_t(use_vtype, p[2], p[3]);
    # 	tmp->reg_flag = reg_flag;
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_data_type_2(p):
    '''data_type : non_integer_type '''
    if(parse_debug):
        print('data_type_2', list(p))
    p[0] = p[1]


    # { real_type_t*tmp = new real_type_t(p[1]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_data_type_3(p):
    '''data_type : struct_data_type '''
    if(parse_debug):
        print('data_type_3', list(p))
    p[0] = p[1]


    # { if (!p[1]->packed_flag) {
    # 	      yyerror(@1, "sorry: Unpacked structs not supported.");
    # 	}
    # 	p[0] = p[1];
    #       }
()


def p_data_type_4(p):
    '''data_type : enum_data_type '''
    if(parse_debug):
        print('data_type_4', list(p))
    p[0] = p[1]


()


def p_data_type_5(p):
    '''data_type : atom2_type signed_unsigned_opt '''
    if(parse_debug):
        print('data_type_5', list(p))


    # { atom2_type_t*tmp = new atom2_type_t(p[1], p[2]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_data_type_6(p):
    '''data_type : K_integer signed_unsigned_opt '''
    if(parse_debug):
        print('data_type_6', list(p))


    # { list<pform_range_t>*pd = make_range_from_width(integer_width);
    # 	vector_type_t*tmp = new vector_type_t(IVL_VT_LOGIC, p[2], pd);
    # 	tmp->reg_flag = true;
    # 	tmp->integer_flag = true;
    # 	p[0] = tmp;
    #       }
()


def p_data_type_7(p):
    '''data_type : K_time '''
    if(parse_debug):
        print('data_type_7', list(p))


    # { list<pform_range_t>*pd = make_range_from_width(64);
    # 	vector_type_t*tmp = new vector_type_t(IVL_VT_LOGIC, false, pd);
    # 	tmp->reg_flag = !gn_system_verilog();
    # 	p[0] = tmp;
    #       }
()


def p_data_type_8(p):
    '''data_type : TYPE_IDENTIFIER dimensions_opt '''
    if(parse_debug):
        print('data_type_8', list(p))


    # { if (p[2]) {
    # 	      parray_type_t*tmp = new parray_type_t(p[1].type, p[2]);
    # 	      FILE_NAME(tmp, @1);
    # 	      p[0] = tmp;
    # 	} else p[0] = p[1].type;
    # 	delete[]p[1].text;
    #       }
()


def p_data_type_9(p):
    '''data_type : PACKAGE_IDENTIFIER K_SCOPE_RES _embed0_data_type TYPE_IDENTIFIER '''
    if(parse_debug):
        print('data_type_9', list(p))


    # { lex_in_package_scope(0);
    # 	p[0] = p[4].type;
    # 	delete[]p[4].text;
    #       }
()


def p_data_type_10(p):
    '''data_type : K_string '''
    if(parse_debug):
        print('data_type_10', list(p))


    # { string_type_t*tmp = new string_type_t;
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p__embed0_data_type(p):
    '''_embed0_data_type : '''


    # { lex_in_package_scope(p[1]); }
()


def p_data_type_or_implicit_1(p):
    '''data_type_or_implicit : data_type '''
    if(parse_debug):
        print('data_type_or_implicit_1', list(p))
    p[0] = p[1]


()


def p_data_type_or_implicit_2(p):
    '''data_type_or_implicit : signing dimensions_opt '''
    if(parse_debug):
        print('data_type_or_implicit_2', list(p))


    # { vector_type_t*tmp = new vector_type_t(IVL_VT_LOGIC, p[1], p[2]);
    # 	tmp->implicit_flag = true;
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_data_type_or_implicit_3(p):
    '''data_type_or_implicit : dimensions '''
    if(parse_debug):
        print('data_type_or_implicit_3', list(p))
    p[0] = list(p)


    # { vector_type_t*tmp = new vector_type_t(IVL_VT_LOGIC, false, p[1]);
    # 	tmp->implicit_flag = true;
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_data_type_or_implicit_4(p):
    '''data_type_or_implicit :  '''
    if(parse_debug > 2):
        print('data_type_or_implicit_4', list(p))


    # { p[0] = None }
()


def p_data_type_or_implicit_or_void_1(p):
    '''data_type_or_implicit_or_void : data_type_or_implicit '''
    if(parse_debug):
        print('data_type_or_implicit_or_void_1', list(p))
    p[0] = p[1]


()


def p_data_type_or_implicit_or_void_2(p):
    '''data_type_or_implicit_or_void : K_void '''
    if(parse_debug):
        print('data_type_or_implicit_or_void_2', list(p))


    # { void_type_t*tmp = new void_type_t;
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_description_1(p):
    '''description : module '''
    if(parse_debug > 2):
        print('description_1', list(p))


()


def p_description_2(p):
    '''description : udp_primitive '''
    if(parse_debug):
        print('description_2', list(p))


()


def p_description_3(p):
    '''description : config_declaration '''
    if(parse_debug):
        print('description_3', list(p))


()


def p_description_4(p):
    '''description : nature_declaration '''
    if(parse_debug):
        print('description_4', list(p))


()


def p_description_5(p):
    '''description : package_declaration '''
    if(parse_debug):
        print('description_5', list(p))


()


def p_description_6(p):
    '''description : discipline_declaration '''
    if(parse_debug):
        print('description_6', list(p))


()


def p_description_7(p):
    '''description : package_item '''
    if(parse_debug):
        print('description_7', list(p))


()


def p_description_8(p):
    '''description : KK_attribute '(' IDENTIFIER ',' STRING ',' STRING ')' '''
    if(parse_debug):
        print('description_8', list(p))


    # { perm_string tmp3 = lex_strings.make(p[3]);
    # 	pform_set_type_attrib(tmp3, p[5], p[7]);
    # 	delete[] p[3];
    # 	delete[] p[5];
    #       }
()


def p_description_list_1(p):
    '''description_list : description '''
    if(parse_debug > 2):
        print('description_list_1', list(p))


()


def p_description_list_2(p):
    '''description_list : description_list description '''
    if(parse_debug):
        print('description_list_2', list(p))


()


def p_endnew_opt_1(p):
    '''endnew_opt : ':' K_new '''
    if(parse_debug):
        print('endnew_opt_1', list(p))


()


def p_endnew_opt_2(p):
    '''endnew_opt :  '''
    if(parse_debug):
        print('endnew_opt_2', list(p))


()


def p_dynamic_array_new_1(p):
    '''dynamic_array_new : K_new '[' expression ']' '''
    if(parse_debug):
        print('dynamic_array_new_1', list(p))


    # { p[0] = new PENewArray(p[3], 0);
    # 	FILE_NAME(p[0], @1);
    #       }
()


def p_dynamic_array_new_2(p):
    '''dynamic_array_new : K_new '[' expression ']' '(' expression ')' '''
    if(parse_debug):
        print('dynamic_array_new_2', list(p))


    # { p[0] = new PENewArray(p[3], p[6]);
    # 	FILE_NAME(p[0], @1);
    #       }
()


def p_for_step_1(p):
    '''for_step : lpvalue '=' expression '''
    if(parse_debug):
        print('for_step_1', list(p))


    # { PAssign*tmp = new PAssign(p[1],p[3]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_for_step_2(p):
    '''for_step : inc_or_dec_expression '''
    if(parse_debug):
        print('for_step_2', list(p))


    # { p[0] = pform_compressed_assign_from_inc_dec(@1, p[1]); }
()


def p_for_step_3(p):
    '''for_step : compressed_statement '''
    if(parse_debug):
        print('for_step_3', list(p))
    p[0] = p[1]


()


def p_function_declaration_1(p):
    '''function_declaration : K_function lifetime_opt data_type_or_implicit_or_void IDENTIFIER ';' _embed0_function_declaration function_item_list statement_or_null_list_opt K_endfunction _embed1_function_declaration endlabel_opt '''
    if(parse_debug):
        print('function_declaration_1', list(p))


    # { // Last step: check any closing name.
    # 	if (p[11]) {
    # 	      if (strcmp(p[4],p[11]) != 0) {
    # 		    yyerror(@11, "error: End label doesn't match "
    # 		                 "function name");
    # 	      }
    # 	      if (! gn_system_verilog()) {
    # 		    yyerror(@11, "error: Function end labels require "
    # 		                 "SystemVerilog.");
    # 	      }
    # 	      delete[]p[11];
    # 	}
    # 	delete[]p[4];
    #       }
()


def p_function_declaration_2(p):
    '''function_declaration : K_function lifetime_opt data_type_or_implicit_or_void IDENTIFIER _embed2_function_declaration '(' tf_port_list_opt ')' ';' block_item_decls_opt statement_or_null_list_opt K_endfunction _embed3_function_declaration endlabel_opt '''
    if(parse_debug):
        print('function_declaration_2', list(p))


    # { // Last step: check any closing name.
    # 	if (p[14]) {
    # 	      if (strcmp(p[4],p[14]) != 0) {
    # 		    yyerror(@14, "error: End label doesn't match "
    # 		                 "function name");
    # 	      }
    # 	      if (! gn_system_verilog()) {
    # 		    yyerror(@14, "error: Function end labels require "
    # 		                 "SystemVerilog.");
    # 	      }
    # 	      delete[]p[14];
    # 	}
    # 	delete[]p[4];
    #       }
()


def p_function_declaration_3(p):
    '''function_declaration : K_function lifetime_opt data_type_or_implicit_or_void IDENTIFIER error K_endfunction _embed4_function_declaration endlabel_opt '''
    if(parse_debug):
        print('function_declaration_3', list(p))


    # { // Last step: check any closing name.
    # 	if (p[8]) {
    # 	      if (strcmp(p[4],p[8]) != 0) {
    # 		    yyerror(@8, "error: End label doesn't match function name");
    # 	      }
    # 	      if (! gn_system_verilog()) {
    # 		    yyerror(@8, "error: Function end labels require "
    # 		                 "SystemVerilog.");
    # 	      }
    # 	      delete[]p[8];
    # 	}
    # 	delete[]p[4];
    #       }
()


def p__embed0_function_declaration(p):
    '''_embed0_function_declaration : '''


    # { assert(current_function == 0);
    # 	current_function = pform_push_function_scope(@1, p[4], p[2]);
    #       }
()


def p__embed1_function_declaration(p):
    '''_embed1_function_declaration : '''


    # { current_function->set_ports(p[7]);
    # 	current_function->set_return(p[3]);
    # 	current_function_set_statement(p[8]? @8 : @4, p[8]);
    # 	pform_set_this_class(@4, current_function);
    # 	pform_pop_scope();
    # 	current_function = 0;
    #       }
()


def p__embed2_function_declaration(p):
    '''_embed2_function_declaration : '''


    # { assert(current_function == 0);
    # 	current_function = pform_push_function_scope(@1, p[4], p[2]);
    #       }
()


def p__embed3_function_declaration(p):
    '''_embed3_function_declaration : '''


    # { current_function->set_ports(p[7]);
    # 	current_function->set_return(p[3]);
    # 	current_function_set_statement(p[11]? @11 : @4, p[11]);
    # 	pform_set_this_class(@4, current_function);
    # 	pform_pop_scope();
    # 	current_function = 0;
    # 	if (p[7]==0 && !gn_system_verilog()) {
    # 	      yyerror(@4, "error: Empty parenthesis syntax requires SystemVerilog.");
    # 	}
    #       }
()


def p__embed4_function_declaration(p):
    '''_embed4_function_declaration : '''


    # { /* */
    # 	if (current_function) {
    # 	      pform_pop_scope();
    # 	      current_function = 0;
    # 	}
    # 	assert(current_function == 0);
    # 	yyerror(@1, "error: Syntax error defining function.");
    # 	yyerrok;
    #       }
()


def p_import_export_1(p):
    '''import_export : K_import '''
    if(parse_debug):
        print('import_export_1', list(p))
    p[0] = True


()


def p_import_export_2(p):
    '''import_export : K_export '''
    if(parse_debug):
        print('import_export_2', list(p))
    p[0] = False


()


def p_implicit_class_handle_1(p):
    '''implicit_class_handle : K_this '''
    if(parse_debug):
        print('implicit_class_handle_1', list(p))


    # { p[0] = pform_create_this(); }
()


def p_implicit_class_handle_2(p):
    '''implicit_class_handle : K_super '''
    if(parse_debug):
        print('implicit_class_handle_2', list(p))


    # { p[0] = pform_create_super(); }
()


def p_inc_or_dec_expression_1(p):
    '''inc_or_dec_expression : K_INCR lpvalue %prec UNARY_PREC '''
    if(parse_debug):
        print('inc_or_dec_expression_1', list(p))


    # { PEUnary*tmp = new PEUnary('I', p[2]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_inc_or_dec_expression_2(p):
    '''inc_or_dec_expression : lpvalue K_INCR %prec UNARY_PREC '''
    if(parse_debug):
        print('inc_or_dec_expression_2', list(p))


    # { PEUnary*tmp = new PEUnary('i', p[1]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_inc_or_dec_expression_3(p):
    '''inc_or_dec_expression : K_DECR lpvalue %prec UNARY_PREC '''
    if(parse_debug):
        print('inc_or_dec_expression_3', list(p))


    # { PEUnary*tmp = new PEUnary('D', p[2]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_inc_or_dec_expression_4(p):
    '''inc_or_dec_expression : lpvalue K_DECR %prec UNARY_PREC '''
    if(parse_debug):
        print('inc_or_dec_expression_4', list(p))


    # { PEUnary*tmp = new PEUnary('d', p[1]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_inside_expression_1(p):
    '''inside_expression : expression K_inside '{' open_range_list '}' '''
    if(parse_debug):
        print('inside_expression_1', list(p))


    # { yyerror(@2, "sorry: \"inside\" expressions not supported yet.");
    # 	p[0] = None
    #       }
()


def p_integer_vector_type_1(p):
    '''integer_vector_type : K_reg '''
    if(parse_debug):
        print('integer_vector_type_1', list(p))
    p[0] = IVL_VT_NO_TYPE


()


def p_integer_vector_type_2(p):
    '''integer_vector_type : K_bit '''
    if(parse_debug):
        print('integer_vector_type_2', list(p))
    p[0] = IVL_VT_BOOL


()


def p_integer_vector_type_3(p):
    '''integer_vector_type : K_logic '''
    if(parse_debug):
        print('integer_vector_type_3', list(p))
    p[0] = IVL_VT_LOGIC


()


def p_integer_vector_type_4(p):
    '''integer_vector_type : K_bool '''
    if(parse_debug):
        print('integer_vector_type_4', list(p))


    # { p[0] = IVL_VT_BOOL; }
()


def p_join_keyword_1(p):
    '''join_keyword : K_join '''
    if(parse_debug):
        print('join_keyword_1', list(p))


    # { p[0] = PBlock::BL_PAR; }
()


def p_join_keyword_2(p):
    '''join_keyword : K_join_none '''
    if(parse_debug):
        print('join_keyword_2', list(p))


    # { p[0] = PBlock::BL_JOIN_NONE; }
()


def p_join_keyword_3(p):
    '''join_keyword : K_join_any '''
    if(parse_debug):
        print('join_keyword_3', list(p))


    # { p[0] = PBlock::BL_JOIN_ANY; }
()


def p_jump_statement_1(p):
    '''jump_statement : K_break ';' '''
    if(parse_debug):
        print('jump_statement_1', list(p))


    # { yyerror(@1, "sorry: break statements not supported.");
    # 	p[0] = None
    #       }
()


def p_jump_statement_2(p):
    '''jump_statement : K_return ';' '''
    if(parse_debug):
        print('jump_statement_2', list(p))


    # { PReturn*tmp = new PReturn(0);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_jump_statement_3(p):
    '''jump_statement : K_return expression ';' '''
    if(parse_debug):
        print('jump_statement_3', list(p))


    # { PReturn*tmp = new PReturn(p[2]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_lifetime_1(p):
    '''lifetime : K_automatic '''
    if(parse_debug):
        print('lifetime_1', list(p))


    # { p[0] = LexicalScope::AUTOMATIC; }
()


def p_lifetime_2(p):
    '''lifetime : K_static '''
    if(parse_debug):
        print('lifetime_2', list(p))


    # { p[0] = LexicalScope::STATIC; }
()


def p_lifetime_opt_1(p):
    '''lifetime_opt : lifetime '''
    if(parse_debug):
        print('lifetime_opt_1', list(p))
    p[0] = p[1]


()


def p_lifetime_opt_2(p):
    '''lifetime_opt :  '''
    if(parse_debug > 2):
        print('lifetime_opt_2', list(p))


    # { p[0] = LexicalScope::INHERITED; }
()


def p_loop_statement_1(p):
    '''loop_statement : K_for '(' lpvalue '=' expression ';' expression ';' for_step ')' statement_or_null '''
    if(parse_debug):
        print('loop_statement_1', list(p))


    # { PForStatement*tmp = new PForStatement(p[3], p[5], p[7], p[9], p[11]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_loop_statement_2(p):
    '''loop_statement : K_for '(' data_type IDENTIFIER '=' expression ';' expression ';' for_step ')' _embed0_loop_statement statement_or_null '''
    if(parse_debug):
        print('loop_statement_2', list(p))


    # { pform_name_t tmp_hident;
    # 	tmp_hident.push_back(name_component_t(lex_strings.make(p[4])));
    #
    # 	PEIdent*tmp_ident = pform_new_ident(tmp_hident);
    # 	FILE_NAME(tmp_ident, @4);
    #
    # 	PForStatement*tmp_for = new PForStatement(tmp_ident, p[6], p[8], p[10], p[13]);
    # 	FILE_NAME(tmp_for, @1);
    #
    # 	pform_pop_scope();
    # 	vector<Statement*>tmp_for_list (1);
    # 	tmp_for_list[0] = tmp_for;
    # 	PBlock*tmp_blk = current_block_stack.top();
    # 	current_block_stack.pop();
    # 	tmp_blk->set_statement(tmp_for_list);
    # 	p[0] = tmp_blk;
    # 	delete[]p[4];
    #       }
()


def p_loop_statement_3(p):
    '''loop_statement : K_forever statement_or_null '''
    if(parse_debug):
        print('loop_statement_3', list(p))


    # { PForever*tmp = new PForever(p[2]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_loop_statement_4(p):
    '''loop_statement : K_repeat '(' expression ')' statement_or_null '''
    if(parse_debug):
        print('loop_statement_4', list(p))


    # { PRepeat*tmp = new PRepeat(p[3], p[5]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_loop_statement_5(p):
    '''loop_statement : K_while '(' expression ')' statement_or_null '''
    if(parse_debug):
        print('loop_statement_5', list(p))


    # { PWhile*tmp = new PWhile(p[3], p[5]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_loop_statement_6(p):
    '''loop_statement : K_do statement_or_null K_while '(' expression ')' ';' '''
    if(parse_debug):
        print('loop_statement_6', list(p))


    # { PDoWhile*tmp = new PDoWhile(p[5], p[2]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_loop_statement_7(p):
    '''loop_statement : K_foreach '(' IDENTIFIER '[' loop_variables ']' ')' _embed1_loop_statement statement_or_null '''
    if(parse_debug):
        print('loop_statement_7', list(p))


    # { PForeach*tmp_for = pform_make_foreach(@1, p[3], p[5], p[9]);
    #
    # 	pform_pop_scope();
    # 	vector<Statement*>tmp_for_list(1);
    # 	tmp_for_list[0] = tmp_for;
    # 	PBlock*tmp_blk = current_block_stack.top();
    # 	current_block_stack.pop();
    # 	tmp_blk->set_statement(tmp_for_list);
    # 	p[0] = tmp_blk;
    #       }
()


def p_loop_statement_8(p):
    '''loop_statement : K_for '(' lpvalue '=' expression ';' expression ';' error ')' statement_or_null '''
    if(parse_debug):
        print('loop_statement_8', list(p))


    # { p[0] = None
    # 	yyerror(@1, "error: Error in for loop step assignment.");
    #       }
()


def p_loop_statement_9(p):
    '''loop_statement : K_for '(' lpvalue '=' expression ';' error ';' for_step ')' statement_or_null '''
    if(parse_debug):
        print('loop_statement_9', list(p))


    # { p[0] = None
    # 	yyerror(@1, "error: Error in for loop condition expression.");
    #       }
()


def p_loop_statement_10(p):
    '''loop_statement : K_for '(' error ')' statement_or_null '''
    if(parse_debug):
        print('loop_statement_10', list(p))


    # { p[0] = None
    # 	yyerror(@1, "error: Incomprehensible for loop.");
    #       }
()


def p_loop_statement_11(p):
    '''loop_statement : K_while '(' error ')' statement_or_null '''
    if(parse_debug):
        print('loop_statement_11', list(p))


    # { p[0] = None
    # 	yyerror(@1, "error: Error in while loop condition.");
    #       }
()


def p_loop_statement_12(p):
    '''loop_statement : K_do statement_or_null K_while '(' error ')' ';' '''
    if(parse_debug):
        print('loop_statement_12', list(p))


    # { p[0] = None
    # 	yyerror(@1, "error: Error in do/while loop condition.");
    #       }
()


def p_loop_statement_13(p):
    '''loop_statement : K_foreach '(' IDENTIFIER '[' error ']' ')' statement_or_null '''
    if(parse_debug):
        print('loop_statement_13', list(p))


    # { p[0] = None
    #         yyerror(@4, "error: Errors in foreach loop variables list.");
    #       }
()


def p__embed0_loop_statement(p):
    '''_embed0_loop_statement : '''


    # { static unsigned for_counter = 0;
    # 	char for_block_name [64];
    # 	snif(parse_debug): printf(for_block_name, sizeof for_block_name, "$ivl_for_loop%u", for_counter);
    # 	for_counter += 1;
    # 	PBlock*tmp = pform_push_block_scope(for_block_name, PBlock::BL_SEQ);
    # 	FILE_NAME(tmp, @1);
    # 	current_block_stack.push(tmp);
    #
    # 	list<decl_assignment_t*>assign_list;
    # 	decl_assignment_t*tmp_assign = new decl_assignment_t;
    # 	tmp_assign->name = lex_strings.make(p[4]);
    # 	assign_list.push_back(tmp_assign);
    # 	pform_makewire(@4, 0, str_strength, &assign_list, NetNet::REG, p[3]);
    #       }
()


def p__embed1_loop_statement(p):
    '''_embed1_loop_statement : '''


    # { static unsigned foreach_counter = 0;
    # 	char for_block_name[64];
    # 	snif(parse_debug): printf(for_block_name, sizeof for_block_name, "$ivl_foreach%u", foreach_counter);
    # 	foreach_counter += 1;
    #
    # 	PBlock*tmp = pform_push_block_scope(for_block_name, PBlock::BL_SEQ);
    # 	FILE_NAME(tmp, @1);
    # 	current_block_stack.push(tmp);
    #
    # 	pform_make_foreach_declarations(@1, p[5]);
    #       }
()


def p_list_of_variable_decl_assignments_1(p):
    '''list_of_variable_decl_assignments : variable_decl_assignment '''
    if(parse_debug):
        print('list_of_variable_decl_assignments_1', list(p))


    # { list<decl_assignment_t*>*tmp = new list<decl_assignment_t*>;
    # 	tmp->push_back(p[1]);
    # 	p[0] = tmp;
    #       }
()


def p_list_of_variable_decl_assignments_2(p):
    '''list_of_variable_decl_assignments : list_of_variable_decl_assignments ',' variable_decl_assignment '''
    if(parse_debug):
        print('list_of_variable_decl_assignments_2', list(p))


    # { list<decl_assignment_t*>*tmp = p[1];
    # 	tmp->push_back(p[3]);
    # 	p[0] = tmp;
    #       }
()


def p_variable_decl_assignment_1(p):
    '''variable_decl_assignment : IDENTIFIER dimensions_opt '''
    if(parse_debug):
        print('variable_decl_assignment_1', list(p))


    # { decl_assignment_t*tmp = new decl_assignment_t;
    # 	tmp->name = lex_strings.make(p[1]);
    # 	if (p[2]) {
    # 	      tmp->index = *p[2];
    # 	      delete p[2];
    # 	}
    # 	delete[]p[1];
    # 	p[0] = tmp;
    #       }
()


def p_variable_decl_assignment_2(p):
    '''variable_decl_assignment : IDENTIFIER '=' expression '''
    if(parse_debug):
        print('variable_decl_assignment_2', list(p))


    # { decl_assignment_t*tmp = new decl_assignment_t;
    # 	tmp->name = lex_strings.make(p[1]);
    # 	tmp->expr .reset(p[3]);
    # 	delete[]p[1];
    # 	p[0] = tmp;
    #       }
()


def p_variable_decl_assignment_3(p):
    '''variable_decl_assignment : IDENTIFIER '=' K_new '(' ')' '''
    if(parse_debug):
        print('variable_decl_assignment_3', list(p))


    # { decl_assignment_t*tmp = new decl_assignment_t;
    # 	tmp->name = lex_strings.make(p[1]);
    # 	PENewClass*expr = new PENewClass;
    # 	FILE_NAME(expr, @3);
    # 	tmp->expr .reset(expr);
    # 	delete[]p[1];
    # 	p[0] = tmp;
    #       }
()


def p_loop_variables_1(p):
    '''loop_variables : loop_variables ',' IDENTIFIER '''
    if(parse_debug):
        print('loop_variables_1', list(p))


    # { list<perm_string>*tmp = p[1];
    # 	tmp->push_back(lex_strings.make(p[3]));
    # 	delete[]p[3];
    # 	p[0] = tmp;
    #       }
()


def p_loop_variables_2(p):
    '''loop_variables : IDENTIFIER '''
    if(parse_debug):
        print('loop_variables_2', list(p))


    # { list<perm_string>*tmp = new list<perm_string>;
    # 	tmp->push_back(lex_strings.make(p[1]));
    # 	delete[]p[1];
    # 	p[0] = tmp;
    #       }
()


def p_method_qualifier_1(p):
    '''method_qualifier : K_virtual '''
    if(parse_debug):
        print('method_qualifier_1', list(p))


()


def p_method_qualifier_2(p):
    '''method_qualifier : class_item_qualifier '''
    if(parse_debug):
        print('method_qualifier_2', list(p))


()


def p_method_qualifier_opt_1(p):
    '''method_qualifier_opt : method_qualifier '''
    if(parse_debug):
        print('method_qualifier_opt_1', list(p))


()


def p_method_qualifier_opt_2(p):
    '''method_qualifier_opt :  '''
    if(parse_debug):
        print('method_qualifier_opt_2', list(p))


()


def p_modport_declaration_1(p):
    '''modport_declaration : K_modport _embed0_modport_declaration modport_item_list ';' '''
    if(parse_debug):
        print('modport_declaration_1', list(p))


()


def p__embed0_modport_declaration(p):
    '''_embed0_modport_declaration : '''


    # { if (!pform_in_interface())
    # 	      yyerror(@1, "error: modport declarations are only allowed "
    # 			  "in interfaces.");
    #       }
()


def p_modport_item_list_1(p):
    '''modport_item_list : modport_item '''
    if(parse_debug):
        print('modport_item_list_1', list(p))


()


def p_modport_item_list_2(p):
    '''modport_item_list : modport_item_list ',' modport_item '''
    if(parse_debug):
        print('modport_item_list_2', list(p))


()


def p_modport_item_1(p):
    '''modport_item : IDENTIFIER _embed0_modport_item '(' modport_ports_list ')' '''
    if(parse_debug):
        print('modport_item_1', list(p))


    # { pform_end_modport_item(@1); }
()


def p__embed0_modport_item(p):
    '''_embed0_modport_item : '''


    # { pform_start_modport_item(@1, p[1]); }
()


def p_modport_ports_list_1(p):
    '''modport_ports_list : modport_ports_declaration '''
    if(parse_debug):
        print('modport_ports_list_1', list(p))


()


def p_modport_ports_list_2(p):
    '''modport_ports_list : modport_ports_list ',' modport_ports_declaration '''
    if(parse_debug):
        print('modport_ports_list_2', list(p))


()


def p_modport_ports_list_3(p):
    '''modport_ports_list : modport_ports_list ',' modport_simple_port '''
    if(parse_debug):
        print('modport_ports_list_3', list(p))


    # { if (last_modport_port.type == MP_SIMPLE) {
    # 	      pform_add_modport_port(@3, last_modport_port.direction,
    # 				     p[3]->name, p[3]->parm);
    # 	} else {
    # 	      yyerror(@3, "error: modport expression not allowed here.");
    # 	}
    # 	delete p[3];
    #       }
()


def p_modport_ports_list_4(p):
    '''modport_ports_list : modport_ports_list ',' modport_tf_port '''
    if(parse_debug):
        print('modport_ports_list_4', list(p))


    # { if (last_modport_port.type != MP_TF)
    # 	      yyerror(@3, "error: task/function declaration not allowed here.");
    #       }
()


def p_modport_ports_list_5(p):
    '''modport_ports_list : modport_ports_list ',' IDENTIFIER '''
    if(parse_debug):
        print('modport_ports_list_5', list(p))


    # { if (last_modport_port.type == MP_SIMPLE) {
    # 	      pform_add_modport_port(@3, last_modport_port.direction,
    # 				     lex_strings.make(p[3]), 0);
    # 	} else if (last_modport_port.type != MP_TF) {
    # 	      yyerror(@3, "error: list of identifiers not allowed here.");
    # 	}
    # 	delete[] p[3];
    #       }
()


def p_modport_ports_list_6(p):
    '''modport_ports_list : modport_ports_list ',' '''
    if(parse_debug):
        print('modport_ports_list_6', list(p))


    # { yyerror(@2, "error: NULL port declarations are not allowed"); }
()


def p_modport_ports_declaration_1(p):
    '''modport_ports_declaration : attribute_list_opt port_direction IDENTIFIER '''
    if(parse_debug):
        print('modport_ports_declaration_1', list(p))


    # { last_modport_port.type = MP_SIMPLE;
    # 	last_modport_port.direction = p[2];
    # 	pform_add_modport_port(@3, p[2], lex_strings.make(p[3]), 0);
    # 	delete[] p[3];
    # 	delete p[1];
    #       }
()


def p_modport_ports_declaration_2(p):
    '''modport_ports_declaration : attribute_list_opt port_direction modport_simple_port '''
    if(parse_debug):
        print('modport_ports_declaration_2', list(p))


    # { last_modport_port.type = MP_SIMPLE;
    # 	last_modport_port.direction = p[2];
    # 	pform_add_modport_port(@3, p[2], p[3]->name, p[3]->parm);
    # 	delete p[3];
    # 	delete p[1];
    #       }
()


def p_modport_ports_declaration_3(p):
    '''modport_ports_declaration : attribute_list_opt import_export IDENTIFIER '''
    if(parse_debug):
        print('modport_ports_declaration_3', list(p))


    # { last_modport_port.type = MP_TF;
    # 	last_modport_port.is_import = p[2];
    # 	yyerror(@3, "sorry: modport task/function ports are not yet supported.");
    # 	delete[] p[3];
    # 	delete p[1];
    #       }
()


def p_modport_ports_declaration_4(p):
    '''modport_ports_declaration : attribute_list_opt import_export modport_tf_port '''
    if(parse_debug):
        print('modport_ports_declaration_4', list(p))


    # { last_modport_port.type = MP_TF;
    # 	last_modport_port.is_import = p[2];
    # 	yyerror(@3, "sorry: modport task/function ports are not yet supported.");
    # 	delete p[1];
    #       }
()


def p_modport_ports_declaration_5(p):
    '''modport_ports_declaration : attribute_list_opt K_clocking IDENTIFIER '''
    if(parse_debug):
        print('modport_ports_declaration_5', list(p))


    # { last_modport_port.type = MP_CLOCKING;
    # 	last_modport_port.direction = NetNet::NOT_A_PORT;
    # 	yyerror(@3, "sorry: modport clocking declaration is not yet supported.");
    # 	delete[] p[3];
    # 	delete p[1];
    #       }
()


def p_modport_simple_port_1(p):
    '''modport_simple_port : '.' IDENTIFIER '(' expression ')' '''
    if(parse_debug):
        print('modport_simple_port_1', list(p))


    # { named_pexpr_t*tmp = new named_pexpr_t;
    # 	tmp->name = lex_strings.make(p[2]);
    # 	tmp->parm = p[4];
    # 	delete[]p[2];
    # 	p[0] = tmp;
    #       }
()


def p_modport_tf_port_1(p):
    '''modport_tf_port : K_task IDENTIFIER '''
    if(parse_debug):
        print('modport_tf_port_1', list(p))


()


def p_modport_tf_port_2(p):
    '''modport_tf_port : K_task IDENTIFIER '(' tf_port_list_opt ')' '''
    if(parse_debug):
        print('modport_tf_port_2', list(p))


()


def p_modport_tf_port_3(p):
    '''modport_tf_port : K_function data_type_or_implicit_or_void IDENTIFIER '''
    if(parse_debug):
        print('modport_tf_port_3', list(p))


()


def p_modport_tf_port_4(p):
    '''modport_tf_port : K_function data_type_or_implicit_or_void IDENTIFIER '(' tf_port_list_opt ')' '''
    if(parse_debug):
        print('modport_tf_port_4', list(p))


()


def p_non_integer_type_1(p):
    '''non_integer_type : K_real '''
    if(parse_debug):
        print('non_integer_type_1', list(p))


    # { p[0] = real_type_t::REAL; }
()


def p_non_integer_type_2(p):
    '''non_integer_type : K_realtime '''
    if(parse_debug):
        print('non_integer_type_2', list(p))


    # { p[0] = real_type_t::REAL; }
()


def p_non_integer_type_3(p):
    '''non_integer_type : K_shortreal '''
    if(parse_debug):
        print('non_integer_type_3', list(p))


    # { p[0] = real_type_t::SHORTREAL; }
()


def p_number_1(p):
    '''number : BASED_NUMBER '''
    if(parse_debug):
        print('number_1', list(p))

    p[1] = p[1].replace("'b", "0b")
    p[1] = p[1].replace("'x", "0x")
    num = Leaf(token.NUMBER, "%s" % (p[1]))
    p[0] = num


    # { p[0] = p[1]; based_size = 0;}
()


def p_number_2(p):
    '''number : DEC_NUMBER '''
    if(parse_debug):
        print('number_2', list(p))
    p[1] = p[1].replace("'b", "0b")
    p[1] = p[1].replace("'x", "0x")
    num = Leaf(token.NUMBER, "%s" % (p[1]))
    p[0] = num


    # { p[0] = p[1]; based_size = 0;}
()


def p_number_3(p):
    '''number : DEC_NUMBER BASED_NUMBER '''
    if(parse_debug):
        print('number_3', list(p))

    p[2] = p[2].replace("'b", "0b")
    p[2] = p[2].replace("'x", "0x")

    num = Leaf(token.NUMBER, "%s" % (p[2]))
    p[0] = num


    # { p[0] = pform_verinum_with_size(p[1],p[2], @2.text, @2.first_line);
    # 	       based_size = 0; }
()


def p_number_4(p):
    '''number : UNBASED_NUMBER '''
    if(parse_debug):
        print('number_4', list(p))


    # { p[0] = p[1]; based_size = 0;}
()


def p_number_5(p):
    '''number : DEC_NUMBER UNBASED_NUMBER '''
    if(parse_debug):
        print('number_5', list(p))


    # { yyerror(@1, "error: Unbased SystemVerilog literal cannot have "
    # 	                   "a size.");
    # 	       p[0] = p[1]; based_size = 0;}
()


def p_open_range_list_1(p):
    '''open_range_list : open_range_list ',' value_range '''
    if(parse_debug):
        print('open_range_list_1', list(p))


()


def p_open_range_list_2(p):
    '''open_range_list : value_range '''
    if(parse_debug):
        print('open_range_list_2', list(p))


()


def p_package_declaration_1(p):
    '''package_declaration : K_package lifetime_opt IDENTIFIER ';' _embed0_package_declaration timeunits_declaration_opt _embed1_package_declaration package_item_list_opt K_endpackage endlabel_opt '''
    if(parse_debug):
        print('package_declaration_1', list(p))


    # { pform_end_package_declaration(@1);
    # 	// If an end label is present make sure it match the package name.
    # 	if (p[10]) {
    # 	      if (strcmp(p[3],p[10]) != 0) {
    # 		    yyerror(@10, "error: End label doesn't match package name");
    # 	      }
    # 	      delete[]p[10];
    # 	}
    # 	delete[]p[3];
    #       }
()


def p__embed0_package_declaration(p):
    '''_embed0_package_declaration : '''


    # { pform_start_package_declaration(@1, p[3], p[2]); }
()


def p__embed1_package_declaration(p):
    '''_embed1_package_declaration : '''


    # { pform_set_scope_timescale(@1); }
()


def p_module_package_import_list_opt_1(p):
    '''module_package_import_list_opt :  '''
    if(parse_debug > 1):
        print('module_package_import_list_opt_1', list(p))


()


def p_module_package_import_list_opt_2(p):
    '''module_package_import_list_opt : package_import_list '''
    if(parse_debug):
        print('module_package_import_list_opt_2', list(p))


()


def p_package_import_list_1(p):
    '''package_import_list : package_import_declaration '''
    if(parse_debug):
        print('package_import_list_1', list(p))


()


def p_package_import_list_2(p):
    '''package_import_list : package_import_list package_import_declaration '''
    if(parse_debug):
        print('package_import_list_2', list(p))


()


def p_package_import_declaration_1(p):
    '''package_import_declaration : K_import package_import_item_list ';' '''
    if(parse_debug):
        print('package_import_declaration_1', list(p))


    # { }
()


def p_package_import_item_1(p):
    '''package_import_item : PACKAGE_IDENTIFIER K_SCOPE_RES IDENTIFIER '''
    if(parse_debug):
        print('package_import_item_1', list(p))


    # { pform_package_import(@2, p[1], p[3]);
    # 	delete[]p[3];
    #       }
()


def p_package_import_item_2(p):
    '''package_import_item : PACKAGE_IDENTIFIER K_SCOPE_RES '*' '''
    if(parse_debug):
        print('package_import_item_2', list(p))


    # { pform_package_import(@2, p[1], 0);
    #       }
()


def p_package_import_item_list_1(p):
    '''package_import_item_list : package_import_item_list ',' package_import_item '''
    if(parse_debug):
        print('package_import_item_list_1', list(p))


()


def p_package_import_item_list_2(p):
    '''package_import_item_list : package_import_item '''
    if(parse_debug):
        print('package_import_item_list_2', list(p))


()


def p_package_item_1(p):
    '''package_item : timeunits_declaration '''
    if(parse_debug):
        print('package_item_1', list(p))


()


def p_package_item_2(p):
    '''package_item : K_parameter param_type parameter_assign_list ';' '''
    if(parse_debug):
        print('package_item_2', list(p))


()


def p_package_item_3(p):
    '''package_item : K_localparam param_type localparam_assign_list ';' '''
    if(parse_debug):
        print('package_item_3', list(p))


()


def p_package_item_4(p):
    '''package_item : type_declaration '''
    if(parse_debug):
        print('package_item_4', list(p))


()


def p_package_item_5(p):
    '''package_item : function_declaration '''
    if(parse_debug):
        print('package_item_5', list(p))


()


def p_package_item_6(p):
    '''package_item : task_declaration '''
    if(parse_debug):
        print('package_item_6', list(p))


()


def p_package_item_7(p):
    '''package_item : data_declaration '''
    if(parse_debug):
        print('package_item_7', list(p))


()


def p_package_item_8(p):
    '''package_item : class_declaration '''
    if(parse_debug):
        print('package_item_8', list(p))


()


def p_package_item_list_1(p):
    '''package_item_list : package_item_list package_item '''
    if(parse_debug):
        print('package_item_list_1', list(p))


()


def p_package_item_list_2(p):
    '''package_item_list : package_item '''
    if(parse_debug):
        print('package_item_list_2', list(p))


()


def p_package_item_list_opt_1(p):
    '''package_item_list_opt : package_item_list '''
    if(parse_debug):
        print('package_item_list_opt_1', list(p))


()


def p_package_item_list_opt_2(p):
    '''package_item_list_opt :  '''
    if(parse_debug):
        print('package_item_list_opt_2', list(p))


()


def p_port_direction_1(p):
    '''port_direction : K_input '''
    if(parse_debug):
        print('port_direction_1', list(p))


    # { p[0] = NetNet::PINPUT; }
()


def p_port_direction_2(p):
    '''port_direction : K_output '''
    if(parse_debug):
        print('port_direction_2', list(p))


    # { p[0] = NetNet::POUTPUT; }
()


def p_port_direction_3(p):
    '''port_direction : K_inout '''
    if(parse_debug):
        print('port_direction_3', list(p))


    # { p[0] = NetNet::PINOUT; }
()


def p_port_direction_4(p):
    '''port_direction : K_ref '''
    if(parse_debug):
        print('port_direction_4', list(p))


    # { p[0] = NetNet::PREF;
    #         if (!gn_system_verilog()) {
    # 	      yyerror(@1, "error: Reference ports (ref) require SystemVerilog.");
    # 	      p[0] = NetNet::PINPUT;
    # 	}
    #       }
()


def p_port_direction_opt_1(p):
    '''port_direction_opt : port_direction '''
    if(parse_debug):
        print('port_direction_opt_1', list(p))
    p[0] = p[1]


()


def p_port_direction_opt_2(p):
    '''port_direction_opt :  '''
    if(parse_debug):
        print('port_direction_opt_2', list(p))


    # { p[0] = NetNet::PIMPLICIT; }
()


def p_property_expr_1(p):
    '''property_expr : expression '''
    if(parse_debug):
        print('property_expr_1', list(p))


()


def p_procedural_assertion_statement_1(p):
    '''procedural_assertion_statement : K_assert '(' expression ')' statement %prec less_than_K_else '''
    if(parse_debug):
        print('procedural_assertion_statement_1', list(p))


    # { yyerror(@1, "sorry: Simple immediate assertion statements not implemented.");
    # 	p[0] = None
    #       }
()


def p_procedural_assertion_statement_2(p):
    '''procedural_assertion_statement : K_assert '(' expression ')' K_else statement '''
    if(parse_debug):
        print('procedural_assertion_statement_2', list(p))


    # { yyerror(@1, "sorry: Simple immediate assertion statements not implemented.");
    # 	p[0] = None
    #       }
()


def p_procedural_assertion_statement_3(p):
    '''procedural_assertion_statement : K_assert '(' expression ')' statement K_else statement '''
    if(parse_debug):
        print('procedural_assertion_statement_3', list(p))


    # { yyerror(@1, "sorry: Simple immediate assertion statements not implemented.");
    # 	p[0] = None
    #       }
()


def p_property_qualifier_1(p):
    '''property_qualifier : class_item_qualifier '''
    if(parse_debug):
        print('property_qualifier_1', list(p))


()


def p_property_qualifier_2(p):
    '''property_qualifier : random_qualifier '''
    if(parse_debug):
        print('property_qualifier_2', list(p))


()


def p_property_qualifier_opt_1(p):
    '''property_qualifier_opt : property_qualifier_list '''
    if(parse_debug):
        print('property_qualifier_opt_1', list(p))
    p[0] = p[1]


()


def p_property_qualifier_opt_2(p):
    '''property_qualifier_opt :  '''
    if(parse_debug):
        print('property_qualifier_opt_2', list(p))


    # { p[0] = property_qualifier_t::make_none(); }
()


def p_property_qualifier_list_1(p):
    '''property_qualifier_list : property_qualifier_list property_qualifier '''
    if(parse_debug):
        print('property_qualifier_list_1', list(p))


    # { p[0] = p[1] | p[2]; }
()


def p_property_qualifier_list_2(p):
    '''property_qualifier_list : property_qualifier '''
    if(parse_debug):
        print('property_qualifier_list_2', list(p))
    p[0] = p[1]


()


def p_property_spec_1(p):
    '''property_spec : clocking_event_opt property_spec_disable_iff_opt property_expr '''
    if(parse_debug):
        print('property_spec_1', list(p))


()


def p_property_spec_disable_iff_opt_1(p):
    '''property_spec_disable_iff_opt : K_disable K_iff '(' expression ')' '''
    if(parse_debug):
        print('property_spec_disable_iff_opt_1', list(p))


()


def p_property_spec_disable_iff_opt_2(p):
    '''property_spec_disable_iff_opt :  '''
    if(parse_debug):
        print('property_spec_disable_iff_opt_2', list(p))


()


def p_random_qualifier_1(p):
    '''random_qualifier : K_rand '''
    if(parse_debug):
        print('random_qualifier_1', list(p))


    # { p[0] = property_qualifier_t::make_rand(); }
()


def p_random_qualifier_2(p):
    '''random_qualifier : K_randc '''
    if(parse_debug):
        print('random_qualifier_2', list(p))


    # { p[0] = property_qualifier_t::make_randc(); }
()


def p_real_or_realtime_1(p):
    '''real_or_realtime : K_real '''
    if(parse_debug):
        print('real_or_realtime_1', list(p))


()


def p_real_or_realtime_2(p):
    '''real_or_realtime : K_realtime '''
    if(parse_debug):
        print('real_or_realtime_2', list(p))


()


def p_signing_1(p):
    '''signing : K_signed '''
    if(parse_debug):
        print('signing_1', list(p))
    p[0] = True


()


def p_signing_2(p):
    '''signing : K_unsigned '''
    if(parse_debug):
        print('signing_2', list(p))
    p[0] = False


()


def p_simple_type_or_string_1(p):
    '''simple_type_or_string : integer_vector_type '''
    if(parse_debug):
        print('simple_type_or_string_1', list(p))


    # { ivl_variable_type_t use_vtype = p[1];
    # 	bool reg_flag = false;
    # 	if (use_vtype == IVL_VT_NO_TYPE) {
    # 	      use_vtype = IVL_VT_LOGIC;
    # 	      reg_flag = true;
    # 	}
    # 	vector_type_t*tmp = new vector_type_t(use_vtype, false, 0);
    # 	tmp->reg_flag = reg_flag;
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_simple_type_or_string_2(p):
    '''simple_type_or_string : non_integer_type '''
    if(parse_debug):
        print('simple_type_or_string_2', list(p))


    # { real_type_t*tmp = new real_type_t(p[1]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_simple_type_or_string_3(p):
    '''simple_type_or_string : atom2_type '''
    if(parse_debug):
        print('simple_type_or_string_3', list(p))


    # { atom2_type_t*tmp = new atom2_type_t(p[1], true);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_simple_type_or_string_4(p):
    '''simple_type_or_string : K_integer '''
    if(parse_debug):
        print('simple_type_or_string_4', list(p))


    # { list<pform_range_t>*pd = make_range_from_width(integer_width);
    # 	vector_type_t*tmp = new vector_type_t(IVL_VT_LOGIC, true, pd);
    # 	tmp->reg_flag = true;
    # 	tmp->integer_flag = true;
    # 	p[0] = tmp;
    #       }
()


def p_simple_type_or_string_5(p):
    '''simple_type_or_string : K_time '''
    if(parse_debug):
        print('simple_type_or_string_5', list(p))


    # { list<pform_range_t>*pd = make_range_from_width(64);
    # 	vector_type_t*tmp = new vector_type_t(IVL_VT_LOGIC, false, pd);
    # 	tmp->reg_flag = !gn_system_verilog();
    # 	p[0] = tmp;
    #       }
()


def p_simple_type_or_string_6(p):
    '''simple_type_or_string : TYPE_IDENTIFIER '''
    if(parse_debug):
        print('simple_type_or_string_6', list(p))


    # { p[0] = p[1].type;
    # 	delete[]p[1].text;
    #       }
()


def p_simple_type_or_string_7(p):
    '''simple_type_or_string : PACKAGE_IDENTIFIER K_SCOPE_RES _embed0_simple_type_or_string TYPE_IDENTIFIER '''
    if(parse_debug):
        print('simple_type_or_string_7', list(p))


    # { lex_in_package_scope(0);
    # 	p[0] = p[4].type;
    # 	delete[]p[4].text;
    #       }
()


def p_simple_type_or_string_8(p):
    '''simple_type_or_string : K_string '''
    if(parse_debug):
        print('simple_type_or_string_8', list(p))


    # { string_type_t*tmp = new string_type_t;
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p__embed0_simple_type_or_string(p):
    '''_embed0_simple_type_or_string : '''


    # { lex_in_package_scope(p[1]); }
()


def p_statement_1(p):
    '''statement : attribute_list_opt statement_item '''
    if(parse_debug):
        print('statement_1', list(p))

    # { pform_bind_attributes(p[2]->attributes, p[1]);
    p[0] = p[2]

    #       }
()


def p_statement_or_null_1(p):
    '''statement_or_null : statement '''
    if(parse_debug):
        print('statement_or_null_1', list(p))
    p[0] = p[1]


()


def p_statement_or_null_2(p):
    '''statement_or_null : attribute_list_opt ';' '''
    if(parse_debug):
        print('statement_or_null_2', list(p))

    raise(Exception("p_statement_or_null_2"))


    # { p[0] = None }
()


def p_stream_expression_1(p):
    '''stream_expression : expression '''
    if(parse_debug):
        print('stream_expression_1', list(p))


()


def p_stream_expression_list_1(p):
    '''stream_expression_list : stream_expression_list ',' stream_expression '''
    if(parse_debug):
        print('stream_expression_list_1', list(p))


()


def p_stream_expression_list_2(p):
    '''stream_expression_list : stream_expression '''
    if(parse_debug):
        print('stream_expression_list_2', list(p))


()


def p_stream_operator_1(p):
    '''stream_operator : K_LS '''
    if(parse_debug):
        print('stream_operator_1', list(p))


()


def p_stream_operator_2(p):
    '''stream_operator : K_RS '''
    if(parse_debug):
        print('stream_operator_2', list(p))


()


def p_streaming_concatenation_1(p):
    '''streaming_concatenation : '{' stream_operator '{' stream_expression_list '}' '}' '''
    if(parse_debug):
        print('streaming_concatenation_1', list(p))


    # { /* streaming concatenation is a SystemVerilog thing. */
    # 	if (gn_system_verilog()) {
    # 	      yyerror(@2, "sorry: Streaming concatenation not supported.");
    # 	      p[0] = None
    # 	} else {
    # 	      yyerror(@2, "error: Streaming concatenation requires SystemVerilog");
    # 	      p[0] = None
    # 	}
    #       }
()


def p_task_declaration_1(p):
    '''task_declaration : K_task lifetime_opt IDENTIFIER ';' _embed0_task_declaration task_item_list_opt statement_or_null_list_opt K_endtask _embed1_task_declaration endlabel_opt '''
    if(parse_debug):
        print('task_declaration_1', list(p))


    # { // Last step: check any closing name. This is done late so
    # 	// that the parser can look ahead to detect the present
    # 	// endlabel_opt but still have the pform_endmodule() called
    # 	// early enough that the lexor can know we are outside the
    # 	// module.
    # 	if (p[10]) {
    # 	      if (strcmp(p[3],p[10]) != 0) {
    # 		    yyerror(@10, "error: End label doesn't match task name");
    # 	      }
    # 	      if (! gn_system_verilog()) {
    # 		    yyerror(@10, "error: Task end labels require "
    # 		                 "SystemVerilog.");
    # 	      }
    # 	      delete[]p[10];
    # 	}
    # 	delete[]p[3];
    #       }
()


def p_task_declaration_2(p):
    '''task_declaration : K_task lifetime_opt IDENTIFIER '(' _embed2_task_declaration tf_port_list ')' ';' block_item_decls_opt statement_or_null_list_opt K_endtask _embed3_task_declaration endlabel_opt '''
    if(parse_debug):
        print('task_declaration_2', list(p))


    # { // Last step: check any closing name. This is done late so
    # 	// that the parser can look ahead to detect the present
    # 	// endlabel_opt but still have the pform_endmodule() called
    # 	// early enough that the lexor can know we are outside the
    # 	// module.
    # 	if (p[13]) {
    # 	      if (strcmp(p[3],p[13]) != 0) {
    # 		    yyerror(@13, "error: End label doesn't match task name");
    # 	      }
    # 	      if (! gn_system_verilog()) {
    # 		    yyerror(@13, "error: Task end labels require "
    # 		                 "SystemVerilog.");
    # 	      }
    # 	      delete[]p[13];
    # 	}
    # 	delete[]p[3];
    #       }
()


def p_task_declaration_3(p):
    '''task_declaration : K_task lifetime_opt IDENTIFIER '(' ')' ';' _embed4_task_declaration block_item_decls_opt statement_or_null_list K_endtask _embed5_task_declaration endlabel_opt '''
    if(parse_debug):
        print('task_declaration_3', list(p))


    # { // Last step: check any closing name. This is done late so
    # 	// that the parser can look ahead to detect the present
    # 	// endlabel_opt but still have the pform_endmodule() called
    # 	// early enough that the lexor can know we are outside the
    # 	// module.
    # 	if (p[12]) {
    # 	      if (strcmp(p[3],p[12]) != 0) {
    # 		    yyerror(@12, "error: End label doesn't match task name");
    # 	      }
    # 	      if (! gn_system_verilog()) {
    # 		    yyerror(@12, "error: Task end labels require "
    # 		                 "SystemVerilog.");
    # 	      }
    # 	      delete[]p[12];
    # 	}
    # 	delete[]p[3];
    #       }
()


def p_task_declaration_4(p):
    '''task_declaration : K_task lifetime_opt IDENTIFIER error K_endtask _embed6_task_declaration endlabel_opt '''
    if(parse_debug):
        print('task_declaration_4', list(p))


    # { // Last step: check any closing name. This is done late so
    # 	// that the parser can look ahead to detect the present
    # 	// endlabel_opt but still have the pform_endmodule() called
    # 	// early enough that the lexor can know we are outside the
    # 	// module.
    # 	if (p[7]) {
    # 	      if (strcmp(p[3],p[7]) != 0) {
    # 		    yyerror(@7, "error: End label doesn't match task name");
    # 	      }
    # 	      if (! gn_system_verilog()) {
    # 		    yyerror(@7, "error: Task end labels require "
    # 		                 "SystemVerilog.");
    # 	      }
    # 	      delete[]p[7];
    # 	}
    # 	delete[]p[3];
    #       }
()


def p__embed0_task_declaration(p):
    '''_embed0_task_declaration : '''


    # { assert(current_task == 0);
    # 	current_task = pform_push_task_scope(@1, p[3], p[2]);
    #       }
()


def p__embed1_task_declaration(p):
    '''_embed1_task_declaration : '''


    # { current_task->set_ports(p[6]);
    # 	current_task_set_statement(@3, p[7]);
    # 	pform_set_this_class(@3, current_task);
    # 	pform_pop_scope();
    # 	current_task = 0;
    # 	if (p[7] && p[7]->size() > 1 && !gn_system_verilog()) {
    # 	      yyerror(@7, "error: Task body with multiple statements requires SystemVerilog.");
    # 	}
    # 	delete p[7];
    #       }
()


def p__embed2_task_declaration(p):
    '''_embed2_task_declaration : '''


    # { assert(current_task == 0);
    # 	current_task = pform_push_task_scope(@1, p[3], p[2]);
    #       }
()


def p__embed3_task_declaration(p):
    '''_embed3_task_declaration : '''


    # { current_task->set_ports(p[6]);
    # 	current_task_set_statement(@3, p[10]);
    # 	pform_set_this_class(@3, current_task);
    # 	pform_pop_scope();
    # 	current_task = 0;
    # 	if (p[10]) delete p[10];
    #       }
()


def p__embed4_task_declaration(p):
    '''_embed4_task_declaration : '''


    # { assert(current_task == 0);
    # 	current_task = pform_push_task_scope(@1, p[3], p[2]);
    #       }
()


def p__embed5_task_declaration(p):
    '''_embed5_task_declaration : '''


    # { current_task->set_ports(0);
    # 	current_task_set_statement(@3, p[9]);
    # 	pform_set_this_class(@3, current_task);
    # 	if (! current_task->method_of()) {
    # 	      cerr << @3 << ": warning: task definition for \"" << p[3]
    # 		   << "\" has an empty port declaration list!" << endl;
    # 	}
    # 	pform_pop_scope();
    # 	current_task = 0;
    # 	if (p[9]->size() > 1 && !gn_system_verilog()) {
    # 	      yyerror(@9, "error: Task body with multiple statements requires SystemVerilog.");
    # 	}
    # 	delete p[9];
    #       }
()


def p__embed6_task_declaration(p):
    '''_embed6_task_declaration : '''


    # {
    # 	if (current_task) {
    # 	      pform_pop_scope();
    # 	      current_task = 0;
    # 	}
    #       }
()


def p_tf_port_declaration_1(p):
    '''tf_port_declaration : port_direction K_reg_opt unsigned_signed_opt dimensions_opt list_of_identifiers ';' '''
    if(parse_debug):
        print('tf_port_declaration_1', list(p))


    # { vector<pform_tf_port_t>*tmp = pform_make_task_ports(@1, p[1],
    # 						p[2] ? IVL_VT_LOGIC :
    # 						     IVL_VT_NO_TYPE,
    # 						p[3], p[4], p[5]);
    # 	p[0] = tmp;
    #       }
()


def p_tf_port_declaration_2(p):
    '''tf_port_declaration : port_direction K_integer list_of_identifiers ';' '''
    if(parse_debug):
        print('tf_port_declaration_2', list(p))


    # { list<pform_range_t>*range_stub = make_range_from_width(integer_width);
    # 	vector<pform_tf_port_t>*tmp = pform_make_task_ports(@1, p[1], IVL_VT_LOGIC, true,
    # 						    range_stub, p[3], true);
    # 	p[0] = tmp;
    #       }
()


def p_tf_port_declaration_3(p):
    '''tf_port_declaration : port_direction K_time list_of_identifiers ';' '''
    if(parse_debug):
        print('tf_port_declaration_3', list(p))


    # { list<pform_range_t>*range_stub = make_range_from_width(64);
    # 	vector<pform_tf_port_t>*tmp = pform_make_task_ports(@1, p[1], IVL_VT_LOGIC, false,
    # 						   range_stub, p[3]);
    # 	p[0] = tmp;
    #       }
()


def p_tf_port_declaration_4(p):
    '''tf_port_declaration : port_direction real_or_realtime list_of_identifiers ';' '''
    if(parse_debug):
        print('tf_port_declaration_4', list(p))


    # { vector<pform_tf_port_t>*tmp = pform_make_task_ports(@1, p[1], IVL_VT_REAL, true,
    # 						   0, p[3]);
    # 	p[0] = tmp;
    #       }
()


def p_tf_port_declaration_5(p):
    '''tf_port_declaration : port_direction K_string list_of_identifiers ';' '''
    if(parse_debug):
        print('tf_port_declaration_5', list(p))


    # { vector<pform_tf_port_t>*tmp = pform_make_task_ports(@1, p[1], IVL_VT_STRING, true,
    # 						   0, p[3]);
    # 	p[0] = tmp;
    #       }
()


def p_tf_port_item_1(p):
    '''tf_port_item : port_direction_opt data_type_or_implicit IDENTIFIER dimensions_opt tf_port_item_expr_opt '''
    if(parse_debug):
        print('tf_port_item_1', list(p))


    # { vector<pform_tf_port_t>*tmp;
    # 	NetNet::PortType use_port_type = p[1];
    #         if ((use_port_type == NetNet::PIMPLICIT) && (gn_system_verilog() || (p[2] == 0)))
    #               use_port_type = port_declaration_context.port_type;
    # 	perm_string name = lex_strings.make(p[3]);
    # 	list<perm_string>* ilist = list_from_identifier(p[3]);
    #
    # 	if (use_port_type == NetNet::PIMPLICIT) {
    # 	      yyerror(@1, "error: missing task/function port direction.");
    # 	      use_port_type = NetNet::PINPUT; // for error recovery
    # 	}
    # 	if ((p[2] == 0) && (p[1]==NetNet::PIMPLICIT)) {
    # 		// Detect special case this is an undecorated
    # 		// identifier and we need to get the declaration from
    # 		// left context.
    # 	      if (p[4] != 0) {
    # 		    yyerror(@4, "internal error: How can there be an unpacked range here?\n");
    # 	      }
    # 	      tmp = pform_make_task_ports(@3, use_port_type,
    # 					  port_declaration_context.data_type,
    # 					  ilist);
    #
    # 	} else {
    # 		// Otherwise, the decorations for this identifier
    # 		// indicate the type. Save the type for any right
    # 		// context that may come later.
    # 	      port_declaration_context.port_type = use_port_type;
    # 	      if (p[2] == 0) {
    # 		    p[2] = new vector_type_t(IVL_VT_LOGIC, false, 0);
    # 		    FILE_NAME(p[2], @3);
    # 	      }
    # 	      port_declaration_context.data_type = p[2];
    # 	      tmp = pform_make_task_ports(@3, use_port_type, p[2], ilist);
    # 	}
    # 	if (p[4] != 0) {
    # 	      pform_set_reg_idx(name, p[4]);
    # 	}
    #
    # 	p[0] = tmp;
    # 	if (p[5]) {
    # 	      assert(tmp->size()==1);
    # 	      tmp->front().defe = p[5];
    # 	}
    #       }
()


def p_tf_port_item_2(p):
    '''tf_port_item : port_direction_opt data_type_or_implicit IDENTIFIER error '''
    if(parse_debug):
        print('tf_port_item_2', list(p))


    # { yyerror(@3, "error: Error in task/function port item after port name %s.", p[3]);
    # 	yyerrok;
    # 	p[0] = None
    #       }
()


def p_tf_port_item_expr_opt_1(p):
    '''tf_port_item_expr_opt : '=' expression '''
    if(parse_debug):
        print('tf_port_item_expr_opt_1', list(p))


    # { if (! gn_system_verilog()) {
    # 	      yyerror(@1, "error: Task/function default arguments require "
    # 	                  "SystemVerilog.");
    # 	}
    # 	p[0] = p[2];
    #       }
()


def p_tf_port_item_expr_opt_2(p):
    '''tf_port_item_expr_opt :  '''
    if(parse_debug):
        print('tf_port_item_expr_opt_2', list(p))


    # { p[0] = None }
()


def p_tf_port_list_1(p):
    '''tf_port_list : _embed0_tf_port_list tf_port_item_list '''
    if(parse_debug):
        print('tf_port_list_1', list(p))
    p[0] = p[2]


()


def p__embed0_tf_port_list(p):
    '''_embed0_tf_port_list : '''


    # { port_declaration_context.port_type = gn_system_verilog() ? NetNet::PINPUT : NetNet::PIMPLICIT;
    # 	port_declaration_context.data_type = 0;
    #       }
()


def p_tf_port_item_list_1(p):
    '''tf_port_item_list : tf_port_item_list ',' tf_port_item '''
    if(parse_debug):
        print('tf_port_item_list_1', list(p))


    # { vector<pform_tf_port_t>*tmp;
    # 	if (p[1] && p[3]) {
    # 	      size_t s1 = p[1]->size();
    # 	      tmp = p[1];
    # 	      tmp->resize(tmp->size()+p[3]->size());
    # 	      for (size_t idx = 0 ; idx < p[3]->size() ; idx += 1)
    # 		    tmp->at(s1+idx) = p[3]->at(idx);
    # 	      delete p[3];
    # 	} else if (p[1]) {
    # 	      tmp = p[1];
    # 	} else {
    # 	      tmp = p[3];
    # 	}
    # 	p[0] = tmp;
    #       }
()


def p_tf_port_item_list_2(p):
    '''tf_port_item_list : tf_port_item '''
    if(parse_debug):
        print('tf_port_item_list_2', list(p))
    p[0] = p[1]


()


def p_tf_port_item_list_3(p):
    '''tf_port_item_list : error ',' tf_port_item '''
    if(parse_debug):
        print('tf_port_item_list_3', list(p))


    # { yyerror(@2, "error: Syntax error in task/function port declaration.");
    # 	p[0] = p[3];
    #       }
()


def p_tf_port_item_list_4(p):
    '''tf_port_item_list : tf_port_item_list ',' '''
    if(parse_debug):
        print('tf_port_item_list_4', list(p))


    # { yyerror(@2, "error: NULL port declarations are not allowed.");
    # 	p[0] = p[1];
    #       }
()


def p_tf_port_item_list_5(p):
    '''tf_port_item_list : tf_port_item_list ';' '''
    if(parse_debug):
        print('tf_port_item_list_5', list(p))


    # { yyerror(@2, "error: ';' is an invalid port declaration separator.");
    # 	p[0] = p[1];
    #       }
()


def p_timeunits_declaration_1(p):
    '''timeunits_declaration : K_timeunit TIME_LITERAL ';' '''
    if(parse_debug):
        print('timeunits_declaration_1', list(p))


    # { pform_set_timeunit(p[2], allow_timeunit_decl); }
()


def p_timeunits_declaration_2(p):
    '''timeunits_declaration : K_timeunit TIME_LITERAL '/' TIME_LITERAL ';' '''
    if(parse_debug):
        print('timeunits_declaration_2', list(p))


    # { bool initial_decl = allow_timeunit_decl && allow_timeprec_decl;
    #         pform_set_timeunit(p[2], initial_decl);
    #         pform_set_timeprec(p[4], initial_decl);
    #       }
()


def p_timeunits_declaration_3(p):
    '''timeunits_declaration : K_timeprecision TIME_LITERAL ';' '''
    if(parse_debug):
        print('timeunits_declaration_3', list(p))


    # { pform_set_timeprec(p[2], allow_timeprec_decl); }
()


def p_timeunits_declaration_opt_1(p):
    '''timeunits_declaration_opt : %prec no_timeunits_declaration '''
    if(parse_debug > 2):
        print('timeunits_declaration_opt_1', list(p))


()


def p_timeunits_declaration_opt_2(p):
    '''timeunits_declaration_opt : timeunits_declaration %prec one_timeunits_declaration '''
    if(parse_debug):
        print('timeunits_declaration_opt_2', list(p))


()


def p_timeunits_declaration_opt_3(p):
    '''timeunits_declaration_opt : timeunits_declaration timeunits_declaration '''
    if(parse_debug):
        print('timeunits_declaration_opt_3', list(p))


()


def p_value_range_1(p):
    '''value_range : expression '''
    if(parse_debug):
        print('value_range_1', list(p))


    # { }
()


def p_value_range_2(p):
    '''value_range : '[' expression ':' expression ']' '''
    if(parse_debug):
        print('value_range_2', list(p))


    # { }
()


def p_variable_dimension_1(p):
    '''variable_dimension : '[' expression ':' expression ']' '''
    if(parse_debug):
        print('variable_dimension_1', list(p))
    # { list<pform_range_t> *tmp = new list<pform_range_t>;
    # 	pform_range_t index (p[2],p[4]);
    # 	tmp->push_back(index);
    # 	p[0] = tmp;
    #       }
    # XXX TODO: subscriptlist
    start = str(p[4])
    end = str(p[2])
    if end.endswith("-1"):
        end = end[:-2]
    elif end.isdigit():
        end = str(int(end)+1)
    else:
        end = "1+%s" % end
    p[0] = '[%s:%s]' % (start, end)  # python slice is LO:HI+1


()


def p_variable_dimension_2(p):
    '''variable_dimension : '[' expression ']' '''
    if(parse_debug):
        print('variable_dimension_2', list(p))


    # { // SystemVerilog canonical range
    # 	if (!gn_system_verilog()) {
    # 	      warn_count += 1;
    # 	      cerr << @2 << ": warning: Use of SystemVerilog [size] dimension. "
    # 		   << "Use at least -g2005-sv to remove this warning." << endl;
    # 	}
    # 	list<pform_range_t> *tmp = new list<pform_range_t>;
    # 	pform_range_t index;
    # 	index.first = new PENumber(new verinum((uint64_t)0, integer_width));
    # 	index.second = new PEBinary('-', p[2], new PENumber(new verinum((uint64_t)1, integer_width)));
    # 	tmp->push_back(index);
    # 	p[0] = tmp;
    #       }
()


def p_variable_dimension_3(p):
    '''variable_dimension : '[' ']' '''
    if(parse_debug):
        print('variable_dimension_3', list(p))


    # { list<pform_range_t> *tmp = new list<pform_range_t>;
    # 	pform_range_t index (0,0);
    # 	tmp->push_back(index);
    # 	p[0] = tmp;
    #       }
()


def p_variable_dimension_4(p):
    '''variable_dimension : '[' '$' ']' '''
    if(parse_debug):
        print('variable_dimension_4', list(p))


    # { // SystemVerilog queue
    # 	list<pform_range_t> *tmp = new list<pform_range_t>;
    # 	pform_range_t index (new PENull,0);
    # 	if (!gn_system_verilog()) {
    # 	      yyerror("error: Queue declarations require SystemVerilog.");
    # 	}
    # 	tmp->push_back(index);
    # 	p[0] = tmp;
    #       }
()


def p_variable_lifetime_1(p):
    '''variable_lifetime : lifetime '''
    if(parse_debug):
        print('variable_lifetime_1', list(p))


    # { if (!gn_system_verilog()) {
    # 	      yyerror(@1, "error: overriding the default variable lifetime "
    # 			  "requires SystemVerilog.");
    # 	} else if (p[1] != pform_peek_scope()->default_lifetime) {
    # 	      yyerror(@1, "sorry: overriding the default variable lifetime "
    # 			  "is not yet supported.");
    # 	}
    # 	var_lifetime = p[1];
    #       }
()


def p_attribute_list_opt_1(p):
    '''attribute_list_opt : attribute_instance_list '''
    if(parse_debug):
        print('attribute_list_opt_1', list(p))
    p[0] = p[1]


()


def p_attribute_list_opt_2(p):
    '''attribute_list_opt :  '''
    if(parse_debug > 2):
        print('attribute_list_opt_2', list(p))


    # { p[0] = None }
()


def p_attribute_instance_list_1(p):
    '''attribute_instance_list : K_PSTAR K_STARP '''
    if(parse_debug):
        print('attribute_instance_list_1', list(p))


    # { p[0] = None }
()


def p_attribute_instance_list_2(p):
    '''attribute_instance_list : K_PSTAR attribute_list K_STARP '''
    if(parse_debug):
        print('attribute_instance_list_2', list(p))
    p[0] = p[2]


()


def p_attribute_instance_list_3(p):
    '''attribute_instance_list : attribute_instance_list K_PSTAR K_STARP '''
    if(parse_debug):
        print('attribute_instance_list_3', list(p))
    p[0] = p[1]


()


def p_attribute_instance_list_4(p):
    '''attribute_instance_list : attribute_instance_list K_PSTAR attribute_list K_STARP '''
    if(parse_debug):
        print('attribute_instance_list_4', list(p))


    # { list<named_pexpr_t>*tmp = p[1];
    # 	if (tmp) {
    # 	    tmp->splice(tmp->end(), *p[3]);
    # 	    delete p[3];
    # 	    p[0] = tmp;
    # 	} else p[0] = p[3];
    #       }
()


def p_attribute_list_1(p):
    '''attribute_list : attribute_list ',' attribute '''
    if(parse_debug):
        print('attribute_list_1', list(p))


    # { list<named_pexpr_t>*tmp = p[1];
    #         tmp->push_back(*p[3]);
    # 	delete p[3];
    # 	p[0] = tmp;
    #       }
()


def p_attribute_list_2(p):
    '''attribute_list : attribute '''
    if(parse_debug):
        print('attribute_list_2', list(p))


    # { list<named_pexpr_t>*tmp = new list<named_pexpr_t>;
    #         tmp->push_back(*p[1]);
    # 	delete p[1];
    # 	p[0] = tmp;
    #       }
()


def p_attribute_1(p):
    '''attribute : IDENTIFIER '''
    if(parse_debug):
        print('attribute_1', list(p))


    # { named_pexpr_t*tmp = new named_pexpr_t;
    # 		  tmp->name = lex_strings.make(p[1]);
    # 		  tmp->parm = 0;
    # 		  delete[]p[1];
    # 		  p[0] = tmp;
    # 		}
()


def p_attribute_2(p):
    '''attribute : IDENTIFIER '=' expression '''
    if(parse_debug):
        print('attribute_2', list(p))


    # { PExpr*tmp = p[3];
    # 		  named_pexpr_t*tmp2 = new named_pexpr_t;
    # 		  tmp2->name = lex_strings.make(p[1]);
    # 		  tmp2->parm = tmp;
    # 		  delete[]p[1];
    # 		  p[0] = tmp2;
    # 		}
()


def p_block_item_decl_1(p):
    '''block_item_decl : data_type register_variable_list ';' '''
    if(parse_debug):
        print('block_item_decl_1', list(p))


    # { if (p[1]) pform_set_data_type(@1, p[1], p[2], NetNet::REG, attributes_in_context);
    #       }
()


def p_block_item_decl_2(p):
    '''block_item_decl : variable_lifetime data_type register_variable_list ';' '''
    if(parse_debug):
        print('block_item_decl_2', list(p))


    # { if (p[2]) pform_set_data_type(@2, p[2], p[3], NetNet::REG, attributes_in_context);
    # 	var_lifetime = LexicalScope::INHERITED;
    #       }
()


def p_block_item_decl_3(p):
    '''block_item_decl : K_reg data_type register_variable_list ';' '''
    if(parse_debug):
        print('block_item_decl_3', list(p))


    # { if (p[2]) pform_set_data_type(@2, p[2], p[3], NetNet::REG, attributes_in_context);
    #       }
()


def p_block_item_decl_4(p):
    '''block_item_decl : variable_lifetime K_reg data_type register_variable_list ';' '''
    if(parse_debug):
        print('block_item_decl_4', list(p))


    # { if (p[3]) pform_set_data_type(@3, p[3], p[4], NetNet::REG, attributes_in_context);
    # 	var_lifetime = LexicalScope::INHERITED;
    #       }
()


def p_block_item_decl_5(p):
    '''block_item_decl : K_event event_variable_list ';' '''
    if(parse_debug):
        print('block_item_decl_5', list(p))


    # { if (p[2]) pform_make_events(p[2], @1.text, @1.first_line);
    #       }
()


def p_block_item_decl_6(p):
    '''block_item_decl : K_parameter param_type parameter_assign_list ';' '''
    if(parse_debug):
        print('block_item_decl_6', list(p))


()


def p_block_item_decl_7(p):
    '''block_item_decl : K_localparam param_type localparam_assign_list ';' '''
    if(parse_debug):
        print('block_item_decl_7', list(p))


()


def p_block_item_decl_8(p):
    '''block_item_decl : type_declaration '''
    if(parse_debug):
        print('block_item_decl_8', list(p))


()


def p_block_item_decl_9(p):
    '''block_item_decl : K_integer error ';' '''
    if(parse_debug):
        print('block_item_decl_9', list(p))


    # { yyerror(@1, "error: syntax error in integer variable list.");
    # 	yyerrok;
    #       }
()


def p_block_item_decl_10(p):
    '''block_item_decl : K_time error ';' '''
    if(parse_debug):
        print('block_item_decl_10', list(p))


    # { yyerror(@1, "error: syntax error in time variable list.");
    # 	yyerrok;
    #       }
()


def p_block_item_decl_11(p):
    '''block_item_decl : K_parameter error ';' '''
    if(parse_debug):
        print('block_item_decl_11', list(p))


    # { yyerror(@1, "error: syntax error in parameter list.");
    # 	yyerrok;
    #       }
()


def p_block_item_decl_12(p):
    '''block_item_decl : K_localparam error ';' '''
    if(parse_debug):
        print('block_item_decl_12', list(p))


    # { yyerror(@1, "error: syntax error localparam list.");
    # 	yyerrok;
    #       }
()


def p_block_item_decls_1(p):
    '''block_item_decls : block_item_decl '''
    if(parse_debug):
        print('block_item_decls_1', list(p))


()


def p_block_item_decls_2(p):
    '''block_item_decls : block_item_decls block_item_decl '''
    if(parse_debug):
        print('block_item_decls_2', list(p))


()


def p_block_item_decls_opt_1(p):
    '''block_item_decls_opt : block_item_decls '''
    if(parse_debug):
        print('block_item_decls_opt_1', list(p))
    p[0] = True


()


def p_block_item_decls_opt_2(p):
    '''block_item_decls_opt :  '''
    if(parse_debug):
        print('block_item_decls_opt_2', list(p))
    p[0] = False


()


def p_type_declaration_1(p):
    '''type_declaration : K_typedef data_type IDENTIFIER dimensions_opt ';' '''
    if(parse_debug):
        print('type_declaration_1', list(p))


    # { perm_string name = lex_strings.make(p[3]);
    # 	pform_set_typedef(name, p[2], p[4]);
    # 	delete[]p[3];
    #       }
()


def p_type_declaration_2(p):
    '''type_declaration : K_typedef data_type TYPE_IDENTIFIER ';' '''
    if(parse_debug):
        print('type_declaration_2', list(p))


    # { perm_string name = lex_strings.make(p[3].text);
    # 	if (pform_test_type_identifier_local(name)) {
    # 	      yyerror(@3, "error: Typedef identifier \"%s\" is already a type name.", p[3].text);
    #
    # 	} else {
    # 	      pform_set_typedef(name, p[2], NULL);
    # 	}
    # 	delete[]p[3].text;
    #       }
()


def p_type_declaration_3(p):
    '''type_declaration : K_typedef K_class IDENTIFIER ';' '''
    if(parse_debug):
        print('type_declaration_3', list(p))


    # { // Create a synthetic typedef for the class name so that the
    # 	// lexor detects the name as a type.
    # 	perm_string name = lex_strings.make(p[3]);
    # 	class_type_t*tmp = new class_type_t(name);
    # 	FILE_NAME(tmp, @3);
    # 	pform_set_typedef(name, tmp, NULL);
    # 	delete[]p[3];
    #       }
()


def p_type_declaration_4(p):
    '''type_declaration : K_typedef K_enum IDENTIFIER ';' '''
    if(parse_debug):
        print('type_declaration_4', list(p))


    # { yyerror(@1, "sorry: Enum forward declarations not supported yet."); }
()


def p_type_declaration_5(p):
    '''type_declaration : K_typedef K_struct IDENTIFIER ';' '''
    if(parse_debug):
        print('type_declaration_5', list(p))


    # { yyerror(@1, "sorry: Struct forward declarations not supported yet."); }
()


def p_type_declaration_6(p):
    '''type_declaration : K_typedef K_union IDENTIFIER ';' '''
    if(parse_debug):
        print('type_declaration_6', list(p))


    # { yyerror(@1, "sorry: Union forward declarations not supported yet."); }
()


def p_type_declaration_7(p):
    '''type_declaration : K_typedef IDENTIFIER ';' '''
    if(parse_debug):
        print('type_declaration_7', list(p))


    # { // Create a synthetic typedef for the class name so that the
    # 	// lexor detects the name as a type.
    # 	perm_string name = lex_strings.make(p[2]);
    # 	class_type_t*tmp = new class_type_t(name);
    # 	FILE_NAME(tmp, @2);
    # 	pform_set_typedef(name, tmp, NULL);
    # 	delete[]p[2];
    #       }
()


def p_type_declaration_8(p):
    '''type_declaration : K_typedef error ';' '''
    if(parse_debug):
        print('type_declaration_8', list(p))


    # { yyerror(@2, "error: Syntax error in typedef clause.");
    # 	yyerrok;
    #       }
()


def p_enum_data_type_1(p):
    '''enum_data_type : K_enum '{' enum_name_list '}' '''
    if(parse_debug):
        print('enum_data_type_1', list(p))


    # { enum_type_t*enum_type = new enum_type_t;
    # 	FILE_NAME(enum_type, @1);
    # 	enum_type->names .reset(p[3]);
    # 	enum_type->base_type = IVL_VT_BOOL;
    # 	enum_type->signed_flag = true;
    # 	enum_type->integer_flag = false;
    # 	enum_type->range.reset(make_range_from_width(32));
    # 	p[0] = enum_type;
    #       }
()


def p_enum_data_type_2(p):
    '''enum_data_type : K_enum atom2_type signed_unsigned_opt '{' enum_name_list '}' '''
    if(parse_debug):
        print('enum_data_type_2', list(p))


    # { enum_type_t*enum_type = new enum_type_t;
    # 	FILE_NAME(enum_type, @1);
    # 	enum_type->names .reset(p[5]);
    # 	enum_type->base_type = IVL_VT_BOOL;
    # 	enum_type->signed_flag = p[3];
    # 	enum_type->integer_flag = false;
    # 	enum_type->range.reset(make_range_from_width(p[2]));
    # 	p[0] = enum_type;
    #       }
()


def p_enum_data_type_3(p):
    '''enum_data_type : K_enum K_integer signed_unsigned_opt '{' enum_name_list '}' '''
    if(parse_debug):
        print('enum_data_type_3', list(p))


    # { enum_type_t*enum_type = new enum_type_t;
    # 	FILE_NAME(enum_type, @1);
    # 	enum_type->names .reset(p[5]);
    # 	enum_type->base_type = IVL_VT_LOGIC;
    # 	enum_type->signed_flag = p[3];
    # 	enum_type->integer_flag = true;
    # 	enum_type->range.reset(make_range_from_width(integer_width));
    # 	p[0] = enum_type;
    #       }
()


def p_enum_data_type_4(p):
    '''enum_data_type : K_enum K_logic unsigned_signed_opt dimensions_opt '{' enum_name_list '}' '''
    if(parse_debug):
        print('enum_data_type_4', list(p))


    # { enum_type_t*enum_type = new enum_type_t;
    # 	FILE_NAME(enum_type, @1);
    # 	enum_type->names .reset(p[6]);
    # 	enum_type->base_type = IVL_VT_LOGIC;
    # 	enum_type->signed_flag = p[3];
    # 	enum_type->integer_flag = false;
    # 	enum_type->range.reset(p[4] ? p[4] : make_range_from_width(1));
    # 	p[0] = enum_type;
    #       }
()


def p_enum_data_type_5(p):
    '''enum_data_type : K_enum K_reg unsigned_signed_opt dimensions_opt '{' enum_name_list '}' '''
    if(parse_debug):
        print('enum_data_type_5', list(p))


    # { enum_type_t*enum_type = new enum_type_t;
    # 	FILE_NAME(enum_type, @1);
    # 	enum_type->names .reset(p[6]);
    # 	enum_type->base_type = IVL_VT_LOGIC;
    # 	enum_type->signed_flag = p[3];
    # 	enum_type->integer_flag = false;
    # 	enum_type->range.reset(p[4] ? p[4] : make_range_from_width(1));
    # 	p[0] = enum_type;
    #       }
()


def p_enum_data_type_6(p):
    '''enum_data_type : K_enum K_bit unsigned_signed_opt dimensions_opt '{' enum_name_list '}' '''
    if(parse_debug):
        print('enum_data_type_6', list(p))


    # { enum_type_t*enum_type = new enum_type_t;
    # 	FILE_NAME(enum_type, @1);
    # 	enum_type->names .reset(p[6]);
    # 	enum_type->base_type = IVL_VT_BOOL;
    # 	enum_type->signed_flag = p[3];
    # 	enum_type->integer_flag = false;
    # 	enum_type->range.reset(p[4] ? p[4] : make_range_from_width(1));
    # 	p[0] = enum_type;
    #       }
()


def p_enum_name_list_1(p):
    '''enum_name_list : enum_name '''
    if(parse_debug):
        print('enum_name_list_1', list(p))


    # { p[0] = p[1];
    #       }
()


def p_enum_name_list_2(p):
    '''enum_name_list : enum_name_list ',' enum_name '''
    if(parse_debug):
        print('enum_name_list_2', list(p))


    # { list<named_pexpr_t>*lst = p[1];
    # 	lst->splice(lst->end(), *p[3]);
    # 	delete p[3];
    # 	p[0] = lst;
    #       }
()


def p_pos_neg_number_1(p):
    '''pos_neg_number : number '''
    if(parse_debug):
        print('pos_neg_number_1', list(p))


    # { p[0] = p[1];
    #       }
()


def p_pos_neg_number_2(p):
    '''pos_neg_number : '-' number '''
    if(parse_debug):
        print('pos_neg_number_2', list(p))


    # { verinum tmp = -(*(p[2]));
    # 	*(p[2]) = tmp;
    # 	p[0] = p[2];
    #       }
()


def p_enum_name_1(p):
    '''enum_name : IDENTIFIER '''
    if(parse_debug):
        print('enum_name_1', list(p))


    # { perm_string name = lex_strings.make(p[1]);
    # 	delete[]p[1];
    # 	p[0] = make_named_number(name);
    #       }
()


def p_enum_name_2(p):
    '''enum_name : IDENTIFIER '[' pos_neg_number ']' '''
    if(parse_debug):
        print('enum_name_2', list(p))


    # { perm_string name = lex_strings.make(p[1]);
    # 	long count = check_enum_seq_value(@1, p[3], false);
    # 	delete[]p[1];
    # 	p[0] = make_named_numbers(name, 0, count-1);
    # 	delete p[3];
    #       }
()


def p_enum_name_3(p):
    '''enum_name : IDENTIFIER '[' pos_neg_number ':' pos_neg_number ']' '''
    if(parse_debug):
        print('enum_name_3', list(p))


    # { perm_string name = lex_strings.make(p[1]);
    # 	p[0] = make_named_numbers(name, check_enum_seq_value(@1, p[3], true),
    # 	                              check_enum_seq_value(@1, p[5], true));
    # 	delete[]p[1];
    # 	delete p[3];
    # 	delete p[5];
    #       }
()


def p_enum_name_4(p):
    '''enum_name : IDENTIFIER '=' expression '''
    if(parse_debug):
        print('enum_name_4', list(p))


    # { perm_string name = lex_strings.make(p[1]);
    # 	delete[]p[1];
    # 	p[0] = make_named_number(name, p[3]);
    #       }
()


def p_enum_name_5(p):
    '''enum_name : IDENTIFIER '[' pos_neg_number ']' '=' expression '''
    if(parse_debug):
        print('enum_name_5', list(p))


    # { perm_string name = lex_strings.make(p[1]);
    # 	long count = check_enum_seq_value(@1, p[3], false);
    # 	p[0] = make_named_numbers(name, 0, count-1, p[6]);
    # 	delete[]p[1];
    # 	delete p[3];
    #       }
()


def p_enum_name_6(p):
    '''enum_name : IDENTIFIER '[' pos_neg_number ':' pos_neg_number ']' '=' expression '''
    if(parse_debug):
        print('enum_name_6', list(p))


    # { perm_string name = lex_strings.make(p[1]);
    # 	p[0] = make_named_numbers(name, check_enum_seq_value(@1, p[3], true),
    # 	                              check_enum_seq_value(@1, p[5], true), p[8]);
    # 	delete[]p[1];
    # 	delete p[3];
    # 	delete p[5];
    #       }
()


def p_struct_data_type_1(p):
    '''struct_data_type : K_struct K_packed_opt '{' struct_union_member_list '}' '''
    if(parse_debug):
        print('struct_data_type_1', list(p))


    # { struct_type_t*tmp = new struct_type_t;
    # 	FILE_NAME(tmp, @1);
    # 	tmp->packed_flag = p[2];
    # 	tmp->union_flag = false;
    # 	tmp->members .reset(p[4]);
    # 	p[0] = tmp;
    #       }
()


def p_struct_data_type_2(p):
    '''struct_data_type : K_union K_packed_opt '{' struct_union_member_list '}' '''
    if(parse_debug):
        print('struct_data_type_2', list(p))


    # { struct_type_t*tmp = new struct_type_t;
    # 	FILE_NAME(tmp, @1);
    # 	tmp->packed_flag = p[2];
    # 	tmp->union_flag = true;
    # 	tmp->members .reset(p[4]);
    # 	p[0] = tmp;
    #       }
()


def p_struct_data_type_3(p):
    '''struct_data_type : K_struct K_packed_opt '{' error '}' '''
    if(parse_debug):
        print('struct_data_type_3', list(p))


    # { yyerror(@3, "error: Errors in struct member list.");
    # 	yyerrok;
    # 	struct_type_t*tmp = new struct_type_t;
    # 	FILE_NAME(tmp, @1);
    # 	tmp->packed_flag = p[2];
    # 	tmp->union_flag = false;
    # 	p[0] = tmp;
    #       }
()


def p_struct_data_type_4(p):
    '''struct_data_type : K_union K_packed_opt '{' error '}' '''
    if(parse_debug):
        print('struct_data_type_4', list(p))


    # { yyerror(@3, "error: Errors in union member list.");
    # 	yyerrok;
    # 	struct_type_t*tmp = new struct_type_t;
    # 	FILE_NAME(tmp, @1);
    # 	tmp->packed_flag = p[2];
    # 	tmp->union_flag = true;
    # 	p[0] = tmp;
    #       }
()


def p_struct_union_member_list_1(p):
    '''struct_union_member_list : struct_union_member_list struct_union_member '''
    if(parse_debug):
        print('struct_union_member_list_1', list(p))


    # { list<struct_member_t*>*tmp = p[1];
    # 	tmp->push_back(p[2]);
    # 	p[0] = tmp;
    #       }
()


def p_struct_union_member_list_2(p):
    '''struct_union_member_list : struct_union_member '''
    if(parse_debug):
        print('struct_union_member_list_2', list(p))


    # { list<struct_member_t*>*tmp = new list<struct_member_t*>;
    # 	tmp->push_back(p[1]);
    # 	p[0] = tmp;
    #       }
()


def p_struct_union_member_1(p):
    '''struct_union_member : attribute_list_opt data_type list_of_variable_decl_assignments ';' '''
    if(parse_debug):
        print('struct_union_member_1', list(p))


    # { struct_member_t*tmp = new struct_member_t;
    # 	FILE_NAME(tmp, @2);
    # 	tmp->type  .reset(p[2]);
    # 	tmp->names .reset(p[3]);
    # 	p[0] = tmp;
    #       }
()


def p_struct_union_member_2(p):
    '''struct_union_member : error ';' '''
    if(parse_debug):
        print('struct_union_member_2', list(p))


    # { yyerror(@2, "Error in struct/union member.");
    # 	yyerrok;
    # 	p[0] = None
    #       }
()


def p_case_item_1(p):
    '''case_item : expression_list_proper ':' statement_or_null '''
    if(parse_debug):
        print('case_item_1', list(p))


    # { PCase::Item*tmp = new PCase::Item;
    # 		  tmp->expr = *p[1];
    # 		  tmp->stat = p[3];
    # 		  delete p[1];
    # 		  p[0] = tmp;
    # 		}
()


def p_case_item_2(p):
    '''case_item : K_default ':' statement_or_null '''
    if(parse_debug):
        print('case_item_2', list(p))


    # { PCase::Item*tmp = new PCase::Item;
    # 		  tmp->stat = p[3];
    # 		  p[0] = tmp;
    # 		}
()


def p_case_item_3(p):
    '''case_item : K_default statement_or_null '''
    if(parse_debug):
        print('case_item_3', list(p))


    # { PCase::Item*tmp = new PCase::Item;
    # 		  tmp->stat = p[2];
    # 		  p[0] = tmp;
    # 		}
()


def p_case_item_4(p):
    '''case_item : error ':' statement_or_null '''
    if(parse_debug):
        print('case_item_4', list(p))


    # { yyerror(@2, "error: Incomprehensible case expression.");
    # 		  yyerrok;
    # 		}
()


def p_case_items_1(p):
    '''case_items : case_items case_item '''
    if(parse_debug):
        print('case_items_1', list(p))


    # { svector<PCase::Item*>*tmp;
    # 		  tmp = new svector<PCase::Item*>(*p[1], p[2]);
    # 		  delete p[1];
    # 		  p[0] = tmp;
    # 		}
()


def p_case_items_2(p):
    '''case_items : case_item '''
    if(parse_debug):
        print('case_items_2', list(p))


    # { svector<PCase::Item*>*tmp = new svector<PCase::Item*>(1);
    # 		  (*tmp)[0] = p[1];
    # 		  p[0] = tmp;
    # 		}
()


def p_charge_strength_1(p):
    '''charge_strength : '(' K_small ')' '''
    if(parse_debug):
        print('charge_strength_1', list(p))


()


def p_charge_strength_2(p):
    '''charge_strength : '(' K_medium ')' '''
    if(parse_debug):
        print('charge_strength_2', list(p))


()


def p_charge_strength_3(p):
    '''charge_strength : '(' K_large ')' '''
    if(parse_debug):
        print('charge_strength_3', list(p))


()


def p_charge_strength_opt_1(p):
    '''charge_strength_opt : charge_strength '''
    if(parse_debug):
        print('charge_strength_opt_1', list(p))


()


def p_charge_strength_opt_2(p):
    '''charge_strength_opt :  '''
    if(parse_debug):
        print('charge_strength_opt_2', list(p))


()


def p_defparam_assign_1(p):
    '''defparam_assign : hierarchy_identifier '=' expression '''
    if(parse_debug):
        print('defparam_assign_1', list(p))


    # { pform_set_defparam(*p[1], p[3]);
    # 		  delete p[1];
    # 		}
()


def p_defparam_assign_list_1(p):
    '''defparam_assign_list : defparam_assign '''
    if(parse_debug):
        print('defparam_assign_list_1', list(p))


()


def p_defparam_assign_list_2(p):
    '''defparam_assign_list : dimensions defparam_assign '''
    if(parse_debug):
        print('defparam_assign_list_2', list(p))


    # { yyerror(@1, "error: defparam may not include a range.");
    # 	delete p[1];
    #       }
()


def p_defparam_assign_list_3(p):
    '''defparam_assign_list : defparam_assign_list ',' defparam_assign '''
    if(parse_debug):
        print('defparam_assign_list_3', list(p))


()


def p_delay1_1(p):
    '''delay1 : '#' delay_value_simple '''
    if(parse_debug):
        print('delay1_1', list(p))


    # { list<PExpr*>*tmp = new list<PExpr*>;
    # 		  tmp->push_back(p[2]);
    # 		  p[0] = tmp;
    # 		}
()


def p_delay1_2(p):
    '''delay1 : '#' '(' delay_value ')' '''
    if(parse_debug):
        print('delay1_2', list(p))


    # { list<PExpr*>*tmp = new list<PExpr*>;
    # 		  tmp->push_back(p[3]);
    # 		  p[0] = tmp;
    # 		}
()


def p_delay3_1(p):
    '''delay3 : '#' delay_value_simple '''
    if(parse_debug):
        print('delay3_1', list(p))


    # { list<PExpr*>*tmp = new list<PExpr*>;
    # 		  tmp->push_back(p[2]);
    # 		  p[0] = tmp;
    # 		}
()


def p_delay3_2(p):
    '''delay3 : '#' '(' delay_value ')' '''
    if(parse_debug):
        print('delay3_2', list(p))


    # { list<PExpr*>*tmp = new list<PExpr*>;
    # 		  tmp->push_back(p[3]);
    # 		  p[0] = tmp;
    # 		}
()


def p_delay3_3(p):
    '''delay3 : '#' '(' delay_value ',' delay_value ')' '''
    if(parse_debug):
        print('delay3_3', list(p))


    # { list<PExpr*>*tmp = new list<PExpr*>;
    # 		  tmp->push_back(p[3]);
    # 		  tmp->push_back(p[5]);
    # 		  p[0] = tmp;
    # 		}
()


def p_delay3_4(p):
    '''delay3 : '#' '(' delay_value ',' delay_value ',' delay_value ')' '''
    if(parse_debug):
        print('delay3_4', list(p))


    # { list<PExpr*>*tmp = new list<PExpr*>;
    # 		  tmp->push_back(p[3]);
    # 		  tmp->push_back(p[5]);
    # 		  tmp->push_back(p[7]);
    # 		  p[0] = tmp;
    # 		}
()


def p_delay3_opt_1(p):
    '''delay3_opt : delay3 '''
    if(parse_debug):
        print('delay3_opt_1', list(p))
    p[0] = p[1]


()


def p_delay3_opt_2(p):
    '''delay3_opt :  '''
    if(parse_debug > 2):
        print('delay3_opt_2', list(p))


    # { p[0] = None }
()


def p_delay_value_list_1(p):
    '''delay_value_list : delay_value '''
    if(parse_debug):
        print('delay_value_list_1', list(p))


    # { list<PExpr*>*tmp = new list<PExpr*>;
    # 	tmp->push_back(p[1]);
    # 	p[0] = tmp;
    #       }
()


def p_delay_value_list_2(p):
    '''delay_value_list : delay_value_list ',' delay_value '''
    if(parse_debug):
        print('delay_value_list_2', list(p))


    # { list<PExpr*>*tmp = p[1];
    # 	tmp->push_back(p[3]);
    # 	p[0] = tmp;
    #       }
()


def p_delay_value_1(p):
    '''delay_value : expression '''
    if(parse_debug):
        print('delay_value_1', list(p))


    # { PExpr*tmp = p[1];
    # 		  p[0] = tmp;
    # 		}
()


def p_delay_value_2(p):
    '''delay_value : expression ':' expression ':' expression '''
    if(parse_debug):
        print('delay_value_2', list(p))


    # { p[0] = pform_select_mtm_expr(p[1], p[3], p[5]); }
()


def p_delay_value_simple_1(p):
    '''delay_value_simple : DEC_NUMBER '''
    if(parse_debug):
        print('delay_value_simple_1', list(p))


    # { verinum*tmp = p[1];
    # 		  if (tmp == 0) {
    # 			yyerror(@1, "internal error: delay.");
    # 			p[0] = None
    # 		  } else {
    # 			p[0] = new PENumber(tmp);
    # 			FILE_NAME(p[0], @1);
    # 		  }
    # 		  based_size = 0;
    # 		}
()


def p_delay_value_simple_2(p):
    '''delay_value_simple : REALTIME '''
    if(parse_debug):
        print('delay_value_simple_2', list(p))


    # { verireal*tmp = p[1];
    # 		  if (tmp == 0) {
    # 			yyerror(@1, "internal error: delay.");
    # 			p[0] = None
    # 		  } else {
    # 			p[0] = new PEFNumber(tmp);
    # 			FILE_NAME(p[0], @1);
    # 		  }
    # 		}
()


def p_delay_value_simple_3(p):
    '''delay_value_simple : IDENTIFIER '''
    if(parse_debug):
        print('delay_value_simple_3', list(p))


    # { PEIdent*tmp = new PEIdent(lex_strings.make(p[1]));
    # 		  FILE_NAME(tmp, @1);
    # 		  p[0] = tmp;
    # 		  delete[]p[1];
    # 		}
()


def p_delay_value_simple_4(p):
    '''delay_value_simple : TIME_LITERAL '''
    if(parse_debug):
        print('delay_value_simple_4', list(p))


    # { int unit;
    #
    # 		  based_size = 0;
    # 		  p[0]         = 0;
    # 		  if (p[1] == 0 || !get_time_unit(p[1], unit))
    # 			yyerror(@1, "internal error: delay.");
    # 		  else {
    # 			double p = pow(10.0,
    # 			               (double)(unit - pform_get_timeunit()));
    # 			double time = atof(p[1]) * p;
    #
    # 			verireal *v = new verireal(time);
    # 			p[0] = new PEFNumber(v);
    # 			FILE_NAME(p[0], @1);
    # 		  }
    # 		}
()


def p_optional_semicolon_1(p):
    '''optional_semicolon : ';' '''
    if(parse_debug):
        print('optional_semicolon_1', list(p))


()


def p_optional_semicolon_2(p):
    '''optional_semicolon :  '''
    if(parse_debug):
        print('optional_semicolon_2', list(p))


()


def p_discipline_declaration_1(p):
    '''discipline_declaration : K_discipline IDENTIFIER optional_semicolon _embed0_discipline_declaration discipline_items K_enddiscipline '''
    if(parse_debug):
        print('discipline_declaration_1', list(p))


    # { pform_end_discipline(@1); delete[] p[2]; }
()


def p__embed0_discipline_declaration(p):
    '''_embed0_discipline_declaration : '''


    # { pform_start_discipline(p[2]); }
()


def p_discipline_items_1(p):
    '''discipline_items : discipline_items discipline_item '''
    if(parse_debug):
        print('discipline_items_1', list(p))


()


def p_discipline_items_2(p):
    '''discipline_items : discipline_item '''
    if(parse_debug):
        print('discipline_items_2', list(p))


()


def p_discipline_item_1(p):
    '''discipline_item : K_domain K_discrete ';' '''
    if(parse_debug):
        print('discipline_item_1', list(p))


    # { pform_discipline_domain(@1, IVL_DIS_DISCRETE); }
()


def p_discipline_item_2(p):
    '''discipline_item : K_domain K_continuous ';' '''
    if(parse_debug):
        print('discipline_item_2', list(p))


    # { pform_discipline_domain(@1, IVL_DIS_CONTINUOUS); }
()


def p_discipline_item_3(p):
    '''discipline_item : K_potential IDENTIFIER ';' '''
    if(parse_debug):
        print('discipline_item_3', list(p))


    # { pform_discipline_potential(@1, p[2]); delete[] p[2]; }
()


def p_discipline_item_4(p):
    '''discipline_item : K_flow IDENTIFIER ';' '''
    if(parse_debug):
        print('discipline_item_4', list(p))


    # { pform_discipline_flow(@1, p[2]); delete[] p[2]; }
()


def p_nature_declaration_1(p):
    '''nature_declaration : K_nature IDENTIFIER optional_semicolon _embed0_nature_declaration nature_items K_endnature '''
    if(parse_debug):
        print('nature_declaration_1', list(p))


    # { pform_end_nature(@1); delete[] p[2]; }
()


def p__embed0_nature_declaration(p):
    '''_embed0_nature_declaration : '''


    # { pform_start_nature(p[2]); }
()


def p_nature_items_1(p):
    '''nature_items : nature_items nature_item '''
    if(parse_debug):
        print('nature_items_1', list(p))


()


def p_nature_items_2(p):
    '''nature_items : nature_item '''
    if(parse_debug):
        print('nature_items_2', list(p))


()


def p_nature_item_1(p):
    '''nature_item : K_units '=' STRING ';' '''
    if(parse_debug):
        print('nature_item_1', list(p))


    # { delete[] p[3]; }
()


def p_nature_item_2(p):
    '''nature_item : K_abstol '=' expression ';' '''
    if(parse_debug):
        print('nature_item_2', list(p))


()


def p_nature_item_3(p):
    '''nature_item : K_access '=' IDENTIFIER ';' '''
    if(parse_debug):
        print('nature_item_3', list(p))


    # { pform_nature_access(@1, p[3]); delete[] p[3]; }
()


def p_nature_item_4(p):
    '''nature_item : K_idt_nature '=' IDENTIFIER ';' '''
    if(parse_debug):
        print('nature_item_4', list(p))


    # { delete[] p[3]; }
()


def p_nature_item_5(p):
    '''nature_item : K_ddt_nature '=' IDENTIFIER ';' '''
    if(parse_debug):
        print('nature_item_5', list(p))


    # { delete[] p[3]; }
()


def p_config_declaration_1(p):
    '''config_declaration : K_config IDENTIFIER ';' K_design lib_cell_identifiers ';' list_of_config_rule_statements K_endconfig '''
    if(parse_debug):
        print('config_declaration_1', list(p))


    # { cerr << @1 << ": sorry: config declarations are not supported and "
    #                 "will be skipped." << endl;
    # 	delete[] p[2];
    #       }
()


def p_lib_cell_identifiers_1(p):
    '''lib_cell_identifiers :  '''
    if(parse_debug):
        print('lib_cell_identifiers_1', list(p))


()


def p_lib_cell_identifiers_2(p):
    '''lib_cell_identifiers : lib_cell_identifiers lib_cell_id '''
    if(parse_debug):
        print('lib_cell_identifiers_2', list(p))


()


def p_list_of_config_rule_statements_1(p):
    '''list_of_config_rule_statements :  '''
    if(parse_debug):
        print('list_of_config_rule_statements_1', list(p))


()


def p_list_of_config_rule_statements_2(p):
    '''list_of_config_rule_statements : list_of_config_rule_statements config_rule_statement '''
    if(parse_debug):
        print('list_of_config_rule_statements_2', list(p))


()


def p_config_rule_statement_1(p):
    '''config_rule_statement : K_default K_liblist list_of_libraries ';' '''
    if(parse_debug):
        print('config_rule_statement_1', list(p))


()


def p_config_rule_statement_2(p):
    '''config_rule_statement : K_instance hierarchy_identifier K_liblist list_of_libraries ';' '''
    if(parse_debug):
        print('config_rule_statement_2', list(p))


    # { delete p[2]; }
()


def p_config_rule_statement_3(p):
    '''config_rule_statement : K_instance hierarchy_identifier K_use lib_cell_id opt_config ';' '''
    if(parse_debug):
        print('config_rule_statement_3', list(p))


    # { delete p[2]; }
()


def p_config_rule_statement_4(p):
    '''config_rule_statement : K_cell lib_cell_id K_liblist list_of_libraries ';' '''
    if(parse_debug):
        print('config_rule_statement_4', list(p))


()


def p_config_rule_statement_5(p):
    '''config_rule_statement : K_cell lib_cell_id K_use lib_cell_id opt_config ';' '''
    if(parse_debug):
        print('config_rule_statement_5', list(p))


()


def p_opt_config_1(p):
    '''opt_config :  '''
    if(parse_debug):
        print('opt_config_1', list(p))


()


def p_opt_config_2(p):
    '''opt_config : ':' K_config '''
    if(parse_debug):
        print('opt_config_2', list(p))


()


def p_lib_cell_id_1(p):
    '''lib_cell_id : IDENTIFIER '''
    if(parse_debug):
        print('lib_cell_id_1', list(p))


    # { delete[] p[1]; }
()


def p_lib_cell_id_2(p):
    '''lib_cell_id : IDENTIFIER '.' IDENTIFIER '''
    if(parse_debug):
        print('lib_cell_id_2', list(p))


    # { delete[] p[1]; delete[] p[3]; }
()


def p_list_of_libraries_1(p):
    '''list_of_libraries :  '''
    if(parse_debug):
        print('list_of_libraries_1', list(p))


()


def p_list_of_libraries_2(p):
    '''list_of_libraries : list_of_libraries IDENTIFIER '''
    if(parse_debug):
        print('list_of_libraries_2', list(p))


    # { delete[] p[2]; }
()


def p_drive_strength_1(p):
    '''drive_strength : '(' dr_strength0 ',' dr_strength1 ')' '''
    if(parse_debug):
        print('drive_strength_1', list(p))


    # { p[0].str0 = p[2].str0;
    # 		  p[0].str1 = p[4].str1;
    # 		}
()


def p_drive_strength_2(p):
    '''drive_strength : '(' dr_strength1 ',' dr_strength0 ')' '''
    if(parse_debug):
        print('drive_strength_2', list(p))


    # { p[0].str0 = p[4].str0;
    # 		  p[0].str1 = p[2].str1;
    # 		}
()


def p_drive_strength_3(p):
    '''drive_strength : '(' dr_strength0 ',' K_highz1 ')' '''
    if(parse_debug):
        print('drive_strength_3', list(p))


    # { p[0].str0 = p[2].str0;
    # 		  p[0].str1 = IVL_DR_HiZ;
    # 		}
()


def p_drive_strength_4(p):
    '''drive_strength : '(' dr_strength1 ',' K_highz0 ')' '''
    if(parse_debug):
        print('drive_strength_4', list(p))


    # { p[0].str0 = IVL_DR_HiZ;
    # 		  p[0].str1 = p[2].str1;
    # 		}
()


def p_drive_strength_5(p):
    '''drive_strength : '(' K_highz1 ',' dr_strength0 ')' '''
    if(parse_debug):
        print('drive_strength_5', list(p))


    # { p[0].str0 = p[4].str0;
    # 		  p[0].str1 = IVL_DR_HiZ;
    # 		}
()


def p_drive_strength_6(p):
    '''drive_strength : '(' K_highz0 ',' dr_strength1 ')' '''
    if(parse_debug):
        print('drive_strength_6', list(p))


    # { p[0].str0 = IVL_DR_HiZ;
    # 		  p[0].str1 = p[4].str1;
    # 		}
()


def p_drive_strength_opt_1(p):
    '''drive_strength_opt : drive_strength '''
    if(parse_debug):
        print('drive_strength_opt_1', list(p))
    p[0] = p[1]


()


def p_drive_strength_opt_2(p):
    '''drive_strength_opt :  '''
    if(parse_debug > 2):
        print('drive_strength_opt_2', list(p))


    # { p[0].str0 = IVL_DR_STRONG; p[0].str1 = IVL_DR_STRONG; }
()


def p_dr_strength0_1(p):
    '''dr_strength0 : K_supply0 '''
    if(parse_debug):
        print('dr_strength0_1', list(p))


    # { p[0].str0 = IVL_DR_SUPPLY; }
()


def p_dr_strength0_2(p):
    '''dr_strength0 : K_strong0 '''
    if(parse_debug):
        print('dr_strength0_2', list(p))


    # { p[0].str0 = IVL_DR_STRONG; }
()


def p_dr_strength0_3(p):
    '''dr_strength0 : K_pull0 '''
    if(parse_debug):
        print('dr_strength0_3', list(p))


    # { p[0].str0 = IVL_DR_PULL; }
()


def p_dr_strength0_4(p):
    '''dr_strength0 : K_weak0 '''
    if(parse_debug):
        print('dr_strength0_4', list(p))


    # { p[0].str0 = IVL_DR_WEAK; }
()


def p_dr_strength1_1(p):
    '''dr_strength1 : K_supply1 '''
    if(parse_debug):
        print('dr_strength1_1', list(p))


    # { p[0].str1 = IVL_DR_SUPPLY; }
()


def p_dr_strength1_2(p):
    '''dr_strength1 : K_strong1 '''
    if(parse_debug):
        print('dr_strength1_2', list(p))


    # { p[0].str1 = IVL_DR_STRONG; }
()


def p_dr_strength1_3(p):
    '''dr_strength1 : K_pull1 '''
    if(parse_debug):
        print('dr_strength1_3', list(p))


    # { p[0].str1 = IVL_DR_PULL; }
()


def p_dr_strength1_4(p):
    '''dr_strength1 : K_weak1 '''
    if(parse_debug):
        print('dr_strength1_4', list(p))


    # { p[0].str1 = IVL_DR_WEAK; }
()


def p_clocking_event_opt_1(p):
    '''clocking_event_opt : event_control '''
    if(parse_debug):
        print('clocking_event_opt_1', list(p))


()


def p_clocking_event_opt_2(p):
    '''clocking_event_opt :  '''
    if(parse_debug):
        print('clocking_event_opt_2', list(p))


()


def p_event_control_1(p):
    '''event_control : '@' hierarchy_identifier '''
    if(parse_debug):
        print('event_control_1', list(p))


    # { PEIdent*tmpi = new PEIdent(*p[2]);
    # 		  PEEvent*tmpe = new PEEvent(PEEvent::ANYEDGE, tmpi);
    # 		  PEventStatement*tmps = new PEventStatement(tmpe);
    # 		  FILE_NAME(tmps, @1);
    # 		  p[0] = tmps;
    # 		  delete p[2];
    # 		}
()


def p_event_control_2(p):
    '''event_control : '@' '(' event_expression_list ')' '''
    if(parse_debug):
        print('event_control_2', list(p))


    # { PEventStatement*tmp = new PEventStatement(*p[3]);
    # 		  FILE_NAME(tmp, @1);
    # 		  delete p[3];
    # 		  p[0] = tmp;
    # 		}
()


def p_event_control_3(p):
    '''event_control : '@' '(' error ')' '''
    if(parse_debug):
        print('event_control_3', list(p))


    # { yyerror(@1, "error: Malformed event control expression.");
    # 		  p[0] = None
    # 		}
()


def p_event_expression_list_1(p):
    '''event_expression_list : event_expression '''
    if(parse_debug):
        print('event_expression_list_1', list(p))
    p[0] = p[1]


()


def p_event_expression_list_2(p):
    '''event_expression_list : event_expression_list K_or event_expression '''
    if(parse_debug):
        print('event_expression_list_2', list(p))


    # { svector<PEEvent*>*tmp = new svector<PEEvent*>(*p[1], *p[3]);
    # 		  delete p[1];
    # 		  delete p[3];
    # 		  p[0] = tmp;
    # 		}
()


def p_event_expression_list_3(p):
    '''event_expression_list : event_expression_list ',' event_expression '''
    if(parse_debug):
        print('event_expression_list_3', list(p))


    # { svector<PEEvent*>*tmp = new svector<PEEvent*>(*p[1], *p[3]);
    # 		  delete p[1];
    # 		  delete p[3];
    # 		  p[0] = tmp;
    # 		}
()


def p_event_expression_1(p):
    '''event_expression : K_posedge expression '''
    if(parse_debug):
        print('event_expression_1', list(p))


    # { PEEvent*tmp = new PEEvent(PEEvent::POSEDGE, p[2]);
    # 		  FILE_NAME(tmp, @1);
    # 		  svector<PEEvent*>*tl = new svector<PEEvent*>(1);
    # 		  (*tl)[0] = tmp;
    # 		  p[0] = tl;
    # 		}
()


def p_event_expression_2(p):
    '''event_expression : K_negedge expression '''
    if(parse_debug):
        print('event_expression_2', list(p))


    # { PEEvent*tmp = new PEEvent(PEEvent::NEGEDGE, p[2]);
    # 		  FILE_NAME(tmp, @1);
    # 		  svector<PEEvent*>*tl = new svector<PEEvent*>(1);
    # 		  (*tl)[0] = tmp;
    # 		  p[0] = tl;
    # 		}
()


def p_event_expression_3(p):
    '''event_expression : expression '''
    if(parse_debug):
        print('event_expression_3', list(p))


    # { PEEvent*tmp = new PEEvent(PEEvent::ANYEDGE, p[1]);
    # 		  FILE_NAME(tmp, @1);
    # 		  svector<PEEvent*>*tl = new svector<PEEvent*>(1);
    # 		  (*tl)[0] = tmp;
    # 		  p[0] = tl;
    # 		}
()


def p_branch_probe_expression_1(p):
    '''branch_probe_expression : IDENTIFIER '(' IDENTIFIER ',' IDENTIFIER ')' '''
    if(parse_debug):
        print('branch_probe_expression_1', list(p))


    # { p[0] = pform_make_branch_probe_expression(@1, p[1], p[3], p[5]); }
()


def p_branch_probe_expression_2(p):
    '''branch_probe_expression : IDENTIFIER '(' IDENTIFIER ')' '''
    if(parse_debug):
        print('branch_probe_expression_2', list(p))


    # { p[0] = pform_make_branch_probe_expression(@1, p[1], p[3]); }
()


def p_expression_1(p):
    '''expression : expr_primary_or_typename '''
    if(parse_debug > 2):
        print('expression_1', list(p))
    p[0] = p[1]


()


def p_expression_2(p):
    '''expression : inc_or_dec_expression '''
    if(parse_debug):
        print('expression_2', list(p))
    p[0] = p[1]


()


def p_expression_3(p):
    '''expression : inside_expression '''
    if(parse_debug):
        print('expression_3', list(p))
    p[0] = p[1]


()


def p_expression_4(p):
    '''expression : '+' attribute_list_opt expr_primary %prec UNARY_PREC '''
    if(parse_debug):
        print('expression_4', list(p))
    p[0] = p[3]


()


def PEUnary(op, o1):
    #Leaf(token.STRING, ' ')
    try:
        return Node(syms.atom, [op, o1])
    except:
        return "error in PEUnary: "+str(op)+","+str(o1)


def PEBinary(op, o1, o2):
    try:
        return Node(syms.atom, [o1, Leaf(token.STRING, ' '), op, Leaf(token.STRING, ' '), o2])
    except:
        return "error in PEBinary: "+str(op)+","+str(o1)+","+str(o2)

# unary minus


def p_expression_5(p):
    '''expression : '-' attribute_list_opt expr_primary %prec UNARY_PREC '''
    if(parse_debug):
        print('expression_5', list(p))

    p[0] = PEUnary(Leaf(token.MINUS, '-'), p[3])


    # { PEUnary*tmp = new PEUnary('-', p[3]);
    # 	FILE_NAME(tmp, @3);
    # 	p[0] = tmp;
    #       }
()


def p_expression_6(p):
    '''expression : '~' attribute_list_opt expr_primary %prec UNARY_PREC '''
    if(parse_debug):
        print('expression_6', list(p))

    p[0] = PEUnary(Leaf(token.TILDE, '~'), p[3])


    # { PEUnary*tmp = new PEUnary('~', p[3]);
    # 	FILE_NAME(tmp, @3);
    # 	p[0] = tmp;
    #       }
()


def p_expression_7(p):
    '''expression : '&' attribute_list_opt expr_primary %prec UNARY_PREC '''
    if(parse_debug):
        print('expression_7', list(p))

    p[0] = PEUnary(Leaf(token.AMPER, '&'), p[3])


    # { PEUnary*tmp = new PEUnary('&', p[3]);
    # 	FILE_NAME(tmp, @3);
    # 	p[0] = tmp;
    #       }
()


def p_expression_8(p):
    '''expression : '!' attribute_list_opt expr_primary %prec UNARY_PREC '''
    if(parse_debug):
        print('expression_8', list(p))

    p[0] = PEUnary(Leaf(token.STRING, '!'), p[3])


    # { PEUnary*tmp = new PEUnary('!', p[3]);
    # 	FILE_NAME(tmp, @3);
    # 	p[0] = tmp;
    #       }
()


def p_expression_9(p):
    '''expression : '|' attribute_list_opt expr_primary %prec UNARY_PREC '''
    if(parse_debug):
        print('expression_9', list(p))

    p[0] = PEUnary(Leaf(token.STRING, '|'), p[3])


    # { PEUnary*tmp = new PEUnary('|', p[3]);
    # 	FILE_NAME(tmp, @3);
    # 	p[0] = tmp;
    #       }
()


def p_expression_10(p):
    '''expression : '^' attribute_list_opt expr_primary %prec UNARY_PREC '''
    if(parse_debug):
        print('expression_10', list(p))

    p[0] = PEUnary(Leaf(token.STRING, '^'), p[3])


    # { PEUnary*tmp = new PEUnary('^', p[3]);
    # 	FILE_NAME(tmp, @3);
    # 	p[0] = tmp;
    #       }
()


def p_expression_11(p):
    '''expression : '~' '&' attribute_list_opt expr_primary %prec UNARY_PREC '''
    if(parse_debug):
        print('expression_11', list(p))


    # { yyerror(@1, "error: '~' '&'  is not a valid expression. "
    # 		"Please use operator '~&' instead.");
    # 	p[0] = None
    #       }
()


def p_expression_12(p):
    '''expression : '~' '|' attribute_list_opt expr_primary %prec UNARY_PREC '''
    if(parse_debug):
        print('expression_12', list(p))


    # { yyerror(@1, "error: '~' '|'  is not a valid expression. "
    # 		"Please use operator '~|' instead.");
    # 	p[0] = None
    #       }
()


def p_expression_13(p):
    '''expression : '~' '^' attribute_list_opt expr_primary %prec UNARY_PREC '''
    if(parse_debug):
        print('expression_13', list(p))


    # { yyerror(@1, "error: '~' '^'  is not a valid expression. "
    # 		"Please use operator '~^' instead.");
    # 	p[0] = None
    #       }
()


def p_expression_14(p):
    '''expression : K_NAND attribute_list_opt expr_primary %prec UNARY_PREC '''
    if(parse_debug):
        print('expression_14', list(p))

    p[0] = PEUnary(Leaf(token.STRING, 'K_NAND'), p[3])


    # { PEUnary*tmp = new PEUnary('A', p[3]);
    # 	FILE_NAME(tmp, @3);
    # 	p[0] = tmp;
    #       }
()


def p_expression_15(p):
    '''expression : K_NOR attribute_list_opt expr_primary %prec UNARY_PREC '''
    if(parse_debug):
        print('expression_15', list(p))

    p[0] = PEUnary(Leaf(token.STRING, 'K_NOR'), p[3])


    # { PEUnary*tmp = new PEUnary('N', p[3]);
    # 	FILE_NAME(tmp, @3);
    # 	p[0] = tmp;
    #       }
()


def p_expression_16(p):
    '''expression : K_NXOR attribute_list_opt expr_primary %prec UNARY_PREC '''
    if(parse_debug):
        print('expression_16', list(p))

    p[0] = PEUnary(Leaf(token.STRING, 'K_NXOR'), p[3])


    # { PEUnary*tmp = new PEUnary('X', p[3]);
    # 	FILE_NAME(tmp, @3);
    # 	p[0] = tmp;
    #       }
()


def p_expression_17(p):
    '''expression : '!' error %prec UNARY_PREC '''
    if(parse_debug):
        print('expression_17', list(p))


    # { yyerror(@1, "error: Operand of unary ! "
    # 		"is not a primary expression.");
    # 	p[0] = None
    #       }
()


def p_expression_18(p):
    '''expression : '^' error %prec UNARY_PREC '''
    if(parse_debug):
        print('expression_18', list(p))


    # { yyerror(@1, "error: Operand of reduction ^ "
    # 		"is not a primary expression.");
    # 	p[0] = None
    #       }
()


def p_expression_19(p):
    '''expression : expression '^' attribute_list_opt expression '''
    if(parse_debug):
        print('expression_19', list(p))

    p[0] = PEBinary(Leaf(token.STRING, '^'), p[1], p[4])


    # { PEBinary*tmp = new PEBinary('^', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_20(p):
    '''expression : expression K_POW attribute_list_opt expression '''
    if(parse_debug):
        print('expression_20', list(p))

    p[0] = PEBinary(Leaf(token.STRING, '**'), p[1], p[4])


    # { PEBinary*tmp = new PEBPower('p', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_21(p):
    '''expression : expression '*' attribute_list_opt expression '''
    if(parse_debug):
        print('expression_21', list(p))

    p[0] = PEBinary(Leaf(token.STRING, '*'), p[1], p[4])


    # { PEBinary*tmp = new PEBinary('*', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_22(p):
    '''expression : expression '/' attribute_list_opt expression '''
    if(parse_debug):
        print('expression_22', list(p))

    p[0] = PEBinary(Leaf(token.STRING, '/'), p[1], p[4])


    # { PEBinary*tmp = new PEBinary('/', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_23(p):
    '''expression : expression '%' attribute_list_opt expression '''
    if(parse_debug):
        print('expression_23', list(p))

    p[0] = PEBinary(Leaf(token.STRING, '%'), p[1], p[4])


    # { PEBinary*tmp = new PEBinary('%', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_24(p):
    '''expression : expression '+' attribute_list_opt expression '''
    if(parse_debug):
        print('expression_24', list(p))

    # { PEBinary*tmp = new PEBinary('+', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
    p[0] = PEBinary(Leaf(token.PLUS, '+'), p[1], p[4])


()


def p_expression_25(p):
    '''expression : expression '-' attribute_list_opt expression '''
    if(parse_debug > 2):
        print('expression_25', list(p))
    # { PEBinary*tmp = new PEBinary('-', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
    p[0] = PEBinary(Leaf(token.MINUS, '-'), p[1], p[4])


()


def p_expression_26(p):
    '''expression : expression '&' attribute_list_opt expression '''
    if(parse_debug > 2):
        print('expression_26', list(p))

    p[0] = PEBinary(Leaf(token.AMPER, '&'), p[1], p[4])


    # { PEBinary*tmp = new PEBinary('&', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_27(p):
    '''expression : expression '|' attribute_list_opt expression '''
    if(parse_debug > 2):
        print('expression_27', list(p))

    p[0] = PEBinary(Leaf(token.VBAR, '|'), p[1], p[4])

    # { PEBinary*tmp = new PEBinary('|', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_28(p):
    '''expression : expression K_NAND attribute_list_opt expression '''
    if(parse_debug):
        print('expression_28', list(p))

    p[0] = PEBinary(Leaf(token.STRING, '~&'), p[1], p[4])


    # { PEBinary*tmp = new PEBinary('A', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_29(p):
    '''expression : expression K_NOR attribute_list_opt expression '''
    if(parse_debug):
        print('expression_29', list(p))

    p[0] = PEBinary(Leaf(token.STRING, '~|'), p[1], p[4])


    # { PEBinary*tmp = new PEBinary('O', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_30(p):
    '''expression : expression K_NXOR attribute_list_opt expression '''
    if(parse_debug):
        print('expression_30', list(p))

    p[0] = PEBinary(Leaf(token.STRING, 'K_XNOR'), p[1], p[4])


    # { PEBinary*tmp = new PEBinary('X', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_31(p):
    '''expression : expression '<' attribute_list_opt expression '''
    if(parse_debug):
        print('expression_31', list(p))

    p[0] = PEBinary(Leaf(token.STRING, '<'), p[1], p[4])


    # { PEBinary*tmp = new PEBComp('<', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_32(p):
    '''expression : expression '>' attribute_list_opt expression '''
    if(parse_debug):
        print('expression_32', list(p))

    p[0] = PEBinary(Leaf(token.STRING, '>'), p[1], p[4])


    # { PEBinary*tmp = new PEBComp('>', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_33(p):
    '''expression : expression K_LS attribute_list_opt expression '''
    if(parse_debug):
        print('expression_33', list(p))

    p[0] = PEBinary(Leaf(token.STRING, 'K_LS'), p[1], p[4])


    # { PEBinary*tmp = new PEBShift('l', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_34(p):
    '''expression : expression K_RS attribute_list_opt expression '''
    if(parse_debug):
        print('expression_34', list(p))

    p[0] = PEBinary(Leaf(token.STRING, 'K_RS'), p[1], p[4])


    # { PEBinary*tmp = new PEBShift('r', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_35(p):
    '''expression : expression K_RSS attribute_list_opt expression '''
    if(parse_debug):
        print('expression_35', list(p))

    p[0] = PEBinary(Leaf(token.STRING, 'K_RSS'), p[1], p[4])


    # { PEBinary*tmp = new PEBShift('R', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_36(p):
    '''expression : expression K_EQ attribute_list_opt expression '''
    if(parse_debug):
        print('expression_36', list(p))

    p[0] = PEBinary(Leaf(token.STRING, '=='), p[1], p[4])


    # { PEBinary*tmp = new PEBComp('e', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_37(p):
    '''expression : expression K_CEQ attribute_list_opt expression '''
    if(parse_debug):
        print('expression_37', list(p))

    p[0] = PEBinary(Leaf(token.STRING, 'K_CEQ'), p[1], p[4])


    # { PEBinary*tmp = new PEBComp('E', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_38(p):
    '''expression : expression K_WEQ attribute_list_opt expression '''
    if(parse_debug):
        print('expression_38', list(p))

    p[0] = PEBinary(Leaf(token.STRING, 'K_WEQ'), p[1], p[4])


    # { PEBinary*tmp = new PEBComp('w', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_39(p):
    '''expression : expression K_LE attribute_list_opt expression '''
    if(parse_debug):
        print('expression_39', list(p))

    p[0] = PEBinary(Leaf(token.STRING, '<='), p[1], p[4])


    # { PEBinary*tmp = new PEBComp('L', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_40(p):
    '''expression : expression K_GE attribute_list_opt expression '''
    if(parse_debug):
        print('expression_40', list(p))

    p[0] = PEBinary(Leaf(token.STRING, '>='), p[1], p[4])


    # { PEBinary*tmp = new PEBComp('G', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_41(p):
    '''expression : expression K_NE attribute_list_opt expression '''
    if(parse_debug):
        print('expression_41', list(p))

    p[0] = PEBinary(Leaf(token.STRING, '!='), p[1], p[4])


    # { PEBinary*tmp = new PEBComp('n', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_42(p):
    '''expression : expression K_CNE attribute_list_opt expression '''
    if(parse_debug):
        print('expression_42', list(p))

    p[0] = PEBinary(Leaf(token.STRING, 'K_CNE'), p[1], p[4])


    # { PEBinary*tmp = new PEBComp('N', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_43(p):
    '''expression : expression K_WNE attribute_list_opt expression '''
    if(parse_debug):
        print('expression_43', list(p))

    p[0] = PEBinary(Leaf(token.STRING, 'K_WNE'), p[1], p[4])


    # { PEBinary*tmp = new PEBComp('W', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_44(p):
    '''expression : expression K_LOR attribute_list_opt expression '''
    if(parse_debug):
        print('expression_44', list(p))

    p[0] = PEBinary(Leaf(token.STRING, '||'), p[1], p[4])


    # { PEBinary*tmp = new PEBLogic('o', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_45(p):
    '''expression : expression K_LAND attribute_list_opt expression '''
    if(parse_debug):
        print('expression_45', list(p))

    p[0] = PEBinary(Leaf(token.STRING, '&&'), p[1], p[4])


    # { PEBinary*tmp = new PEBLogic('a', p[1], p[4]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expression_46(p):
    '''expression : expression '?' attribute_list_opt expression ':' expression '''
    if(parse_debug):
        print('expression_46', list(p))

    try:
        p[0] = Node(syms.atom, [p[1], Leaf(token.STRING, ' ? '),
                                p[4], Leaf(token.STRING, ' : '), p[6]])
    except:
        p[0] = "error in PETernary"


    # { PETernary*tmp = new PETernary(p[1], p[4], p[6]);
    # 	FILE_NAME(tmp, @2);
    # 	p[0] = tmp;
    #       }
()


def p_expr_mintypmax_1(p):
    '''expr_mintypmax : expression '''
    if(parse_debug):
        print('expr_mintypmax_1', list(p))
    p[0] = p[1]


()


def p_expr_mintypmax_2(p):
    '''expr_mintypmax : expression ':' expression ':' expression '''
    if(parse_debug):
        print('expr_mintypmax_2', list(p))


    # { switch (min_typ_max_flag) {
    # 		      case MIN:
    # 			p[0] = p[1];
    # 			delete p[3];
    # 			delete p[5];
    # 			break;
    # 		      case TYP:
    # 			delete p[1];
    # 			p[0] = p[3];
    # 			delete p[5];
    # 			break;
    # 		      case MAX:
    # 			delete p[1];
    # 			delete p[3];
    # 			p[0] = p[5];
    # 			break;
    # 		  }
    # 		  if (min_typ_max_warn > 0) {
    # 		        cerr << p[0]->get_fileline() << ": warning: choosing ";
    # 		        switch (min_typ_max_flag) {
    # 		            case MIN:
    # 		              cerr << "min";
    # 		              break;
    # 		            case TYP:
    # 		              cerr << "typ";
    # 		              break;
    # 		            case MAX:
    # 		              cerr << "max";
    # 		              break;
    # 		        }
    # 		        cerr << " expression." << endl;
    # 		        min_typ_max_warn -= 1;
    # 		  }
    # 		}
()


def p_expression_list_with_nuls_1(p):
    '''expression_list_with_nuls : expression_list_with_nuls ',' expression '''
    if(parse_debug):
        print('expression_list_with_nuls_1', list(p))


    # { list<PExpr*>*tmp = p[1];
    # 	tmp->push_back(p[3]);
    # 	p[0] = tmp;
    #       }
()


def p_expression_list_with_nuls_2(p):
    '''expression_list_with_nuls : expression '''
    if(parse_debug):
        print('expression_list_with_nuls_2', list(p))


    # { list<PExpr*>*tmp = new list<PExpr*>;
    # 	tmp->push_back(p[1]);
    # 	p[0] = tmp;
    #       }
()


def p_expression_list_with_nuls_3(p):
    '''expression_list_with_nuls :  '''
    if(parse_debug):
        print('expression_list_with_nuls_3', list(p))


    # { list<PExpr*>*tmp = new list<PExpr*>;
    #         tmp->push_back(0);
    # 	p[0] = tmp;
    #       }
()


def p_expression_list_with_nuls_4(p):
    '''expression_list_with_nuls : expression_list_with_nuls ',' '''
    if(parse_debug):
        print('expression_list_with_nuls_4', list(p))


    # { list<PExpr*>*tmp = p[1];
    # 	tmp->push_back(0);
    # 	p[0] = tmp;
    #       }
()


def p_expression_list_proper_1(p):
    '''expression_list_proper : expression_list_proper ',' expression '''
    if(parse_debug):
        print('expression_list_proper_1', list(p))


    # { list<PExpr*>*tmp = p[1];
    #         tmp->push_back(p[3]);
    #         p[0] = tmp;
    #       }
()


def p_expression_list_proper_2(p):
    '''expression_list_proper : expression '''
    if(parse_debug):
        print('expression_list_proper_2', list(p))


    # { list<PExpr*>*tmp = new list<PExpr*>;
    # 	tmp->push_back(p[1]);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_or_typename_1(p):
    '''expr_primary_or_typename : expr_primary '''
    if(parse_debug > 2):
        print('expr_primary_or_typename_1', list(p))
    p[0] = p[1]


()


def p_expr_primary_or_typename_2(p):
    '''expr_primary_or_typename : TYPE_IDENTIFIER '''
    if(parse_debug):
        print('expr_primary_or_typename_2', list(p))
    p[0] = p[1]


    # { PETypename*tmp = new PETypename(p[1].type);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    # 	delete[]p[1].text;
    #       }
()


def p_expr_primary_1(p):
    '''expr_primary : number '''
    if(parse_debug):
        print('expr_primary_1', list(p))
    p[0] = p[1]


    # { assert(p[1]);
    # 	PENumber*tmp = new PENumber(p[1]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_2(p):
    '''expr_primary : REALTIME '''
    if(parse_debug):
        print('expr_primary_2', list(p))


    # { PEFNumber*tmp = new PEFNumber(p[1]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_3(p):
    '''expr_primary : STRING '''
    if(parse_debug):
        print('expr_primary_3', list(p))


    # { PEString*tmp = new PEString(p[1]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_4(p):
    '''expr_primary : TIME_LITERAL '''
    if(parse_debug):
        print('expr_primary_4', list(p))


    # { int unit;
    #
    #           based_size = 0;
    #           p[0]         = 0;
    #           if (p[1] == 0 || !get_time_unit(p[1], unit))
    #               yyerror(@1, "internal error: delay.");
    #           else {
    #               double p = pow(10.0, (double)(unit - pform_get_timeunit()));
    #               double time = atof(p[1]) * p;
    #
    #               verireal *v = new verireal(time);
    #               p[0] = new PEFNumber(v);
    #               FILE_NAME(p[0], @1);
    #           }
    #       }
()


def p_expr_primary_5(p):
    '''expr_primary : SYSTEM_IDENTIFIER '''
    if(parse_debug):
        print('expr_primary_5', list(p))


    # { perm_string tn = lex_strings.make(p[1]);
    # 	PECallFunction*tmp = new PECallFunction(tn);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    # 	delete[]p[1];
    #       }
()


def p_expr_primary_6(p):
    '''expr_primary : hierarchy_identifier '''
    if(parse_debug > 2):
        print('expr_primary_6', list(p))
    p[0] = p[1]


    # { PEIdent*tmp = pform_new_ident(*p[1]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    # 	delete p[1];
    #       }
()


def p_expr_primary_7(p):
    '''expr_primary : PACKAGE_IDENTIFIER K_SCOPE_RES hierarchy_identifier '''
    if(parse_debug):
        print('expr_primary_7', list(p))


    # { p[0] = pform_package_ident(@2, p[1], p[3]);
    # 	delete p[3];
    #       }
()


def p_expr_primary_8(p):
    '''expr_primary : hierarchy_identifier '(' expression_list_with_nuls ')' '''
    if(parse_debug):
        print('expr_primary_8', list(p))


    # { list<PExpr*>*expr_list = p[3];
    # 	strip_tail_items(expr_list);
    # 	PECallFunction*tmp = pform_make_call_function(@1, *p[1], *expr_list);
    # 	delete p[1];
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_9(p):
    '''expr_primary : implicit_class_handle '.' hierarchy_identifier '(' expression_list_with_nuls ')' '''
    if(parse_debug):
        print('expr_primary_9', list(p))


    # { pform_name_t*t_name = p[1];
    # 	while (! p[3]->empty()) {
    # 	      t_name->push_back(p[3]->front());
    # 	      p[3]->pop_front();
    # 	}
    # 	list<PExpr*>*expr_list = p[5];
    # 	strip_tail_items(expr_list);
    # 	PECallFunction*tmp = pform_make_call_function(@1, *t_name, *expr_list);
    # 	delete p[1];
    # 	delete p[3];
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_10(p):
    '''expr_primary : SYSTEM_IDENTIFIER '(' expression_list_proper ')' '''
    if(parse_debug):
        print('expr_primary_10', list(p))


    # { perm_string tn = lex_strings.make(p[1]);
    # 	PECallFunction*tmp = new PECallFunction(tn, *p[3]);
    # 	FILE_NAME(tmp, @1);
    # 	delete[]p[1];
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_11(p):
    '''expr_primary : PACKAGE_IDENTIFIER K_SCOPE_RES IDENTIFIER '(' expression_list_proper ')' '''
    if(parse_debug):
        print('expr_primary_11', list(p))


    # { perm_string use_name = lex_strings.make(p[3]);
    # 	PECallFunction*tmp = new PECallFunction(p[1], use_name, *p[5]);
    # 	FILE_NAME(tmp, @3);
    # 	delete[]p[3];
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_12(p):
    '''expr_primary : SYSTEM_IDENTIFIER '(' ')' '''
    if(parse_debug):
        print('expr_primary_12', list(p))


    # { perm_string tn = lex_strings.make(p[1]);
    # 	const vector<PExpr*>empty;
    # 	PECallFunction*tmp = new PECallFunction(tn, empty);
    # 	FILE_NAME(tmp, @1);
    # 	delete[]p[1];
    # 	p[0] = tmp;
    # 	if (!gn_system_verilog()) {
    # 	      yyerror(@1, "error: Empty function argument list requires SystemVerilog.");
    # 	}
    #       }
()


def p_expr_primary_13(p):
    '''expr_primary : implicit_class_handle '''
    if(parse_debug):
        print('expr_primary_13', list(p))


    # { PEIdent*tmp = new PEIdent(*p[1]);
    # 	FILE_NAME(tmp,@1);
    # 	delete p[1];
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_14(p):
    '''expr_primary : implicit_class_handle '.' hierarchy_identifier '''
    if(parse_debug):
        print('expr_primary_14', list(p))


    # { pform_name_t*t_name = p[1];
    # 	while (! p[3]->empty()) {
    # 	      t_name->push_back(p[3]->front());
    # 	      p[3]->pop_front();
    # 	}
    # 	PEIdent*tmp = new PEIdent(*t_name);
    # 	FILE_NAME(tmp,@1);
    # 	delete p[1];
    # 	delete p[3];
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_15(p):
    '''expr_primary : K_acos '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_15', list(p))


    # { perm_string tn = perm_string::literal("$acos");
    # 	PECallFunction*tmp = make_call_function(tn, p[3]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_16(p):
    '''expr_primary : K_acosh '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_16', list(p))


    # { perm_string tn = perm_string::literal("$acosh");
    # 	PECallFunction*tmp = make_call_function(tn, p[3]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_17(p):
    '''expr_primary : K_asin '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_17', list(p))


    # { perm_string tn = perm_string::literal("$asin");
    # 	PECallFunction*tmp = make_call_function(tn, p[3]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_18(p):
    '''expr_primary : K_asinh '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_18', list(p))


    # { perm_string tn = perm_string::literal("$asinh");
    # 	PECallFunction*tmp = make_call_function(tn, p[3]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_19(p):
    '''expr_primary : K_atan '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_19', list(p))


    # { perm_string tn = perm_string::literal("$atan");
    # 	PECallFunction*tmp = make_call_function(tn, p[3]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_20(p):
    '''expr_primary : K_atanh '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_20', list(p))


    # { perm_string tn = perm_string::literal("$atanh");
    # 	PECallFunction*tmp = make_call_function(tn, p[3]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_21(p):
    '''expr_primary : K_atan2 '(' expression ',' expression ')' '''
    if(parse_debug):
        print('expr_primary_21', list(p))


    # { perm_string tn = perm_string::literal("$atan2");
    # 	PECallFunction*tmp = make_call_function(tn, p[3], p[5]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_22(p):
    '''expr_primary : K_ceil '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_22', list(p))


    # { perm_string tn = perm_string::literal("$ceil");
    # 	PECallFunction*tmp = make_call_function(tn, p[3]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_23(p):
    '''expr_primary : K_cos '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_23', list(p))


    # { perm_string tn = perm_string::literal("$cos");
    # 	PECallFunction*tmp = make_call_function(tn, p[3]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_24(p):
    '''expr_primary : K_cosh '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_24', list(p))


    # { perm_string tn = perm_string::literal("$cosh");
    # 	PECallFunction*tmp = make_call_function(tn, p[3]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_25(p):
    '''expr_primary : K_exp '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_25', list(p))


    # { perm_string tn = perm_string::literal("$exp");
    # 	PECallFunction*tmp = make_call_function(tn, p[3]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_26(p):
    '''expr_primary : K_floor '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_26', list(p))


    # { perm_string tn = perm_string::literal("$floor");
    # 	PECallFunction*tmp = make_call_function(tn, p[3]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_27(p):
    '''expr_primary : K_hypot '(' expression ',' expression ')' '''
    if(parse_debug):
        print('expr_primary_27', list(p))


    # { perm_string tn = perm_string::literal("$hypot");
    # 	PECallFunction*tmp = make_call_function(tn, p[3], p[5]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_28(p):
    '''expr_primary : K_ln '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_28', list(p))


    # { perm_string tn = perm_string::literal("$ln");
    # 	PECallFunction*tmp = make_call_function(tn, p[3]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_29(p):
    '''expr_primary : K_log '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_29', list(p))


    # { perm_string tn = perm_string::literal("$log10");
    # 	PECallFunction*tmp = make_call_function(tn, p[3]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_30(p):
    '''expr_primary : K_pow '(' expression ',' expression ')' '''
    if(parse_debug):
        print('expr_primary_30', list(p))


    # { perm_string tn = perm_string::literal("$pow");
    #         PECallFunction*tmp = make_call_function(tn, p[3], p[5]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_31(p):
    '''expr_primary : K_sin '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_31', list(p))


    # { perm_string tn = perm_string::literal("$sin");
    # 	PECallFunction*tmp = make_call_function(tn, p[3]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_32(p):
    '''expr_primary : K_sinh '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_32', list(p))


    # { perm_string tn = perm_string::literal("$sinh");
    # 	PECallFunction*tmp = make_call_function(tn, p[3]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_33(p):
    '''expr_primary : K_sqrt '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_33', list(p))


    # { perm_string tn = perm_string::literal("$sqrt");
    # 	PECallFunction*tmp = make_call_function(tn, p[3]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_34(p):
    '''expr_primary : K_tan '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_34', list(p))


    # { perm_string tn = perm_string::literal("$tan");
    # 	PECallFunction*tmp = make_call_function(tn, p[3]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_35(p):
    '''expr_primary : K_tanh '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_35', list(p))


    # { perm_string tn = perm_string::literal("$tanh");
    # 	PECallFunction*tmp = make_call_function(tn, p[3]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_36(p):
    '''expr_primary : K_abs '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_36', list(p))


    # { PEUnary*tmp = new PEUnary('m', p[3]);
    #         FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_37(p):
    '''expr_primary : K_max '(' expression ',' expression ')' '''
    if(parse_debug):
        print('expr_primary_37', list(p))


    # { PEBinary*tmp = new PEBinary('M', p[3], p[5]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_38(p):
    '''expr_primary : K_min '(' expression ',' expression ')' '''
    if(parse_debug):
        print('expr_primary_38', list(p))


    # { PEBinary*tmp = new PEBinary('m', p[3], p[5]);
    # 	FILE_NAME(tmp,@1);
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_39(p):
    '''expr_primary : '(' expr_mintypmax ')' '''
    if(parse_debug):
        print('expr_primary_39', list(p))
    p[0] = p[2]


()


def p_expr_primary_40(p):
    '''expr_primary : '{' expression_list_proper '}' '''
    if(parse_debug):
        print('expr_primary_40', list(p))


    # { PEConcat*tmp = new PEConcat(*p[2]);
    # 	FILE_NAME(tmp, @1);
    # 	delete p[2];
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_41(p):
    '''expr_primary : '{' expression '{' expression_list_proper '}' '}' '''
    if(parse_debug):
        print('expr_primary_41', list(p))


    # { PExpr*rep = p[2];
    # 	PEConcat*tmp = new PEConcat(*p[4], rep);
    # 	FILE_NAME(tmp, @1);
    # 	delete p[4];
    # 	p[0] = tmp;
    #       }
()


def p_expr_primary_42(p):
    '''expr_primary : '{' expression '{' expression_list_proper '}' error '}' '''
    if(parse_debug):
        print('expr_primary_42', list(p))


    # { PExpr*rep = p[2];
    # 	PEConcat*tmp = new PEConcat(*p[4], rep);
    # 	FILE_NAME(tmp, @1);
    # 	delete p[4];
    # 	p[0] = tmp;
    # 	yyerror(@5, "error: Syntax error between internal '}' "
    # 		"and closing '}' of repeat concatenation.");
    # 	yyerrok;
    #       }
()


def p_expr_primary_43(p):
    '''expr_primary : '{' '}' '''
    if(parse_debug):
        print('expr_primary_43', list(p))


    # { // This is the empty queue syntax.
    # 	if (gn_system_verilog()) {
    # 	      list<PExpr*> empty_list;
    # 	      PEConcat*tmp = new PEConcat(empty_list);
    # 	      FILE_NAME(tmp, @1);
    # 	      p[0] = tmp;
    # 	} else {
    # 	      yyerror(@1, "error: Concatenations are not allowed to be empty.");
    # 	      p[0] = None
    # 	}
    #       }
()


def p_expr_primary_44(p):
    '''expr_primary : expr_primary "'" '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_44', list(p))


    # { PExpr*base = p[4];
    # 	if (gn_system_verilog()) {
    # 	      PECastSize*tmp = new PECastSize(p[1], base);
    # 	      FILE_NAME(tmp, @1);
    # 	      p[0] = tmp;
    # 	} else {
    # 	      yyerror(@1, "error: Size cast requires SystemVerilog.");
    # 	      p[0] = base;
    # 	}
    #       }
()


def p_expr_primary_45(p):
    '''expr_primary : simple_type_or_string "'" '(' expression ')' '''
    if(parse_debug):
        print('expr_primary_45', list(p))


    # { PExpr*base = p[4];
    # 	if (gn_system_verilog()) {
    # 	      PECastType*tmp = new PECastType(p[1], base);
    # 	      FILE_NAME(tmp, @1);
    # 	      p[0] = tmp;
    # 	} else {
    # 	      yyerror(@1, "error: Type cast requires SystemVerilog.");
    # 	      p[0] = base;
    # 	}
    #       }
()


def p_expr_primary_46(p):
    '''expr_primary : assignment_pattern '''
    if(parse_debug):
        print('expr_primary_46', list(p))
    p[0] = p[1]


()


def p_expr_primary_47(p):
    '''expr_primary : streaming_concatenation '''
    if(parse_debug):
        print('expr_primary_47', list(p))
    p[0] = p[1]


()


def p_expr_primary_48(p):
    '''expr_primary : K_null '''
    if(parse_debug):
        print('expr_primary_48', list(p))


    # { PENull*tmp = new PENull;
    # 	    FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_function_item_list_opt_1(p):
    '''function_item_list_opt : function_item_list '''
    if(parse_debug):
        print('function_item_list_opt_1', list(p))
    p[0] = p[1]


()


def p_function_item_list_opt_2(p):
    '''function_item_list_opt :  '''
    if(parse_debug):
        print('function_item_list_opt_2', list(p))


    # { p[0] = None }
()


def p_function_item_list_1(p):
    '''function_item_list : function_item '''
    if(parse_debug):
        print('function_item_list_1', list(p))
    p[0] = p[1]


()


def p_function_item_list_2(p):
    '''function_item_list : function_item_list function_item '''
    if(parse_debug):
        print('function_item_list_2', list(p))


    # { /* */
    # 	if (p[1] && p[2]) {
    # 	      vector<pform_tf_port_t>*tmp = p[1];
    # 	      size_t s1 = tmp->size();
    # 	      tmp->resize(s1 + p[2]->size());
    # 	      for (size_t idx = 0 ; idx < p[2]->size() ; idx += 1)
    # 		    tmp->at(s1+idx) = p[2]->at(idx);
    # 	      delete p[2];
    # 	      p[0] = tmp;
    # 	} else if (p[1]) {
    # 	      p[0] = p[1];
    # 	} else {
    # 	      p[0] = p[2];
    # 	}
    #       }
()


def p_function_item_1(p):
    '''function_item : tf_port_declaration '''
    if(parse_debug):
        print('function_item_1', list(p))
    p[0] = p[1]


()


def p_function_item_2(p):
    '''function_item : block_item_decl '''
    if(parse_debug):
        print('function_item_2', list(p))


    # { p[0] = None }
()


def p_gate_instance_1(p):
    '''gate_instance : IDENTIFIER '(' expression_list_with_nuls ')' '''
    if(parse_debug):
        print('gate_instance_1', list(p))


    # { lgate*tmp = new lgate;
    # 		  tmp->name = p[1];
    # 		  tmp->parms = p[3];
    # 		  tmp->file  = @1.text;
    # 		  tmp->lineno = @1.first_line;
    # 		  delete[]p[1];
    # 		  p[0] = tmp;
    # 		}
()


def p_gate_instance_2(p):
    '''gate_instance : IDENTIFIER dimensions '(' expression_list_with_nuls ')' '''
    if(parse_debug):
        print('gate_instance_2', list(p))


    # { lgate*tmp = new lgate;
    # 	list<pform_range_t>*rng = p[2];
    # 	tmp->name = p[1];
    # 	tmp->parms = p[4];
    # 	tmp->range = rng->front();
    # 	rng->pop_front();
    # 	assert(rng->empty());
    # 	tmp->file  = @1.text;
    # 	tmp->lineno = @1.first_line;
    # 	delete[]p[1];
    # 	delete rng;
    # 	p[0] = tmp;
    #       }
()


def p_gate_instance_3(p):
    '''gate_instance : '(' expression_list_with_nuls ')' '''
    if(parse_debug):
        print('gate_instance_3', list(p))


    # { lgate*tmp = new lgate;
    # 		  tmp->name = "";
    # 		  tmp->parms = p[2];
    # 		  tmp->file  = @1.text;
    # 		  tmp->lineno = @1.first_line;
    # 		  p[0] = tmp;
    # 		}
()


def p_gate_instance_4(p):
    '''gate_instance : IDENTIFIER dimensions '''
    if(parse_debug):
        print('gate_instance_4', list(p))


    # { lgate*tmp = new lgate;
    # 	list<pform_range_t>*rng = p[2];
    # 	tmp->name = p[1];
    # 	tmp->parms = 0;
    # 	tmp->parms_by_name = 0;
    # 	tmp->range = rng->front();
    # 	rng->pop_front();
    # 	assert(rng->empty());
    # 	tmp->file  = @1.text;
    # 	tmp->lineno = @1.first_line;
    # 	delete[]p[1];
    # 	delete rng;
    # 	p[0] = tmp;
    #       }
()


def p_gate_instance_5(p):
    '''gate_instance : IDENTIFIER '(' port_name_list ')' '''
    if(parse_debug):
        print('gate_instance_5', list(p))


    # { lgate*tmp = new lgate;
    # 	tmp->name = p[1];
    # 	tmp->parms = 0;
    # 	tmp->parms_by_name = p[3];
    # 	tmp->file  = @1.text;
    # 	tmp->lineno = @1.first_line;
    # 	delete[]p[1];
    # 	p[0] = tmp;
    #       }
()


def p_gate_instance_6(p):
    '''gate_instance : IDENTIFIER dimensions '(' port_name_list ')' '''
    if(parse_debug):
        print('gate_instance_6', list(p))


    # { lgate*tmp = new lgate;
    # 	list<pform_range_t>*rng = p[2];
    # 	tmp->name = p[1];
    # 	tmp->parms = 0;
    # 	tmp->parms_by_name = p[4];
    # 	tmp->range = rng->front();
    # 	rng->pop_front();
    # 	assert(rng->empty());
    # 	tmp->file  = @1.text;
    # 	tmp->lineno = @1.first_line;
    # 	delete[]p[1];
    # 	delete rng;
    # 	p[0] = tmp;
    #       }
()


def p_gate_instance_7(p):
    '''gate_instance : IDENTIFIER '(' error ')' '''
    if(parse_debug):
        print('gate_instance_7', list(p))


    # { lgate*tmp = new lgate;
    # 		  tmp->name = p[1];
    # 		  tmp->parms = 0;
    # 		  tmp->parms_by_name = 0;
    # 		  tmp->file  = @1.text;
    # 		  tmp->lineno = @1.first_line;
    # 		  yyerror(@2, "error: Syntax error in instance port "
    # 			  "expression(s).");
    # 		  delete[]p[1];
    # 		  p[0] = tmp;
    # 		}
()


def p_gate_instance_8(p):
    '''gate_instance : IDENTIFIER dimensions '(' error ')' '''
    if(parse_debug):
        print('gate_instance_8', list(p))


    # { lgate*tmp = new lgate;
    # 		  tmp->name = p[1];
    # 		  tmp->parms = 0;
    # 		  tmp->parms_by_name = 0;
    # 		  tmp->file  = @1.text;
    # 		  tmp->lineno = @1.first_line;
    # 		  yyerror(@3, "error: Syntax error in instance port "
    # 			  "expression(s).");
    # 		  delete[]p[1];
    # 		  p[0] = tmp;
    # 		}
()


def p_gate_instance_list_1(p):
    '''gate_instance_list : gate_instance_list ',' gate_instance '''
    if(parse_debug):
        print('gate_instance_list_1', list(p))


    # { svector<lgate>*tmp1 = p[1];
    # 		  lgate*tmp2 = p[3];
    # 		  svector<lgate>*out = new svector<lgate> (*tmp1, *tmp2);
    # 		  delete tmp1;
    # 		  delete tmp2;
    # 		  p[0] = out;
    # 		}
()


def p_gate_instance_list_2(p):
    '''gate_instance_list : gate_instance '''
    if(parse_debug):
        print('gate_instance_list_2', list(p))


    # { svector<lgate>*tmp = new svector<lgate>(1);
    # 		  (*tmp)[0] = *p[1];
    # 		  delete p[1];
    # 		  p[0] = tmp;
    # 		}
()


def p_gatetype_1(p):
    '''gatetype : K_and '''
    if(parse_debug):
        print('gatetype_1', list(p))


    # { p[0] = PGBuiltin::AND; }
()


def p_gatetype_2(p):
    '''gatetype : K_nand '''
    if(parse_debug):
        print('gatetype_2', list(p))


    # { p[0] = PGBuiltin::NAND; }
()


def p_gatetype_3(p):
    '''gatetype : K_or '''
    if(parse_debug):
        print('gatetype_3', list(p))


    # { p[0] = PGBuiltin::OR; }
()


def p_gatetype_4(p):
    '''gatetype : K_nor '''
    if(parse_debug):
        print('gatetype_4', list(p))


    # { p[0] = PGBuiltin::NOR; }
()


def p_gatetype_5(p):
    '''gatetype : K_xor '''
    if(parse_debug):
        print('gatetype_5', list(p))


    # { p[0] = PGBuiltin::XOR; }
()


def p_gatetype_6(p):
    '''gatetype : K_xnor '''
    if(parse_debug):
        print('gatetype_6', list(p))


    # { p[0] = PGBuiltin::XNOR; }
()


def p_gatetype_7(p):
    '''gatetype : K_buf '''
    if(parse_debug):
        print('gatetype_7', list(p))


    # { p[0] = PGBuiltin::BUF; }
()


def p_gatetype_8(p):
    '''gatetype : K_bufif0 '''
    if(parse_debug):
        print('gatetype_8', list(p))


    # { p[0] = PGBuiltin::BUFIF0; }
()


def p_gatetype_9(p):
    '''gatetype : K_bufif1 '''
    if(parse_debug):
        print('gatetype_9', list(p))


    # { p[0] = PGBuiltin::BUFIF1; }
()


def p_gatetype_10(p):
    '''gatetype : K_not '''
    if(parse_debug):
        print('gatetype_10', list(p))


    # { p[0] = PGBuiltin::NOT; }
()


def p_gatetype_11(p):
    '''gatetype : K_notif0 '''
    if(parse_debug):
        print('gatetype_11', list(p))


    # { p[0] = PGBuiltin::NOTIF0; }
()


def p_gatetype_12(p):
    '''gatetype : K_notif1 '''
    if(parse_debug):
        print('gatetype_12', list(p))


    # { p[0] = PGBuiltin::NOTIF1; }
()


def p_switchtype_1(p):
    '''switchtype : K_nmos '''
    if(parse_debug):
        print('switchtype_1', list(p))


    # { p[0] = PGBuiltin::NMOS; }
()


def p_switchtype_2(p):
    '''switchtype : K_rnmos '''
    if(parse_debug):
        print('switchtype_2', list(p))


    # { p[0] = PGBuiltin::RNMOS; }
()


def p_switchtype_3(p):
    '''switchtype : K_pmos '''
    if(parse_debug):
        print('switchtype_3', list(p))


    # { p[0] = PGBuiltin::PMOS; }
()


def p_switchtype_4(p):
    '''switchtype : K_rpmos '''
    if(parse_debug):
        print('switchtype_4', list(p))


    # { p[0] = PGBuiltin::RPMOS; }
()


def p_switchtype_5(p):
    '''switchtype : K_cmos '''
    if(parse_debug):
        print('switchtype_5', list(p))


    # { p[0] = PGBuiltin::CMOS; }
()


def p_switchtype_6(p):
    '''switchtype : K_rcmos '''
    if(parse_debug):
        print('switchtype_6', list(p))


    # { p[0] = PGBuiltin::RCMOS; }
()


def p_switchtype_7(p):
    '''switchtype : K_tran '''
    if(parse_debug):
        print('switchtype_7', list(p))


    # { p[0] = PGBuiltin::TRAN; }
()


def p_switchtype_8(p):
    '''switchtype : K_rtran '''
    if(parse_debug):
        print('switchtype_8', list(p))


    # { p[0] = PGBuiltin::RTRAN; }
()


def p_switchtype_9(p):
    '''switchtype : K_tranif0 '''
    if(parse_debug):
        print('switchtype_9', list(p))


    # { p[0] = PGBuiltin::TRANIF0; }
()


def p_switchtype_10(p):
    '''switchtype : K_tranif1 '''
    if(parse_debug):
        print('switchtype_10', list(p))


    # { p[0] = PGBuiltin::TRANIF1; }
()


def p_switchtype_11(p):
    '''switchtype : K_rtranif0 '''
    if(parse_debug):
        print('switchtype_11', list(p))


    # { p[0] = PGBuiltin::RTRANIF0; }
()


def p_switchtype_12(p):
    '''switchtype : K_rtranif1 '''
    if(parse_debug):
        print('switchtype_12', list(p))


    # { p[0] = PGBuiltin::RTRANIF1; }
()


def p_hierarchy_identifier_1(p):
    '''hierarchy_identifier : IDENTIFIER '''
    if(parse_debug):
        print('hierarchy_identifier_1 FIXME', list(p))
    lpvalue = Leaf(token.NAME, p[1])
    p[0] = lpvalue


    # { p[0] = new pform_name_t;
    # 	  p[0]->push_back(name_component_t(lex_strings.make(p[1])));
    # 	  delete[]p[1];
    # 	}
()


def p_hierarchy_identifier_2(p):
    '''hierarchy_identifier : hierarchy_identifier '.' IDENTIFIER '''
    if(parse_debug):
        print('hierarchy_identifier_2', list(p))


    # { pform_name_t * tmp = p[1];
    # 	  tmp->push_back(name_component_t(lex_strings.make(p[3])));
    # 	  delete[]p[3];
    # 	  p[0] = tmp;
    # 	}
()


def p_hierarchy_identifier_3(p):
    '''hierarchy_identifier : hierarchy_identifier '[' expression ']' '''
    if(parse_debug):
        print('hierarchy_identifier_3', list(p))

        p[0] = Node(syms.atom, [p[1], Leaf(
            token.STRING, '['), p[3], Leaf(token.STRING, ']')])


    # { pform_name_t * tmp = p[1];
    # 	  name_component_t&tail = tmp->back();
    # 	  index_component_t itmp;
    # 	  itmp.sel = index_component_t::SEL_BIT;
    # 	  itmp.msb = p[3];
    # 	  tail.index.push_back(itmp);
    # 	  p[0] = tmp;
    # 	}
()


def p_hierarchy_identifier_4(p):
    '''hierarchy_identifier : hierarchy_identifier '[' '$' ']' '''
    if(parse_debug):
        print('hierarchy_identifier_4', list(p))


    # { pform_name_t * tmp = p[1];
    # 	  name_component_t&tail = tmp->back();
    # 	  if (! gn_system_verilog()) {
    # 		yyerror(@3, "error: Last element expression ($) "
    # 			"requires SystemVerilog. Try enabling SystemVerilog.");
    # 	  }
    # 	  index_component_t itmp;
    # 	  itmp.sel = index_component_t::SEL_BIT_LAST;
    # 	  itmp.msb = 0;
    # 	  itmp.lsb = 0;
    # 	  tail.index.push_back(itmp);
    # 	  p[0] = tmp;
    # 	}
()


def p_hierarchy_identifier_5(p):
    '''hierarchy_identifier : hierarchy_identifier '[' expression ':' expression ']' '''
    if(parse_debug):
        print('hierarchy_identifier_5', list(p))


    # { pform_name_t * tmp = p[1];
    # 	  name_component_t&tail = tmp->back();
    # 	  index_component_t itmp;
    # 	  itmp.sel = index_component_t::SEL_PART;
    # 	  itmp.msb = p[3];
    # 	  itmp.lsb = p[5];
    # 	  tail.index.push_back(itmp);
    # 	  p[0] = tmp;
    # 	}
()


def p_hierarchy_identifier_6(p):
    '''hierarchy_identifier : hierarchy_identifier '[' expression K_PO_POS expression ']' '''
    if(parse_debug):
        print('hierarchy_identifier_6', list(p))


    # { pform_name_t * tmp = p[1];
    # 	  name_component_t&tail = tmp->back();
    # 	  index_component_t itmp;
    # 	  itmp.sel = index_component_t::SEL_IDX_UP;
    # 	  itmp.msb = p[3];
    # 	  itmp.lsb = p[5];
    # 	  tail.index.push_back(itmp);
    # 	  p[0] = tmp;
    # 	}
()


def p_hierarchy_identifier_7(p):
    '''hierarchy_identifier : hierarchy_identifier '[' expression K_PO_NEG expression ']' '''
    if(parse_debug):
        print('hierarchy_identifier_7', list(p))


    # { pform_name_t * tmp = p[1];
    # 	  name_component_t&tail = tmp->back();
    # 	  index_component_t itmp;
    # 	  itmp.sel = index_component_t::SEL_IDX_DO;
    # 	  itmp.msb = p[3];
    # 	  itmp.lsb = p[5];
    # 	  tail.index.push_back(itmp);
    # 	  p[0] = tmp;
    # 	}
()


def p_list_of_identifiers_1(p):
    '''list_of_identifiers : IDENTIFIER '''
    if(parse_debug):
        print('list_of_identifiers_1', list(p))


    # { p[0] = list_from_identifier(p[1]); }
()


def p_list_of_identifiers_2(p):
    '''list_of_identifiers : list_of_identifiers ',' IDENTIFIER '''
    if(parse_debug):
        print('list_of_identifiers_2', list(p))


    # { p[0] = list_from_identifier(p[1], p[3]); }
()


def p_list_of_port_identifiers_1(p):
    '''list_of_port_identifiers : IDENTIFIER dimensions_opt '''
    if(parse_debug):
        print('list_of_port_identifiers_1', list(p))


    # { p[0] = make_port_list(p[1], p[2], 0); }
()


def p_list_of_port_identifiers_2(p):
    '''list_of_port_identifiers : list_of_port_identifiers ',' IDENTIFIER dimensions_opt '''
    if(parse_debug):
        print('list_of_port_identifiers_2', list(p))


    # { p[0] = make_port_list(p[1], p[3], p[4], 0); }
()


def p_list_of_variable_port_identifiers_1(p):
    '''list_of_variable_port_identifiers : IDENTIFIER dimensions_opt '''
    if(parse_debug):
        print('list_of_variable_port_identifiers_1', list(p))


    # { p[0] = make_port_list(p[1], p[2], 0); }
()


def p_list_of_variable_port_identifiers_2(p):
    '''list_of_variable_port_identifiers : IDENTIFIER dimensions_opt '=' expression '''
    if(parse_debug):
        print('list_of_variable_port_identifiers_2', list(p))


    # { p[0] = make_port_list(p[1], p[2], p[4]); }
()


def p_list_of_variable_port_identifiers_3(p):
    '''list_of_variable_port_identifiers : list_of_variable_port_identifiers ',' IDENTIFIER dimensions_opt '''
    if(parse_debug):
        print('list_of_variable_port_identifiers_3', list(p))


    # { p[0] = make_port_list(p[1], p[3], p[4], 0); }
()


def p_list_of_variable_port_identifiers_4(p):
    '''list_of_variable_port_identifiers : list_of_variable_port_identifiers ',' IDENTIFIER dimensions_opt '=' expression '''
    if(parse_debug):
        print('list_of_variable_port_identifiers_4', list(p))


    # { p[0] = make_port_list(p[1], p[3], p[4], p[6]); }
()


def p_list_of_ports_1(p):
    '''list_of_ports : port_opt '''
    if(parse_debug):
        print('list_of_ports_1', list(p))


    # { vector<Module::port_t*>*tmp
    # 			  = new vector<Module::port_t*>(1);
    # 		  (*tmp)[0] = p[1];
    # 		  p[0] = tmp;
    # 		}
()


def p_list_of_ports_2(p):
    '''list_of_ports : list_of_ports ',' port_opt '''
    if(parse_debug):
        print('list_of_ports_2', list(p))


    # { vector<Module::port_t*>*tmp = p[1];
    # 		  tmp->push_back(p[3]);
    # 		  p[0] = tmp;
    # 		}
()


def p_list_of_port_declarations_1(p):
    '''list_of_port_declarations : port_declaration '''
    if(parse_debug > 1):
        print('list_of_port_declarations_1', list(p))
    p[0] = [p[1]]


    # { vector<Module::port_t*>*tmp
    # 			  = new vector<Module::port_t*>(1);
    # 		  (*tmp)[0] = p[1];
    # 		  p[0] = tmp;
    # 		}
()


def p_list_of_port_declarations_2(p):
    '''list_of_port_declarations : list_of_port_declarations ',' port_declaration '''
    if(parse_debug):
        print('list_of_port_declarations_2 FIXME', list(p))
    # MOVE_TO absyn p[1].append(Leaf(token.NEWLINE, '\n')) # should be a comma
    # XXX p[3].prefix=' ' # add a space after the NL, must go in parameter
    p[1].append(p[3])
    p[0] = p[1]


    # { vector<Module::port_t*>*tmp = p[1];
    # 		  tmp->push_back(p[3]);
    # 		  p[0] = tmp;
    # 		}
()


def p_list_of_port_declarations_3(p):
    '''list_of_port_declarations : list_of_port_declarations ',' IDENTIFIER '''
    if(parse_debug):
        print('list_of_port_declarations_3', list(p))


    # { Module::port_t*ptmp;
    # 		  perm_string name = lex_strings.make(p[3]);
    # 		  ptmp = pform_module_port_reference(name, @3.text,
    # 						     @3.first_line);
    # 		  vector<Module::port_t*>*tmp = p[1];
    # 		  tmp->push_back(ptmp);
    #
    # 		    /* Get the port declaration details, the port type
    # 		       and what not, from context data stored by the
    # 		       last port_declaration rule. */
    # 		  pform_module_define_port(@3, name,
    # 					port_declaration_context.port_type,
    # 					port_declaration_context.port_net_type,
    # 					port_declaration_context.data_type, 0);
    # 		  delete[]p[3];
    # 		  p[0] = tmp;
    # 		}
()


def p_list_of_port_declarations_4(p):
    '''list_of_port_declarations : list_of_port_declarations ',' '''
    if(parse_debug):
        print('list_of_port_declarations_4', list(p))


    # {
    # 		  yyerror(@2, "error: NULL port declarations are not "
    # 		              "allowed.");
    # 		}
()


def p_list_of_port_declarations_5(p):
    '''list_of_port_declarations : list_of_port_declarations ';' '''
    if(parse_debug):
        print('list_of_port_declarations_5', list(p))


    # {
    # 		  yyerror(@2, "error: ';' is an invalid port declaration "
    # 		              "separator.");
    # 		}
()


def p_port_declaration_1(p):
    '''port_declaration : attribute_list_opt K_input net_type_opt data_type_or_implicit IDENTIFIER dimensions_opt '''
    if(parse_debug):
        print('port_declaration_1 FIXME', list(p))
    comment, dt, name = p[2], p[4], p[5]
    p[0] = absyn.port_decl(comment, dt, name)


    # { Module::port_t*ptmp;
    # 	perm_string name = lex_strings.make(p[5]);
    # 	data_type_t*use_type = p[4];
    # 	if (p[6]) use_type = new uarray_type_t(use_type, p[6]);
    # 	ptmp = pform_module_port_reference(name, @2.text, @2.first_line);
    # 	pform_module_define_port(@2, name, NetNet::PINPUT, p[3], use_type, p[1]);
    # 	port_declaration_context.port_type = NetNet::PINPUT;
    # 	port_declaration_context.port_net_type = p[3];
    # 	port_declaration_context.data_type = p[4];
    # 	delete[]p[5];
    # 	p[0] = ptmp;
    #       }
()


def p_port_declaration_2(p):
    '''port_declaration : attribute_list_opt K_input K_wreal IDENTIFIER '''
    if(parse_debug):
        print('port_declaration_2', list(p))


    # { Module::port_t*ptmp;
    # 	perm_string name = lex_strings.make(p[4]);
    # 	ptmp = pform_module_port_reference(name, @2.text,
    # 					   @2.first_line);
    # 	real_type_t*real_type = new real_type_t(real_type_t::REAL);
    # 	FILE_NAME(real_type, @3);
    # 	pform_module_define_port(@2, name, NetNet::PINPUT,
    # 				 NetNet::WIRE, real_type, p[1]);
    # 	port_declaration_context.port_type = NetNet::PINPUT;
    # 	port_declaration_context.port_net_type = NetNet::WIRE;
    # 	port_declaration_context.data_type = real_type;
    # 	delete[]p[4];
    # 	p[0] = ptmp;
    #       }
()


def p_port_declaration_3(p):
    '''port_declaration : attribute_list_opt K_inout net_type_opt data_type_or_implicit IDENTIFIER dimensions_opt '''
    if(parse_debug):
        print('port_declaration_3', list(p))


    # { Module::port_t*ptmp;
    # 	perm_string name = lex_strings.make(p[5]);
    # 	ptmp = pform_module_port_reference(name, @2.text, @2.first_line);
    # 	pform_module_define_port(@2, name, NetNet::PINOUT, p[3], p[4], p[1]);
    # 	port_declaration_context.port_type = NetNet::PINOUT;
    # 	port_declaration_context.port_net_type = p[3];
    # 	port_declaration_context.data_type = p[4];
    # 	delete[]p[5];
    # 	if (p[6]) {
    # 	      yyerror(@6, "sorry: Inout ports with unpacked dimensions not supported.");
    # 	      delete p[6];
    # 	}
    # 	p[0] = ptmp;
    #       }
()


def p_port_declaration_4(p):
    '''port_declaration : attribute_list_opt K_inout K_wreal IDENTIFIER '''
    if(parse_debug):
        print('port_declaration_4', list(p))


    # { Module::port_t*ptmp;
    # 	perm_string name = lex_strings.make(p[4]);
    # 	ptmp = pform_module_port_reference(name, @2.text,
    # 					   @2.first_line);
    # 	real_type_t*real_type = new real_type_t(real_type_t::REAL);
    # 	FILE_NAME(real_type, @3);
    # 	pform_module_define_port(@2, name, NetNet::PINOUT,
    # 				 NetNet::WIRE, real_type, p[1]);
    # 	port_declaration_context.port_type = NetNet::PINOUT;
    # 	port_declaration_context.port_net_type = NetNet::WIRE;
    # 	port_declaration_context.data_type = real_type;
    # 	delete[]p[4];
    # 	p[0] = ptmp;
    #       }
()


def p_port_declaration_5(p):
    '''port_declaration : attribute_list_opt K_output net_type_opt data_type_or_implicit IDENTIFIER dimensions_opt '''
    if(parse_debug):
        print('port_declaration_5 FIXME', list(p))
    comment, dt, name = p[2], p[4], p[5]
    p[0] = absyn.port_decl(comment, dt, name)


    # { Module::port_t*ptmp;
    # 	perm_string name = lex_strings.make(p[5]);
    # 	data_type_t*use_dtype = p[4];
    # 	if (p[6]) use_dtype = new uarray_type_t(use_dtype, p[6]);
    # 	NetNet::Type use_type = p[3];
    # 	if (use_type == NetNet::IMPLICIT) {
    # 	      if (vector_type_t*dtype = dynamic_cast<vector_type_t*> (p[4])) {
    # 		    if (dtype->reg_flag)
    # 			  use_type = NetNet::REG;
    # 		    else if (dtype->implicit_flag)
    # 			  use_type = NetNet::IMPLICIT;
    # 		    else
    # 			  use_type = NetNet::IMPLICIT_REG;
    #
    # 		      // The SystemVerilog types that can show up as
    # 		      // output ports are implicitly (on the inside)
    # 		      // variables because "reg" is not valid syntax
    # 		      // here.
    # 	      } else if (dynamic_cast<atom2_type_t*> (p[4])) {
    # 		    use_type = NetNet::IMPLICIT_REG;
    # 	      } else if (dynamic_cast<struct_type_t*> (p[4])) {
    # 		    use_type = NetNet::IMPLICIT_REG;
    # 	      } else if (enum_type_t*etype = dynamic_cast<enum_type_t*> (p[4])) {
    # 		    if(etype->base_type == IVL_VT_LOGIC)
    # 			use_type = NetNet::IMPLICIT_REG;
    # 	      }
    # 	}
    # 	ptmp = pform_module_port_reference(name, @2.text, @2.first_line);
    # 	pform_module_define_port(@2, name, NetNet::POUTPUT, use_type, use_dtype, p[1]);
    # 	port_declaration_context.port_type = NetNet::POUTPUT;
    # 	port_declaration_context.port_net_type = use_type;
    # 	port_declaration_context.data_type = p[4];
    # 	delete[]p[5];
    # 	p[0] = ptmp;
    #       }
()


def p_port_declaration_6(p):
    '''port_declaration : attribute_list_opt K_output K_wreal IDENTIFIER '''
    if(parse_debug):
        print('port_declaration_6', list(p))


    # { Module::port_t*ptmp;
    # 	perm_string name = lex_strings.make(p[4]);
    # 	ptmp = pform_module_port_reference(name, @2.text,
    # 					   @2.first_line);
    # 	real_type_t*real_type = new real_type_t(real_type_t::REAL);
    # 	FILE_NAME(real_type, @3);
    # 	pform_module_define_port(@2, name, NetNet::POUTPUT,
    # 				 NetNet::WIRE, real_type, p[1]);
    # 	port_declaration_context.port_type = NetNet::POUTPUT;
    # 	port_declaration_context.port_net_type = NetNet::WIRE;
    # 	port_declaration_context.data_type = real_type;
    # 	delete[]p[4];
    # 	p[0] = ptmp;
    #       }
()


def p_port_declaration_7(p):
    '''port_declaration : attribute_list_opt K_output net_type_opt data_type_or_implicit IDENTIFIER '=' expression '''
    if(parse_debug):
        print('port_declaration_7', list(p))


    # { Module::port_t*ptmp;
    # 	perm_string name = lex_strings.make(p[5]);
    # 	NetNet::Type use_type = p[3];
    # 	if (use_type == NetNet::IMPLICIT) {
    # 	      if (vector_type_t*dtype = dynamic_cast<vector_type_t*> (p[4])) {
    # 		    if (dtype->reg_flag)
    # 			  use_type = NetNet::REG;
    # 		    else
    # 			  use_type = NetNet::IMPLICIT_REG;
    # 	      } else {
    # 		    use_type = NetNet::IMPLICIT_REG;
    # 	      }
    # 	}
    # 	ptmp = pform_module_port_reference(name, @2.text, @2.first_line);
    # 	pform_module_define_port(@2, name, NetNet::POUTPUT, use_type, p[4], p[1]);
    # 	port_declaration_context.port_type = NetNet::PINOUT;
    # 	port_declaration_context.port_net_type = use_type;
    # 	port_declaration_context.data_type = p[4];
    #
    # 	pform_make_var_init(@5, name, p[7]);
    #
    # 	delete[]p[5];
    # 	p[0] = ptmp;
    #       }
()


def p_net_type_opt_1(p):
    '''net_type_opt : net_type '''
    if(parse_debug):
        print('net_type_opt_1', list(p))
    p[0] = p[1]


()


def p_net_type_opt_2(p):
    '''net_type_opt :  '''
    if(parse_debug > 2):
        print('net_type_opt_2', list(p))
    p[0] = NN_IMPLICIT


()


def p_unsigned_signed_opt_1(p):
    '''unsigned_signed_opt : K_signed '''
    if(parse_debug):
        print('unsigned_signed_opt_1', list(p))
    p[0] = True


()


def p_unsigned_signed_opt_2(p):
    '''unsigned_signed_opt : K_unsigned '''
    if(parse_debug):
        print('unsigned_signed_opt_2', list(p))
    p[0] = False


()


def p_unsigned_signed_opt_3(p):
    '''unsigned_signed_opt :  '''
    if(parse_debug):
        print('unsigned_signed_opt_3', list(p))
    p[0] = False


()


def p_signed_unsigned_opt_1(p):
    '''signed_unsigned_opt : K_signed '''
    if(parse_debug):
        print('signed_unsigned_opt_1', list(p))
    p[0] = True


()


def p_signed_unsigned_opt_2(p):
    '''signed_unsigned_opt : K_unsigned '''
    if(parse_debug):
        print('signed_unsigned_opt_2', list(p))
    p[0] = False


()


def p_signed_unsigned_opt_3(p):
    '''signed_unsigned_opt :  '''
    if(parse_debug):
        print('signed_unsigned_opt_3', list(p))
    p[0] = True


()


def p_atom2_type_1(p):
    '''atom2_type : K_byte '''
    if(parse_debug):
        print('atom2_type_1', list(p))


    # { p[0] = 8; }
()


def p_atom2_type_2(p):
    '''atom2_type : K_shortint '''
    if(parse_debug):
        print('atom2_type_2', list(p))


    # { p[0] = 16; }
()


def p_atom2_type_3(p):
    '''atom2_type : K_int '''
    if(parse_debug):
        print('atom2_type_3', list(p))


    # { p[0] = 32; }
()


def p_atom2_type_4(p):
    '''atom2_type : K_longint '''
    if(parse_debug):
        print('atom2_type_4', list(p))


    # { p[0] = 64; }
()


def p_lpvalue_1(p):
    '''lpvalue : hierarchy_identifier '''
    if(parse_debug > 2):
        print('lpvalue_1', list(p))
    p[0] = p[1]


    # { PEIdent*tmp = pform_new_ident(*p[1]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    # 	delete p[1];
    #       }
()


def p_lpvalue_2(p):
    '''lpvalue : implicit_class_handle '.' hierarchy_identifier '''
    if(parse_debug):
        print('lpvalue_2', list(p))


    # { pform_name_t*t_name = p[1];
    # 	while (!p[3]->empty()) {
    # 	      t_name->push_back(p[3]->front());
    # 	      p[3]->pop_front();
    # 	}
    # 	PEIdent*tmp = new PEIdent(*t_name);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    # 	delete p[1];
    # 	delete p[3];
    #       }
()


def p_lpvalue_3(p):
    '''lpvalue : '{' expression_list_proper '}' '''
    if(parse_debug):
        print('lpvalue_3', list(p))


    # { PEConcat*tmp = new PEConcat(*p[2]);
    # 	FILE_NAME(tmp, @1);
    # 	delete p[2];
    # 	p[0] = tmp;
    #       }
()


def p_lpvalue_4(p):
    '''lpvalue : streaming_concatenation '''
    if(parse_debug):
        print('lpvalue_4', list(p))


    # { yyerror(@1, "sorry: streaming concatenation not supported in l-values.");
    # 	p[0] = None
    #       }
()


def p_cont_assign_1(p):
    '''cont_assign : lpvalue '=' expression '''
    if(parse_debug):
        print('cont_assign_1', list(p))
    absyn.cont_assign_1(p)


    # { list<PExpr*>*tmp = new list<PExpr*>;
    # 	tmp->push_back(p[1]);
    # 	tmp->push_back(p[3]);
    # 	p[0] = tmp;
    #       }
()


def p_cont_assign_list_1(p):
    '''cont_assign_list : cont_assign_list ',' cont_assign '''
    if(parse_debug):
        print('cont_assign_list_1', list(p))


    # { list<PExpr*>*tmp = p[1];
    # 	tmp->splice(tmp->end(), *p[3]);
    # 	delete p[3];
    # 	p[0] = tmp;
    #       }
()


def p_cont_assign_list_2(p):
    '''cont_assign_list : cont_assign '''
    if(parse_debug > 2):
        print('cont_assign_list_2', list(p))
    p[0] = p[1]


()


def p_module_1(p):
    '''module : attribute_list_opt module_start lifetime_opt IDENTIFIER _embed0_module module_package_import_list_opt module_parameter_port_list_opt module_port_list_opt module_attribute_foreign ';' _embed1_module timeunits_declaration_opt _embed2_module module_item_list_opt module_end _embed3_module endlabel_opt '''
    if(parse_debug > 2):
        print('module_1', list(p))
    clsdecl = absyn.module_1(p)
    p[0] = clsdecl


()


def p__embed0_module(p):
    '''_embed0_module : '''


    # { pform_startmodule(@2, p[4], p[2]==K_program, p[2]==K_interface, p[3], p[1]); }
()


def p__embed1_module(p):
    '''_embed1_module : '''


    # { pform_module_set_ports(p[8]); }
()


def p__embed2_module(p):
    '''_embed2_module : '''


    # { pform_set_scope_timescale(@2); }
()


def p__embed3_module(p):
    '''_embed3_module : '''


    # { Module::UCDriveType ucd;
    # 	  // The lexor detected `unconnected_drive directives and
    # 	  // marked what it found in the uc_drive variable. Use that
    # 	  // to generate a UCD flag for the module.
    # 	switch (uc_drive) {
    # 	    case UCD_NONE:
    # 	    default:
    # 	      ucd = Module::UCD_NONE;
    # 	      break;
    # 	    case UCD_PULL0:
    # 	      ucd = Module::UCD_PULL0;
    # 	      break;
    # 	    case UCD_PULL1:
    # 	      ucd = Module::UCD_PULL1;
    # 	      break;
    # 	}
    # 	  // Check that program/endprogram and module/endmodule
    # 	  // keywords match.
    # 	if (p[2] != p[15]) {
    # 	      switch (p[2]) {
    # 		  case K_module:
    # 		    yyerror(@15, "error: module not closed by endmodule.");
    # 		    break;
    # 		  case K_program:
    # 		    yyerror(@15, "error: program not closed by endprogram.");
    # 		    break;
    # 		  case K_interface:
    # 		    yyerror(@15, "error: interface not closed by endinterface.");
    # 		    break;
    # 		  default:
    # 		    break;
    # 	      }
    # 	}
    # 	pform_endmodule(p[4], in_celldefine, ucd);
    #       }
()


def p_module_start_1(p):
    '''module_start : K_module '''
    if(parse_debug > 1):
        print('module_start_1', list(p))


    # { p[0] = K_module; }
()


def p_module_start_2(p):
    '''module_start : K_macromodule '''
    if(parse_debug):
        print('module_start_2', list(p))


    # { p[0] = K_module; }
()


def p_module_start_3(p):
    '''module_start : K_program '''
    if(parse_debug):
        print('module_start_3', list(p))


    # { p[0] = K_program; }
()


def p_module_start_4(p):
    '''module_start : K_interface '''
    if(parse_debug):
        print('module_start_4', list(p))


    # { p[0] = K_interface; }
()


def p_module_end_1(p):
    '''module_end : K_endmodule '''
    if(parse_debug > 2):
        print('module_end_1', list(p))


    # { p[0] = K_module; }
()


def p_module_end_2(p):
    '''module_end : K_endprogram '''
    if(parse_debug):
        print('module_end_2', list(p))


    # { p[0] = K_program; }
()


def p_module_end_3(p):
    '''module_end : K_endinterface '''
    if(parse_debug):
        print('module_end_3', list(p))


    # { p[0] = K_interface; }
()


def p_endlabel_opt_1(p):
    '''endlabel_opt : ':' IDENTIFIER '''
    if(parse_debug):
        print('endlabel_opt_1', list(p))
    p[0] = p[2]


()


def p_endlabel_opt_2(p):
    '''endlabel_opt :  '''
    if(parse_debug > 2):
        print('endlabel_opt_2', list(p))


    # { p[0] = None }
()


def p_module_attribute_foreign_1(p):
    '''module_attribute_foreign : K_PSTAR IDENTIFIER K_integer IDENTIFIER '=' STRING ';' K_STARP '''
    if(parse_debug):
        print('module_attribute_foreign_1', list(p))


    # { p[0] = None }
()


def p_module_attribute_foreign_2(p):
    '''module_attribute_foreign :  '''
    if(parse_debug > 2):
        print('module_attribute_foreign_2', list(p))


    # { p[0] = None }
()


def p_module_port_list_opt_1(p):
    '''module_port_list_opt : '(' list_of_ports ')' '''
    if(parse_debug):
        print('module_port_list_opt_1', list(p))
    p[0] = p[2]


()


def p_module_port_list_opt_2(p):
    '''module_port_list_opt : '(' list_of_port_declarations ')' '''
    if(parse_debug > 2):
        print('module_port_list_opt_2', list(p))
    p[0] = p[2]


()


def p_module_port_list_opt_3(p):
    '''module_port_list_opt :  '''
    if(parse_debug):
        print('module_port_list_opt_3', list(p))


    # { p[0] = None }
()


def p_module_port_list_opt_4(p):
    '''module_port_list_opt : '(' error ')' '''
    if(parse_debug):
        print('module_port_list_opt_4', list(p))


    # { yyerror(@2, "Errors in port declarations.");
    # 	yyerrok;
    # 	p[0] = None
    #       }
()


def p_module_parameter_port_list_opt_1(p):
    '''module_parameter_port_list_opt :  '''
    if(parse_debug > 2):
        print('module_parameter_port_list_opt_1', list(p))


()


def p_module_parameter_port_list_opt_2(p):
    '''module_parameter_port_list_opt : '#' '(' module_parameter_port_list ')' '''
    if(parse_debug):
        print('module_parameter_port_list_opt_2', list(p))
    p[0] = p[3]


()


def p_module_parameter_port_list_1(p):
    '''module_parameter_port_list : K_parameter param_type parameter_assign '''
    if(parse_debug):
        print('module_parameter_port_list_1', list(p))
    p[0] = [p[3]]


()


def p_module_parameter_port_list_2(p):
    '''module_parameter_port_list : module_parameter_port_list ',' parameter_assign '''
    if(parse_debug):
        print('module_parameter_port_list_2', list(p))
    p[0] = p[1].append(p[3])


()


def p_module_parameter_port_list_3(p):
    '''module_parameter_port_list : module_parameter_port_list ',' K_parameter param_type parameter_assign '''
    if(parse_debug):
        print('module_parameter_port_list_3', list(p))
    p[1].append(Leaf(token.COMMA, ','))
    p[1].append(Leaf(token.NEWLINE, '\n'))
    p[5].prefix = '                 '  # add space after newline
    p[1].append(p[5])
    p[0] = p[1]


()


def p_module_item_1(p):
    '''module_item : module '''
    if(parse_debug):
        print('module_item_1', list(p))


()


def p_module_item_2(p):
    '''module_item : attribute_list_opt net_type data_type_or_implicit delay3_opt net_variable_list ';' '''
    if(parse_debug):
        print('module_item_2', list(p))

    p[0] = absyn.module_item_2(p[2], p[3], p[5])
    #p[0] = ["module_item_2"]+list(p)


    # { data_type_t*data_type = p[3];
    # 	if (data_type == 0) {
    # 	      data_type = new vector_type_t(IVL_VT_LOGIC, false, 0);
    # 	      FILE_NAME(data_type, @2);
    # 	}
    # 	pform_set_data_type(@2, data_type, p[5], p[2], p[1]);
    # 	if (p[4] != 0) {
    # 	      yyerror(@2, "sorry: net delays not supported.");
    # 	      delete p[4];
    # 	}
    # 	delete p[1];
    #       }
()


def p_module_item_3(p):
    '''module_item : attribute_list_opt K_wreal delay3 net_variable_list ';' '''
    if(parse_debug):
        print('module_item_3', list(p))


    # { real_type_t*tmpt = new real_type_t(real_type_t::REAL);
    # 	pform_set_data_type(@2, tmpt, p[4], NetNet::WIRE, p[1]);
    # 	if (p[3] != 0) {
    # 	      yyerror(@3, "sorry: net delays not supported.");
    # 	      delete p[3];
    # 	}
    # 	delete p[1];
    #       }
()


def p_module_item_4(p):
    '''module_item : attribute_list_opt K_wreal net_variable_list ';' '''
    if(parse_debug):
        print('module_item_4', list(p))


    # { real_type_t*tmpt = new real_type_t(real_type_t::REAL);
    # 	pform_set_data_type(@2, tmpt, p[3], NetNet::WIRE, p[1]);
    # 	delete p[1];
    #       }
()


def p_module_item_5(p):
    '''module_item : attribute_list_opt net_type data_type_or_implicit delay3_opt net_decl_assigns ';' '''
    if(parse_debug):
        print('module_item_5', list(p))


    # { data_type_t*data_type = p[3];
    # 	if (data_type == 0) {
    # 	      data_type = new vector_type_t(IVL_VT_LOGIC, false, 0);
    # 	      FILE_NAME(data_type, @2);
    # 	}
    # 	pform_makewire(@2, p[4], str_strength, p[5], p[2], data_type);
    # 	if (p[1]) {
    # 	      yywarn(@2, "Attributes are not supported on net declaration "
    # 		     "assignments and will be discarded.");
    # 	      delete p[1];
    # 	}
    #       }
()


def p_module_item_6(p):
    '''module_item : attribute_list_opt net_type data_type_or_implicit drive_strength net_decl_assigns ';' '''
    if(parse_debug):
        print('module_item_6', list(p))


    # { data_type_t*data_type = p[3];
    # 	if (data_type == 0) {
    # 	      data_type = new vector_type_t(IVL_VT_LOGIC, false, 0);
    # 	      FILE_NAME(data_type, @2);
    # 	}
    # 	pform_makewire(@2, 0, p[4], p[5], p[2], data_type);
    # 	if (p[1]) {
    # 	      yywarn(@2, "Attributes are not supported on net declaration "
    # 		     "assignments and will be discarded.");
    # 	      delete p[1];
    # 	}
    #       }
()


def p_module_item_7(p):
    '''module_item : attribute_list_opt K_wreal net_decl_assigns ';' '''
    if(parse_debug):
        print('module_item_7', list(p))


    # { real_type_t*data_type = new real_type_t(real_type_t::REAL);
    #         pform_makewire(@2, 0, str_strength, p[3], NetNet::WIRE, data_type);
    # 	if (p[1]) {
    # 	      yywarn(@2, "Attributes are not supported on net declaration "
    # 		     "assignments and will be discarded.");
    # 	      delete p[1];
    # 	}
    #       }
()


def p_module_item_8(p):
    '''module_item : K_trireg charge_strength_opt dimensions_opt delay3_opt list_of_identifiers ';' '''
    if(parse_debug):
        print('module_item_8', list(p))


    # { yyerror(@1, "sorry: trireg nets not supported.");
    # 		  delete p[3];
    # 		  delete p[4];
    # 		}
()


def p_module_item_9(p):
    '''module_item : attribute_list_opt port_direction net_type data_type_or_implicit list_of_port_identifiers ';' '''
    if(parse_debug):
        print('module_item_9', list(p))


    # { pform_module_define_port(@2, p[5], p[2], p[3], p[4], p[1]); }
()


def p_module_item_10(p):
    '''module_item : attribute_list_opt port_direction K_wreal list_of_port_identifiers ';' '''
    if(parse_debug):
        print('module_item_10', list(p))


    # { real_type_t*real_type = new real_type_t(real_type_t::REAL);
    # 	pform_module_define_port(@2, p[4], p[2], NetNet::WIRE, real_type, p[1]);
    #       }
()


def p_module_item_11(p):
    '''module_item : attribute_list_opt K_inout data_type_or_implicit list_of_port_identifiers ';' '''
    if(parse_debug):
        print('module_item_11', list(p))


    # { NetNet::Type use_type = p[3] ? NetNet::IMPLICIT : NetNet::NONE;
    # 	if (vector_type_t*dtype = dynamic_cast<vector_type_t*> (p[3])) {
    # 	      if (dtype->implicit_flag)
    # 		    use_type = NetNet::NONE;
    # 	}
    # 	if (use_type == NetNet::NONE)
    # 	      pform_set_port_type(@2, p[4], NetNet::PINOUT, p[3], p[1]);
    # 	else
    # 	      pform_module_define_port(@2, p[4], NetNet::PINOUT, use_type, p[3], p[1]);
    #       }
()


def p_module_item_12(p):
    '''module_item : attribute_list_opt K_input data_type_or_implicit list_of_port_identifiers ';' '''
    if(parse_debug):
        print('module_item_12', list(p))


    # { NetNet::Type use_type = p[3] ? NetNet::IMPLICIT : NetNet::NONE;
    # 	if (vector_type_t*dtype = dynamic_cast<vector_type_t*> (p[3])) {
    # 	      if (dtype->implicit_flag)
    # 		    use_type = NetNet::NONE;
    # 	}
    # 	if (use_type == NetNet::NONE)
    # 	      pform_set_port_type(@2, p[4], NetNet::PINPUT, p[3], p[1]);
    # 	else
    # 	      pform_module_define_port(@2, p[4], NetNet::PINPUT, use_type, p[3], p[1]);
    #       }
()


def p_module_item_13(p):
    '''module_item : attribute_list_opt K_output data_type_or_implicit list_of_variable_port_identifiers ';' '''
    if(parse_debug):
        print('module_item_13', list(p))


    # { NetNet::Type use_type = p[3] ? NetNet::IMPLICIT : NetNet::NONE;
    # 	if (vector_type_t*dtype = dynamic_cast<vector_type_t*> (p[3])) {
    # 	      if (dtype->implicit_flag)
    # 		    use_type = NetNet::NONE;
    # 	      else if (dtype->reg_flag)
    # 		    use_type = NetNet::REG;
    # 	      else
    # 		    use_type = NetNet::IMPLICIT_REG;
    #
    # 		// The SystemVerilog types that can show up as
    # 		// output ports are implicitly (on the inside)
    # 		// variables because "reg" is not valid syntax
    # 		// here.
    # 	} else if (dynamic_cast<atom2_type_t*> (p[3])) {
    # 	      use_type = NetNet::IMPLICIT_REG;
    # 	} else if (dynamic_cast<struct_type_t*> (p[3])) {
    # 	      use_type = NetNet::IMPLICIT_REG;
    # 	} else if (enum_type_t*etype = dynamic_cast<enum_type_t*> (p[3])) {
    # 	      if(etype->base_type == IVL_VT_LOGIC)
    # 		  use_type = NetNet::IMPLICIT_REG;
    # 	}
    # 	if (use_type == NetNet::NONE)
    # 	      pform_set_port_type(@2, p[4], NetNet::POUTPUT, p[3], p[1]);
    # 	else
    # 	      pform_module_define_port(@2, p[4], NetNet::POUTPUT, use_type, p[3], p[1]);
    #       }
()


def p_module_item_14(p):
    '''module_item : attribute_list_opt port_direction net_type data_type_or_implicit error ';' '''
    if(parse_debug):
        print('module_item_14', list(p))


    # { yyerror(@2, "error: Invalid variable list in port declaration.");
    # 	if (p[1]) delete p[1];
    # 	if (p[4]) delete p[4];
    # 	yyerrok;
    #       }
()


def p_module_item_15(p):
    '''module_item : attribute_list_opt K_inout data_type_or_implicit error ';' '''
    if(parse_debug):
        print('module_item_15', list(p))


    # { yyerror(@2, "error: Invalid variable list in port declaration.");
    # 	if (p[1]) delete p[1];
    # 	if (p[3]) delete p[3];
    # 	yyerrok;
    #       }
()


def p_module_item_16(p):
    '''module_item : attribute_list_opt K_input data_type_or_implicit error ';' '''
    if(parse_debug):
        print('module_item_16', list(p))


    # { yyerror(@2, "error: Invalid variable list in port declaration.");
    # 	if (p[1]) delete p[1];
    # 	if (p[3]) delete p[3];
    # 	yyerrok;
    #       }
()


def p_module_item_17(p):
    '''module_item : attribute_list_opt K_output data_type_or_implicit error ';' '''
    if(parse_debug):
        print('module_item_17', list(p))


    # { yyerror(@2, "error: Invalid variable list in port declaration.");
    # 	if (p[1]) delete p[1];
    # 	if (p[3]) delete p[3];
    # 	yyerrok;
    #       }
()


def p_module_item_18(p):
    '''module_item : DISCIPLINE_IDENTIFIER list_of_identifiers ';' '''
    if(parse_debug):
        print('module_item_18', list(p))


    # { pform_attach_discipline(@1, p[1], p[2]); }
()


def p_module_item_19(p):
    '''module_item : attribute_list_opt _embed0_module_item block_item_decl '''
    if(parse_debug):
        print('module_item_19', list(p))


    # { delete attributes_in_context;
    # 	attributes_in_context = 0;
    #       }
()


def p_module_item_20(p):
    '''module_item : K_defparam _embed1_module_item defparam_assign_list ';' '''
    if(parse_debug):
        print('module_item_20', list(p))


()


def p_module_item_21(p):
    '''module_item : attribute_list_opt gatetype gate_instance_list ';' '''
    if(parse_debug):
        print('module_item_21', list(p))


    # { pform_makegates(@2, p[2], str_strength, 0, p[3], p[1]); }
()


def p_module_item_22(p):
    '''module_item : attribute_list_opt gatetype delay3 gate_instance_list ';' '''
    if(parse_debug):
        print('module_item_22', list(p))


    # { pform_makegates(@2, p[2], str_strength, p[3], p[4], p[1]); }
()


def p_module_item_23(p):
    '''module_item : attribute_list_opt gatetype drive_strength gate_instance_list ';' '''
    if(parse_debug):
        print('module_item_23', list(p))


    # { pform_makegates(@2, p[2], p[3], 0, p[4], p[1]); }
()


def p_module_item_24(p):
    '''module_item : attribute_list_opt gatetype drive_strength delay3 gate_instance_list ';' '''
    if(parse_debug):
        print('module_item_24', list(p))


    # { pform_makegates(@2, p[2], p[3], p[4], p[5], p[1]); }
()


def p_module_item_25(p):
    '''module_item : attribute_list_opt switchtype gate_instance_list ';' '''
    if(parse_debug):
        print('module_item_25', list(p))


    # { pform_makegates(@2, p[2], str_strength, 0, p[3], p[1]); }
()


def p_module_item_26(p):
    '''module_item : attribute_list_opt switchtype delay3 gate_instance_list ';' '''
    if(parse_debug):
        print('module_item_26', list(p))


    # { pform_makegates(@2, p[2], str_strength, p[3], p[4], p[1]); }
()


def p_module_item_27(p):
    '''module_item : K_pullup gate_instance_list ';' '''
    if(parse_debug):
        print('module_item_27', list(p))


    # { pform_makegates(@1, PGBuiltin::PULLUP, pull_strength, 0, p[2], 0); }
()


def p_module_item_28(p):
    '''module_item : K_pulldown gate_instance_list ';' '''
    if(parse_debug):
        print('module_item_28', list(p))


    # { pform_makegates(@1, PGBuiltin::PULLDOWN, pull_strength, 0, p[2], 0); }
()


def p_module_item_29(p):
    '''module_item : K_pullup '(' dr_strength1 ')' gate_instance_list ';' '''
    if(parse_debug):
        print('module_item_29', list(p))


    # { pform_makegates(@1, PGBuiltin::PULLUP, p[3], 0, p[5], 0); }
()


def p_module_item_30(p):
    '''module_item : K_pullup '(' dr_strength1 ',' dr_strength0 ')' gate_instance_list ';' '''
    if(parse_debug):
        print('module_item_30', list(p))


    # { pform_makegates(@1, PGBuiltin::PULLUP, p[3], 0, p[7], 0); }
()


def p_module_item_31(p):
    '''module_item : K_pullup '(' dr_strength0 ',' dr_strength1 ')' gate_instance_list ';' '''
    if(parse_debug):
        print('module_item_31', list(p))


    # { pform_makegates(@1, PGBuiltin::PULLUP, p[5], 0, p[7], 0); }
()


def p_module_item_32(p):
    '''module_item : K_pulldown '(' dr_strength0 ')' gate_instance_list ';' '''
    if(parse_debug):
        print('module_item_32', list(p))


    # { pform_makegates(@1, PGBuiltin::PULLDOWN, p[3], 0, p[5], 0); }
()


def p_module_item_33(p):
    '''module_item : K_pulldown '(' dr_strength1 ',' dr_strength0 ')' gate_instance_list ';' '''
    if(parse_debug):
        print('module_item_33', list(p))


    # { pform_makegates(@1, PGBuiltin::PULLDOWN, p[5], 0, p[7], 0); }
()


def p_module_item_34(p):
    '''module_item : K_pulldown '(' dr_strength0 ',' dr_strength1 ')' gate_instance_list ';' '''
    if(parse_debug):
        print('module_item_34', list(p))


    # { pform_makegates(@1, PGBuiltin::PULLDOWN, p[3], 0, p[7], 0); }
()


def p_module_item_35(p):
    '''module_item : attribute_list_opt IDENTIFIER parameter_value_opt gate_instance_list ';' '''
    if(parse_debug):
        print('module_item_35', list(p))


    # { perm_string tmp1 = lex_strings.make(p[2]);
    # 		  pform_make_modgates(@2, tmp1, p[3], p[4], p[1]);
    # 		  delete[]p[2];
    # 		}
()


def p_module_item_36(p):
    '''module_item : attribute_list_opt IDENTIFIER parameter_value_opt error ';' '''
    if(parse_debug):
        print('module_item_36', list(p))


    # { yyerror(@2, "error: Invalid module instantiation");
    # 		  delete[]p[2];
    # 		  if (p[1]) delete p[1];
    # 		}
()


def p_module_item_37(p):
    '''module_item : K_assign drive_strength_opt delay3_opt cont_assign_list ';' '''
    if(parse_debug > 2):
        print('module_item_37', list(p))


    # { pform_make_pgassign_list(p[4], p[3], p[2], @1.text, @1.first_line); }
()


def p_module_item_38(p):
    '''module_item : attribute_list_opt K_always statement_item '''
    if(parse_debug):
        print('module_item_38', list(p))


    # { PProcess*tmp = pform_make_behavior(IVL_PR_ALWAYS, p[3], p[1]);
    # 	FILE_NAME(tmp, @2);
    #       }
()


def p_module_item_39(p):
    '''module_item : attribute_list_opt K_always_comb statement_item '''
    if(parse_debug):
        print('module_item_39', list(p))

    absyn.always_comb(p[3], p[1])


    # { PProcess*tmp = pform_make_behavior(IVL_PR_ALWAYS_COMB, p[3], p[1]);
    # 	FILE_NAME(tmp, @2);
    #       }
()


def p_module_item_40(p):
    '''module_item : attribute_list_opt K_always_ff statement_item '''
    if(parse_debug):
        print('module_item_40', list(p))

    absyn.always_ff(p[3], p[1])


    # { PProcess*tmp = pform_make_behavior(IVL_PR_ALWAYS_FF, p[3], p[1]);
    # 	FILE_NAME(tmp, @2);
    #       }
()


def p_module_item_41(p):
    '''module_item : attribute_list_opt K_always_latch statement_item '''
    if(parse_debug):
        print('module_item_41', list(p))


    # { PProcess*tmp = pform_make_behavior(IVL_PR_ALWAYS_LATCH, p[3], p[1]);
    # 	FILE_NAME(tmp, @2);
    #       }
()


def p_module_item_42(p):
    '''module_item : attribute_list_opt K_initial statement_item '''
    if(parse_debug):
        print('module_item_42', list(p))


    # { PProcess*tmp = pform_make_behavior(IVL_PR_INITIAL, p[3], p[1]);
    # 	FILE_NAME(tmp, @2);
    #       }
()


def p_module_item_43(p):
    '''module_item : attribute_list_opt K_final statement_item '''
    if(parse_debug):
        print('module_item_43', list(p))


    # { PProcess*tmp = pform_make_behavior(IVL_PR_FINAL, p[3], p[1]);
    # 	FILE_NAME(tmp, @2);
    #       }
()


def p_module_item_44(p):
    '''module_item : attribute_list_opt K_analog analog_statement '''
    if(parse_debug):
        print('module_item_44', list(p))


    # { pform_make_analog_behavior(@2, IVL_PR_ALWAYS, p[3]); }
()


def p_module_item_45(p):
    '''module_item : attribute_list_opt assertion_item '''
    if(parse_debug):
        print('module_item_45', list(p))


()


def p_module_item_46(p):
    '''module_item : timeunits_declaration '''
    if(parse_debug):
        print('module_item_46', list(p))


()


def p_module_item_47(p):
    '''module_item : class_declaration '''
    if(parse_debug):
        print('module_item_47', list(p))


()


def p_module_item_48(p):
    '''module_item : task_declaration '''
    if(parse_debug):
        print('module_item_48', list(p))


()


def p_module_item_49(p):
    '''module_item : function_declaration '''
    if(parse_debug):
        print('module_item_49', list(p))


()


def p_module_item_50(p):
    '''module_item : K_generate generate_item_list_opt K_endgenerate '''
    if(parse_debug):
        print('module_item_50', list(p))


    # { // Test for bad nesting. I understand it, but it is illegal.
    #        if (pform_parent_generate()) {
    # 	     cerr << @1 << ": error: Generate/endgenerate regions cannot nest." << endl;
    # 	     cerr << @1 << ":      : Try removing optional generate/endgenerate keywords," << endl;
    # 	     cerr << @1 << ":      : or move them to surround the parent generate scheme." << endl;
    # 	     error_count += 1;
    # 	}
    #       }
()


def p_module_item_51(p):
    '''module_item : K_genvar list_of_identifiers ';' '''
    if(parse_debug):
        print('module_item_51', list(p))


    # { pform_genvars(@1, p[2]); }
()


def p_module_item_52(p):
    '''module_item : K_for '(' IDENTIFIER '=' expression ';' expression ';' IDENTIFIER '=' expression ')' _embed2_module_item generate_block '''
    if(parse_debug):
        print('module_item_52', list(p))


    # { pform_endgenerate(); }
()


def p_module_item_53(p):
    '''module_item : generate_if generate_block_opt K_else _embed3_module_item generate_block '''
    if(parse_debug):
        print('module_item_53', list(p))


    # { pform_endgenerate(); }
()


def p_module_item_54(p):
    '''module_item : generate_if generate_block_opt %prec less_than_K_else '''
    if(parse_debug):
        print('module_item_54', list(p))


    # { pform_endgenerate(); }
()


def p_module_item_55(p):
    '''module_item : K_case '(' expression ')' _embed4_module_item generate_case_items K_endcase '''
    if(parse_debug):
        print('module_item_55', list(p))


    # { pform_endgenerate(); }
()


def p_module_item_56(p):
    '''module_item : modport_declaration '''
    if(parse_debug):
        print('module_item_56', list(p))


()


def p_module_item_57(p):
    '''module_item : package_import_declaration '''
    if(parse_debug):
        print('module_item_57', list(p))


()


def p_module_item_58(p):
    '''module_item : attribute_list_opt K_specparam _embed5_module_item specparam_decl ';' '''
    if(parse_debug):
        print('module_item_58', list(p))


()


def p_module_item_59(p):
    '''module_item : K_specify _embed6_module_item specify_item_list_opt K_endspecify '''
    if(parse_debug):
        print('module_item_59', list(p))


()


def p_module_item_60(p):
    '''module_item : K_specify error K_endspecify '''
    if(parse_debug):
        print('module_item_60', list(p))


    # { yyerror(@1, "error: syntax error in specify block");
    # 	yyerrok;
    #       }
()


def p_module_item_61(p):
    '''module_item : error ';' '''
    if(parse_debug):
        print('module_item_61', list(p))


    # { yyerror(@2, "error: invalid module item.");
    # 		  yyerrok;
    # 		}
()


def p_module_item_62(p):
    '''module_item : K_assign error '=' expression ';' '''
    if(parse_debug):
        print('module_item_62', list(p))


    # { yyerror(@1, "error: syntax error in left side "
    # 			  "of continuous assignment.");
    # 		  yyerrok;
    # 		}
()


def p_module_item_63(p):
    '''module_item : K_assign error ';' '''
    if(parse_debug):
        print('module_item_63', list(p))


    # { yyerror(@1, "error: syntax error in "
    # 			  "continuous assignment");
    # 		  yyerrok;
    # 		}
()


def p_module_item_64(p):
    '''module_item : K_function error K_endfunction endlabel_opt '''
    if(parse_debug):
        print('module_item_64', list(p))


    # { yyerror(@1, "error: I give up on this "
    # 			  "function definition.");
    # 		  if (p[4]) {
    # 			if (!gn_system_verilog()) {
    # 			      yyerror(@4, "error: Function end names require "
    # 			                  "SystemVerilog.");
    # 			}
    # 			delete[]p[4];
    # 		  }
    # 		  yyerrok;
    # 		}
()


def p_module_item_65(p):
    '''module_item : KK_attribute '(' IDENTIFIER ',' STRING ',' STRING ')' ';' '''
    if(parse_debug):
        print('module_item_65', list(p))


    # { perm_string tmp3 = lex_strings.make(p[3]);
    # 		  perm_string tmp5 = lex_strings.make(p[5]);
    # 		  pform_set_attrib(tmp3, tmp5, p[7]);
    # 		  delete[] p[3];
    # 		  delete[] p[5];
    # 		}
()


def p_module_item_66(p):
    '''module_item : KK_attribute '(' error ')' ';' '''
    if(parse_debug):
        print('module_item_66', list(p))


    # { yyerror(@1, "error: Malformed $attribute parameter list."); }
()


def p__embed0_module_item(p):
    '''_embed0_module_item : '''


    # { attributes_in_context = p[1]; }
()


def p__embed1_module_item(p):
    '''_embed1_module_item : '''


    # { if (pform_in_interface())
    # 	      yyerror(@1, "error: Parameter overrides are not allowed "
    # 			  "in interfaces.");
    #       }
()


def p__embed2_module_item(p):
    '''_embed2_module_item : '''


    # { pform_start_generate_for(@1, p[3], p[5], p[7], p[9], p[11]); }
()


def p__embed3_module_item(p):
    '''_embed3_module_item : '''


    # { pform_start_generate_else(@1); }
()


def p__embed4_module_item(p):
    '''_embed4_module_item : '''


    # { pform_start_generate_case(@1, p[3]); }
()


def p__embed5_module_item(p):
    '''_embed5_module_item : '''


    # { if (pform_in_interface())
    # 	      yyerror(@1, "error: specparam declarations are not allowed "
    # 			  "in interfaces.");
    #       }
()


def p__embed6_module_item(p):
    '''_embed6_module_item : '''


    # { if (pform_in_interface())
    # 	      yyerror(@1, "error: specify blocks are not allowed "
    # 			  "in interfaces.");
    #       }
()


def p_module_item_list_1(p):
    '''module_item_list : module_item_list module_item '''
    if(parse_debug):
        print('module_item_list_1', list(p))


()


def p_module_item_list_2(p):
    '''module_item_list : module_item '''
    if(parse_debug > 2):
        print('module_item_list_2', list(p))


()


def p_module_item_list_opt_1(p):
    '''module_item_list_opt : module_item_list '''
    if(parse_debug > 2):
        print('module_item_list_opt_1', list(p))


()


def p_module_item_list_opt_2(p):
    '''module_item_list_opt :  '''
    if(parse_debug):
        print('module_item_list_opt_2', list(p))


()


def p_generate_if_1(p):
    '''generate_if : K_if '(' expression ')' '''
    if(parse_debug):
        print('generate_if_1', list(p))


    # { pform_start_generate_if(@1, p[3]); }
()


def p_generate_case_items_1(p):
    '''generate_case_items : generate_case_items generate_case_item '''
    if(parse_debug):
        print('generate_case_items_1', list(p))


()


def p_generate_case_items_2(p):
    '''generate_case_items : generate_case_item '''
    if(parse_debug):
        print('generate_case_items_2', list(p))


()


def p_generate_case_item_1(p):
    '''generate_case_item : expression_list_proper ':' _embed0_generate_case_item generate_block_opt '''
    if(parse_debug):
        print('generate_case_item_1', list(p))


    # { pform_endgenerate(); }
()


def p_generate_case_item_2(p):
    '''generate_case_item : K_default ':' _embed1_generate_case_item generate_block_opt '''
    if(parse_debug):
        print('generate_case_item_2', list(p))


    # { pform_endgenerate(); }
()


def p__embed0_generate_case_item(p):
    '''_embed0_generate_case_item : '''


    # { pform_generate_case_item(@1, p[1]); }
()


def p__embed1_generate_case_item(p):
    '''_embed1_generate_case_item : '''


    # { pform_generate_case_item(@1, 0); }
()


def p_generate_item_1(p):
    '''generate_item : module_item '''
    if(parse_debug):
        print('generate_item_1', list(p))


()


def p_generate_item_2(p):
    '''generate_item : K_begin generate_item_list_opt K_end '''
    if(parse_debug):
        print('generate_item_2', list(p))


    # { /* Detect and warn about anachronistic begin/end use */
    # 	if (generation_flag > GN_VER2001 && warn_anachronisms) {
    # 	      warn_count += 1;
    # 	      cerr << @1 << ": warning: Anachronistic use of begin/end to surround generate schemes." << endl;
    # 	}
    #       }
()


def p_generate_item_3(p):
    '''generate_item : K_begin ':' IDENTIFIER _embed0_generate_item generate_item_list_opt K_end '''
    if(parse_debug):
        print('generate_item_3', list(p))


    # { /* Detect and warn about anachronistic named begin/end use */
    # 	if (generation_flag > GN_VER2001 && warn_anachronisms) {
    # 	      warn_count += 1;
    # 	      cerr << @1 << ": warning: Anachronistic use of named begin/end to surround generate schemes." << endl;
    # 	}
    # 	pform_endgenerate();
    #       }
()


def p__embed0_generate_item(p):
    '''_embed0_generate_item : '''


    # {
    # 	pform_start_generate_nblock(@1, p[3]);
    #       }
()


def p_generate_item_list_1(p):
    '''generate_item_list : generate_item_list generate_item '''
    if(parse_debug):
        print('generate_item_list_1', list(p))


()


def p_generate_item_list_2(p):
    '''generate_item_list : generate_item '''
    if(parse_debug):
        print('generate_item_list_2', list(p))


()


def p_generate_item_list_opt_1(p):
    '''generate_item_list_opt : generate_item_list '''
    if(parse_debug):
        print('generate_item_list_opt_1', list(p))


()


def p_generate_item_list_opt_2(p):
    '''generate_item_list_opt :  '''
    if(parse_debug):
        print('generate_item_list_opt_2', list(p))


()


def p_generate_block_1(p):
    '''generate_block : module_item '''
    if(parse_debug):
        print('generate_block_1', list(p))


()


def p_generate_block_2(p):
    '''generate_block : K_begin generate_item_list_opt K_end '''
    if(parse_debug):
        print('generate_block_2', list(p))


()


def p_generate_block_3(p):
    '''generate_block : K_begin ':' IDENTIFIER generate_item_list_opt K_end endlabel_opt '''
    if(parse_debug):
        print('generate_block_3', list(p))


    # { pform_generate_block_name(p[3]);
    # 	if (p[6]) {
    # 	      if (strcmp(p[3],p[6]) != 0) {
    # 		    yyerror(@6, "error: End label doesn't match "
    # 				"begin name");
    # 	      }
    # 	      if (! gn_system_verilog()) {
    # 		    yyerror(@6, "error: Begin end labels require "
    # 				"SystemVerilog.");
    # 	      }
    # 	      delete[]p[6];
    # 	}
    # 	delete[]p[3];
    #       }
()


def p_generate_block_opt_1(p):
    '''generate_block_opt : generate_block '''
    if(parse_debug):
        print('generate_block_opt_1', list(p))


()


def p_generate_block_opt_2(p):
    '''generate_block_opt : ';' '''
    if(parse_debug):
        print('generate_block_opt_2', list(p))


()


def p_net_decl_assign_1(p):
    '''net_decl_assign : IDENTIFIER '=' expression '''
    if(parse_debug):
        print('net_decl_assign_1', list(p))


    # { net_decl_assign_t*tmp = new net_decl_assign_t;
    # 	tmp->next = tmp;
    # 	tmp->name = lex_strings.make(p[1]);
    # 	tmp->expr = p[3];
    # 	delete[]p[1];
    # 	p[0] = tmp;
    #       }
()


def p_net_decl_assigns_1(p):
    '''net_decl_assigns : net_decl_assigns ',' net_decl_assign '''
    if(parse_debug):
        print('net_decl_assigns_1', list(p))


    # { net_decl_assign_t*tmp = p[1];
    # 		  p[3]->next = tmp->next;
    # 		  tmp->next = p[3];
    # 		  p[0] = tmp;
    # 		}
()


def p_net_decl_assigns_2(p):
    '''net_decl_assigns : net_decl_assign '''
    if(parse_debug):
        print('net_decl_assigns_2', list(p))


    # { p[0] = p[1];
    # 		}
()


def p_bit_logic_1(p):
    '''bit_logic : K_logic '''
    if(parse_debug):
        print('bit_logic_1', list(p))


    # { p[0] = IVL_VT_LOGIC; }
()


def p_bit_logic_2(p):
    '''bit_logic : K_bool '''
    if(parse_debug):
        print('bit_logic_2', list(p))


    # { p[0] = IVL_VT_BOOL; /* Icarus misc */}
()


def p_bit_logic_3(p):
    '''bit_logic : K_bit '''
    if(parse_debug):
        print('bit_logic_3', list(p))


    # { p[0] = IVL_VT_BOOL; /* IEEE1800 / IEEE1364-2009 */}
()


def p_bit_logic_opt_1(p):
    '''bit_logic_opt : bit_logic '''
    if(parse_debug):
        print('bit_logic_opt_1', list(p))


()


def p_bit_logic_opt_2(p):
    '''bit_logic_opt :  '''
    if(parse_debug):
        print('bit_logic_opt_2', list(p))


    # { p[0] = IVL_VT_NO_TYPE; }
()


def p_net_type_1(p):
    '''net_type : K_wire '''
    if(parse_debug > 2):
        print('net_type_1', list(p))

    p[0] = "wire"


()


def p_net_type_2(p):
    '''net_type : K_tri '''
    if(parse_debug):
        print('net_type_2', list(p))


    # { p[0] = NetNet::TRI; }
()


def p_net_type_3(p):
    '''net_type : K_tri1 '''
    if(parse_debug):
        print('net_type_3', list(p))


    # { p[0] = NetNet::TRI1; }
()


def p_net_type_4(p):
    '''net_type : K_supply0 '''
    if(parse_debug):
        print('net_type_4', list(p))


    # { p[0] = NetNet::SUPPLY0; }
()


def p_net_type_5(p):
    '''net_type : K_wand '''
    if(parse_debug):
        print('net_type_5', list(p))


    # { p[0] = NetNet::WAND; }
()


def p_net_type_6(p):
    '''net_type : K_triand '''
    if(parse_debug):
        print('net_type_6', list(p))


    # { p[0] = NetNet::TRIAND; }
()


def p_net_type_7(p):
    '''net_type : K_tri0 '''
    if(parse_debug):
        print('net_type_7', list(p))


    # { p[0] = NetNet::TRI0; }
()


def p_net_type_8(p):
    '''net_type : K_supply1 '''
    if(parse_debug):
        print('net_type_8', list(p))


    # { p[0] = NetNet::SUPPLY1; }
()


def p_net_type_9(p):
    '''net_type : K_wor '''
    if(parse_debug):
        print('net_type_9', list(p))


    # { p[0] = NetNet::WOR; }
()


def p_net_type_10(p):
    '''net_type : K_trior '''
    if(parse_debug):
        print('net_type_10', list(p))


    # { p[0] = NetNet::TRIOR; }
()


def p_net_type_11(p):
    '''net_type : K_wone '''
    if(parse_debug):
        print('net_type_11', list(p))


    # { p[0] = NetNet::UNRESOLVED_WIRE;
    # 		      cerr << @1.text << ":" << @1.first_line << ": warning: "
    # 		              "'wone' is deprecated, please use 'uwire' "
    # 		              "instead." << endl;
    # 		    }
()


def p_net_type_12(p):
    '''net_type : K_uwire '''
    if(parse_debug):
        print('net_type_12', list(p))


    # { p[0] = NetNet::UNRESOLVED_WIRE; }
()


def p_param_type_1(p):
    '''param_type : bit_logic_opt unsigned_signed_opt dimensions_opt '''
    if(parse_debug):
        print('param_type_1', list(p))


    # { param_active_range = p[3];
    # 	param_active_signed = p[2];
    # 	if ((p[1] == IVL_VT_NO_TYPE) && (p[3] != 0))
    # 	      param_active_type = IVL_VT_LOGIC;
    # 	else
    # 	      param_active_type = p[1];
    #       }
()


def p_param_type_2(p):
    '''param_type : K_integer '''
    if(parse_debug):
        print('param_type_2', list(p))


    # { param_active_range = make_range_from_width(integer_width);
    # 	param_active_signed = true;
    # 	param_active_type = IVL_VT_LOGIC;
    #       }
()


def p_param_type_3(p):
    '''param_type : K_time '''
    if(parse_debug):
        print('param_type_3', list(p))


    # { param_active_range = make_range_from_width(64);
    # 	param_active_signed = false;
    # 	param_active_type = IVL_VT_LOGIC;
    #       }
()


def p_param_type_4(p):
    '''param_type : real_or_realtime '''
    if(parse_debug):
        print('param_type_4', list(p))


    # { param_active_range = 0;
    # 	param_active_signed = true;
    # 	param_active_type = IVL_VT_REAL;
    #       }
()


def p_param_type_5(p):
    '''param_type : atom2_type '''
    if(parse_debug):
        print('param_type_5', list(p))


    # { param_active_range = make_range_from_width(p[1]);
    # 	param_active_signed = true;
    # 	param_active_type = IVL_VT_BOOL;
    #       }
()


def p_param_type_6(p):
    '''param_type : TYPE_IDENTIFIER '''
    if(parse_debug):
        print('param_type_6', list(p))


    # { pform_set_param_from_type(@1, p[1].type, p[1].text, param_active_range,
    # 	                          param_active_signed, param_active_type);
    # 	delete[]p[1].text;
    #       }
()


def p_parameter_assign_list_1(p):
    '''parameter_assign_list : parameter_assign '''
    if(parse_debug):
        print('parameter_assign_list_1', list(p))


()


def p_parameter_assign_list_2(p):
    '''parameter_assign_list : parameter_assign_list ',' parameter_assign '''
    if(parse_debug):
        print('parameter_assign_list_2', list(p))


()


def p_localparam_assign_list_1(p):
    '''localparam_assign_list : localparam_assign '''
    if(parse_debug):
        print('localparam_assign_list_1', list(p))


()


def p_localparam_assign_list_2(p):
    '''localparam_assign_list : localparam_assign_list ',' localparam_assign '''
    if(parse_debug):
        print('localparam_assign_list_2', list(p))


()


def p_parameter_assign_1(p):
    '''parameter_assign : IDENTIFIER '=' expression parameter_value_ranges_opt '''
    if(parse_debug):
        print('parameter_assign_1', list(p))
    tpname = Node(syms.tname, [Leaf(token.NAME, p[1])])
    expr = Node(syms.tfpdef, [tpname, Leaf(token.EQUAL, p[2]), p[3]])
    p[0] = expr


    # { PExpr*tmp = p[3];
    # 	pform_set_parameter(@1, lex_strings.make(p[1]), param_active_type,
    # 			    param_active_signed, param_active_range, tmp, p[4]);
    # 	delete[]p[1];
    #       }
()


def p_localparam_assign_1(p):
    '''localparam_assign : IDENTIFIER '=' expression '''
    if(parse_debug):
        print('localparam_assign_1', list(p))


    # { PExpr*tmp = p[3];
    # 	pform_set_localparam(@1, lex_strings.make(p[1]), param_active_type,
    # 			     param_active_signed, param_active_range, tmp);
    # 	delete[]p[1];
    #       }
()


def p_parameter_value_ranges_opt_1(p):
    '''parameter_value_ranges_opt : parameter_value_ranges '''
    if(parse_debug):
        print('parameter_value_ranges_opt_1', list(p))
    p[0] = p[1]


()


def p_parameter_value_ranges_opt_2(p):
    '''parameter_value_ranges_opt :  '''
    if(parse_debug):
        print('parameter_value_ranges_opt_2', list(p))


    # { p[0] = None }
()


def p_parameter_value_ranges_1(p):
    '''parameter_value_ranges : parameter_value_ranges parameter_value_range '''
    if(parse_debug):
        print('parameter_value_ranges_1', list(p))


    # { p[0] = p[2]; p[0]->next = p[1]; }
()


def p_parameter_value_ranges_2(p):
    '''parameter_value_ranges : parameter_value_range '''
    if(parse_debug):
        print('parameter_value_ranges_2', list(p))


    # { p[0] = p[1]; p[0]->next = 0; }
()


def p_parameter_value_range_1(p):
    '''parameter_value_range : from_exclude '[' value_range_expression ':' value_range_expression ']' '''
    if(parse_debug):
        print('parameter_value_range_1', list(p))


    # { p[0] = pform_parameter_value_range(p[1], false, p[3], false, p[5]); }
()


def p_parameter_value_range_2(p):
    '''parameter_value_range : from_exclude '[' value_range_expression ':' value_range_expression ')' '''
    if(parse_debug):
        print('parameter_value_range_2', list(p))


    # { p[0] = pform_parameter_value_range(p[1], false, p[3], true, p[5]); }
()


def p_parameter_value_range_3(p):
    '''parameter_value_range : from_exclude '(' value_range_expression ':' value_range_expression ']' '''
    if(parse_debug):
        print('parameter_value_range_3', list(p))


    # { p[0] = pform_parameter_value_range(p[1], true, p[3], false, p[5]); }
()


def p_parameter_value_range_4(p):
    '''parameter_value_range : from_exclude '(' value_range_expression ':' value_range_expression ')' '''
    if(parse_debug):
        print('parameter_value_range_4', list(p))


    # { p[0] = pform_parameter_value_range(p[1], true, p[3], true, p[5]); }
()


def p_parameter_value_range_5(p):
    '''parameter_value_range : K_exclude expression '''
    if(parse_debug):
        print('parameter_value_range_5', list(p))


    # { p[0] = pform_parameter_value_range(true, false, p[2], false, p[2]); }
()


def p_value_range_expression_1(p):
    '''value_range_expression : expression '''
    if(parse_debug):
        print('value_range_expression_1', list(p))
    p[0] = p[1]


()


def p_value_range_expression_2(p):
    '''value_range_expression : K_inf '''
    if(parse_debug):
        print('value_range_expression_2', list(p))


    # { p[0] = None }
()


def p_value_range_expression_3(p):
    '''value_range_expression : '+' K_inf '''
    if(parse_debug):
        print('value_range_expression_3', list(p))


    # { p[0] = None }
()


def p_value_range_expression_4(p):
    '''value_range_expression : '-' K_inf '''
    if(parse_debug):
        print('value_range_expression_4', list(p))


    # { p[0] = None }
()


def p_from_exclude_1(p):
    '''from_exclude : K_from '''
    if(parse_debug):
        print('from_exclude_1', list(p))
    p[0] = False


()


def p_from_exclude_2(p):
    '''from_exclude : K_exclude '''
    if(parse_debug):
        print('from_exclude_2', list(p))
    p[0] = True


()


def p_parameter_value_opt_1(p):
    '''parameter_value_opt : '#' '(' expression_list_with_nuls ')' '''
    if(parse_debug):
        print('parameter_value_opt_1', list(p))


    # { struct parmvalue_t*tmp = new struct parmvalue_t;
    # 		  tmp->by_order = p[3];
    # 		  tmp->by_name = 0;
    # 		  p[0] = tmp;
    # 		}
()


def p_parameter_value_opt_2(p):
    '''parameter_value_opt : '#' '(' parameter_value_byname_list ')' '''
    if(parse_debug):
        print('parameter_value_opt_2', list(p))


    # { struct parmvalue_t*tmp = new struct parmvalue_t;
    # 		  tmp->by_order = 0;
    # 		  tmp->by_name = p[3];
    # 		  p[0] = tmp;
    # 		}
()


def p_parameter_value_opt_3(p):
    '''parameter_value_opt : '#' DEC_NUMBER '''
    if(parse_debug):
        print('parameter_value_opt_3', list(p))


    # { assert(p[2]);
    # 		  PENumber*tmp = new PENumber(p[2]);
    # 		  FILE_NAME(tmp, @1);
    #
    # 		  struct parmvalue_t*lst = new struct parmvalue_t;
    # 		  lst->by_order = new list<PExpr*>;
    # 		  lst->by_order->push_back(tmp);
    # 		  lst->by_name = 0;
    # 		  p[0] = lst;
    # 		  based_size = 0;
    # 		}
()


def p_parameter_value_opt_4(p):
    '''parameter_value_opt : '#' REALTIME '''
    if(parse_debug):
        print('parameter_value_opt_4', list(p))


    # { assert(p[2]);
    # 		  PEFNumber*tmp = new PEFNumber(p[2]);
    # 		  FILE_NAME(tmp, @1);
    #
    # 		  struct parmvalue_t*lst = new struct parmvalue_t;
    # 		  lst->by_order = new list<PExpr*>;
    # 		  lst->by_order->push_back(tmp);
    # 		  lst->by_name = 0;
    # 		  p[0] = lst;
    # 		}
()


def p_parameter_value_opt_5(p):
    '''parameter_value_opt : '#' error '''
    if(parse_debug):
        print('parameter_value_opt_5', list(p))


    # { yyerror(@1, "error: syntax error in parameter value "
    # 			  "assignment list.");
    # 		  p[0] = None
    # 		}
()


def p_parameter_value_opt_6(p):
    '''parameter_value_opt :  '''
    if(parse_debug):
        print('parameter_value_opt_6', list(p))


    # { p[0] = None }
()


def p_parameter_value_byname_1(p):
    '''parameter_value_byname : '.' IDENTIFIER '(' expression ')' '''
    if(parse_debug):
        print('parameter_value_byname_1', list(p))


    # { named_pexpr_t*tmp = new named_pexpr_t;
    # 		  tmp->name = lex_strings.make(p[2]);
    # 		  tmp->parm = p[4];
    # 		  delete[]p[2];
    # 		  p[0] = tmp;
    # 		}
()


def p_parameter_value_byname_2(p):
    '''parameter_value_byname : '.' IDENTIFIER '(' ')' '''
    if(parse_debug):
        print('parameter_value_byname_2', list(p))


    # { named_pexpr_t*tmp = new named_pexpr_t;
    # 		  tmp->name = lex_strings.make(p[2]);
    # 		  tmp->parm = 0;
    # 		  delete[]p[2];
    # 		  p[0] = tmp;
    # 		}
()


def p_parameter_value_byname_list_1(p):
    '''parameter_value_byname_list : parameter_value_byname '''
    if(parse_debug):
        print('parameter_value_byname_list_1', list(p))


    # { list<named_pexpr_t>*tmp = new list<named_pexpr_t>;
    # 	tmp->push_back(*p[1]);
    # 	delete p[1];
    # 	p[0] = tmp;
    #       }
()


def p_parameter_value_byname_list_2(p):
    '''parameter_value_byname_list : parameter_value_byname_list ',' parameter_value_byname '''
    if(parse_debug):
        print('parameter_value_byname_list_2', list(p))


    # { list<named_pexpr_t>*tmp = p[1];
    # 	tmp->push_back(*p[3]);
    # 	delete p[3];
    # 	p[0] = tmp;
    #       }
()


def p_port_1(p):
    '''port : port_reference '''
    if(parse_debug):
        print('port_1', list(p))
    p[0] = p[1]


()


def p_port_2(p):
    '''port : '.' IDENTIFIER '(' port_reference ')' '''
    if(parse_debug):
        print('port_2', list(p))


    # { Module::port_t*tmp = p[4];
    # 		  tmp->name = lex_strings.make(p[2]);
    # 		  delete[]p[2];
    # 		  p[0] = tmp;
    # 		}
()


def p_port_3(p):
    '''port : '{' port_reference_list '}' '''
    if(parse_debug):
        print('port_3', list(p))


    # { Module::port_t*tmp = p[2];
    # 		  tmp->name = perm_string();
    # 		  p[0] = tmp;
    # 		}
()


def p_port_4(p):
    '''port : '.' IDENTIFIER '(' '{' port_reference_list '}' ')' '''
    if(parse_debug):
        print('port_4', list(p))


    # { Module::port_t*tmp = p[5];
    # 		  tmp->name = lex_strings.make(p[2]);
    # 		  delete[]p[2];
    # 		  p[0] = tmp;
    # 		}
()


def p_port_opt_1(p):
    '''port_opt : port '''
    if(parse_debug):
        print('port_opt_1', list(p))
    p[0] = p[1]


()


def p_port_opt_2(p):
    '''port_opt :  '''
    if(parse_debug):
        print('port_opt_2', list(p))


    # { p[0] = None }
()


def p_port_name_1(p):
    '''port_name : '.' IDENTIFIER '(' expression ')' '''
    if(parse_debug):
        print('port_name_1', list(p))


    # { named_pexpr_t*tmp = new named_pexpr_t;
    # 		  tmp->name = lex_strings.make(p[2]);
    # 		  tmp->parm = p[4];
    # 		  delete[]p[2];
    # 		  p[0] = tmp;
    # 		}
()


def p_port_name_2(p):
    '''port_name : '.' IDENTIFIER '(' error ')' '''
    if(parse_debug):
        print('port_name_2', list(p))


    # { yyerror(@3, "error: invalid port connection expression.");
    # 		  named_pexpr_t*tmp = new named_pexpr_t;
    # 		  tmp->name = lex_strings.make(p[2]);
    # 		  tmp->parm = 0;
    # 		  delete[]p[2];
    # 		  p[0] = tmp;
    # 		}
()


def p_port_name_3(p):
    '''port_name : '.' IDENTIFIER '(' ')' '''
    if(parse_debug):
        print('port_name_3', list(p))


    # { named_pexpr_t*tmp = new named_pexpr_t;
    # 		  tmp->name = lex_strings.make(p[2]);
    # 		  tmp->parm = 0;
    # 		  delete[]p[2];
    # 		  p[0] = tmp;
    # 		}
()


def p_port_name_4(p):
    '''port_name : '.' IDENTIFIER '''
    if(parse_debug):
        print('port_name_4', list(p))


    # { named_pexpr_t*tmp = new named_pexpr_t;
    # 		  tmp->name = lex_strings.make(p[2]);
    # 		  tmp->parm = new PEIdent(lex_strings.make(p[2]), true);
    # 		  FILE_NAME(tmp->parm, @1);
    # 		  delete[]p[2];
    # 		  p[0] = tmp;
    # 		}
()


def p_port_name_5(p):
    '''port_name : K_DOTSTAR '''
    if(parse_debug):
        print('port_name_5', list(p))


    # { named_pexpr_t*tmp = new named_pexpr_t;
    # 		  tmp->name = lex_strings.make("*");
    # 		  tmp->parm = 0;
    # 		  p[0] = tmp;
    # 		}
()


def p_port_name_list_1(p):
    '''port_name_list : port_name_list ',' port_name '''
    if(parse_debug):
        print('port_name_list_1', list(p))


    # { list<named_pexpr_t>*tmp = p[1];
    #         tmp->push_back(*p[3]);
    # 	delete p[3];
    # 	p[0] = tmp;
    #       }
()


def p_port_name_list_2(p):
    '''port_name_list : port_name '''
    if(parse_debug):
        print('port_name_list_2', list(p))


    # { list<named_pexpr_t>*tmp = new list<named_pexpr_t>;
    #         tmp->push_back(*p[1]);
    # 	delete p[1];
    # 	p[0] = tmp;
    #       }
()


def p_port_reference_1(p):
    '''port_reference : IDENTIFIER '''
    if(parse_debug):
        print('port_reference_1', list(p))


    # { Module::port_t*ptmp;
    # 	  perm_string name = lex_strings.make(p[1]);
    # 	  ptmp = pform_module_port_reference(name, @1.text, @1.first_line);
    # 	  delete[]p[1];
    # 	  p[0] = ptmp;
    # 	}
()


def p_port_reference_2(p):
    '''port_reference : IDENTIFIER '[' expression ':' expression ']' '''
    if(parse_debug):
        print('port_reference_2', list(p))


    # { index_component_t itmp;
    # 	  itmp.sel = index_component_t::SEL_PART;
    # 	  itmp.msb = p[3];
    # 	  itmp.lsb = p[5];
    #
    # 	  name_component_t ntmp (lex_strings.make(p[1]));
    # 	  ntmp.index.push_back(itmp);
    #
    # 	  pform_name_t pname;
    # 	  pname.push_back(ntmp);
    #
    # 	  PEIdent*wtmp = new PEIdent(pname);
    # 	  FILE_NAME(wtmp, @1);
    #
    # 	  Module::port_t*ptmp = new Module::port_t;
    # 	  ptmp->name = perm_string();
    # 	  ptmp->expr.push_back(wtmp);
    #
    # 	  delete[]p[1];
    # 	  p[0] = ptmp;
    # 	}
()


def p_port_reference_3(p):
    '''port_reference : IDENTIFIER '[' expression ']' '''
    if(parse_debug):
        print('port_reference_3', list(p))


    # { index_component_t itmp;
    # 	  itmp.sel = index_component_t::SEL_BIT;
    # 	  itmp.msb = p[3];
    # 	  itmp.lsb = 0;
    #
    # 	  name_component_t ntmp (lex_strings.make(p[1]));
    # 	  ntmp.index.push_back(itmp);
    #
    # 	  pform_name_t pname;
    # 	  pname.push_back(ntmp);
    #
    # 	  PEIdent*tmp = new PEIdent(pname);
    # 	  FILE_NAME(tmp, @1);
    #
    # 	  Module::port_t*ptmp = new Module::port_t;
    # 	  ptmp->name = perm_string();
    # 	  ptmp->expr.push_back(tmp);
    # 	  delete[]p[1];
    # 	  p[0] = ptmp;
    # 	}
()


def p_port_reference_4(p):
    '''port_reference : IDENTIFIER '[' error ']' '''
    if(parse_debug):
        print('port_reference_4', list(p))


    # { yyerror(@1, "error: invalid port bit select");
    # 	  Module::port_t*ptmp = new Module::port_t;
    # 	  PEIdent*wtmp = new PEIdent(lex_strings.make(p[1]));
    # 	  FILE_NAME(wtmp, @1);
    # 	  ptmp->name = lex_strings.make(p[1]);
    # 	  ptmp->expr.push_back(wtmp);
    # 	  delete[]p[1];
    # 	  p[0] = ptmp;
    # 	}
()


def p_port_reference_list_1(p):
    '''port_reference_list : port_reference '''
    if(parse_debug):
        print('port_reference_list_1', list(p))
    p[0] = p[1]


()


def p_port_reference_list_2(p):
    '''port_reference_list : port_reference_list ',' port_reference '''
    if(parse_debug):
        print('port_reference_list_2', list(p))


    # { Module::port_t*tmp = p[1];
    # 		  append(tmp->expr, p[3]->expr);
    # 		  delete p[3];
    # 		  p[0] = tmp;
    # 		}
()


def p_dimensions_opt_1(p):
    '''dimensions_opt :  '''
    if(parse_debug > 2):
        print('dimensions_opt_1', list(p))


    # { p[0] = None }
()


def p_dimensions_opt_2(p):
    '''dimensions_opt : dimensions '''
    if(parse_debug):
        print('dimensions_opt_2', list(p))
    p[0] = p[1]


()


def p_dimensions_1(p):
    '''dimensions : variable_dimension '''
    if(parse_debug):
        print('dimensions_1', list(p))
    p[0] = p[1]


()


def p_dimensions_2(p):
    '''dimensions : dimensions variable_dimension '''
    if(parse_debug):
        print('dimensions_2', list(p))


    # { list<pform_range_t> *tmp = p[1];
    # 	if (p[2]) {
    # 	      tmp->splice(tmp->end(), *p[2]);
    # 	      delete p[2];
    # 	}
    # 	p[0] = tmp;
    #       }
()


def p_register_variable_1(p):
    '''register_variable : IDENTIFIER dimensions_opt '''
    if(parse_debug):
        print('register_variable_1', list(p))


    # { perm_string name = lex_strings.make(p[1]);
    # 	pform_makewire(@1, name, NetNet::REG,
    # 		       NetNet::NOT_A_PORT, IVL_VT_NO_TYPE, 0);
    # 	pform_set_reg_idx(name, p[2]);
    # 	p[0] = p[1];
    #       }
()


def p_register_variable_2(p):
    '''register_variable : IDENTIFIER dimensions_opt '=' expression '''
    if(parse_debug):
        print('register_variable_2', list(p))


    # { if (pform_peek_scope()->var_init_needs_explicit_lifetime()
    # 	    && (var_lifetime == LexicalScope::INHERITED)) {
    # 	      cerr << @3 << ": warning: Static variable initialization requires "
    # 			    "explicit lifetime in this context." << endl;
    # 	      warn_count += 1;
    # 	}
    # 	perm_string name = lex_strings.make(p[1]);
    # 	pform_makewire(@1, name, NetNet::REG,
    # 		       NetNet::NOT_A_PORT, IVL_VT_NO_TYPE, 0);
    # 	pform_set_reg_idx(name, p[2]);
    # 	pform_make_var_init(@1, name, p[4]);
    # 	p[0] = p[1];
    #       }
()


def p_register_variable_list_1(p):
    '''register_variable_list : register_variable '''
    if(parse_debug):
        print('register_variable_list_1', list(p))


    # { list<perm_string>*tmp = new list<perm_string>;
    # 		  tmp->push_back(lex_strings.make(p[1]));
    # 		  p[0] = tmp;
    # 		  delete[]p[1];
    # 		}
()


def p_register_variable_list_2(p):
    '''register_variable_list : register_variable_list ',' register_variable '''
    if(parse_debug):
        print('register_variable_list_2', list(p))


    # { list<perm_string>*tmp = p[1];
    # 		  tmp->push_back(lex_strings.make(p[3]));
    # 		  p[0] = tmp;
    # 		  delete[]p[3];
    # 		}
()


def p_net_variable_1(p):
    '''net_variable : IDENTIFIER dimensions_opt '''
    if(parse_debug > 2):
        print('net_variable_1', list(p))

    #p[0]= ('net_variable_1', list(p))

    # { perm_string name = lex_strings.make(p[1]);
    # 	pform_makewire(@1, name, NetNet::IMPLICIT,
    # 		       NetNet::NOT_A_PORT, IVL_VT_NO_TYPE, 0);
    # 	pform_set_reg_idx(name, p[2]);
    p[0] = [p[1], p[2]]

    #       }
()


def p_net_variable_list_1(p):
    '''net_variable_list : net_variable '''
    if(parse_debug > 2):
        print('net_variable_list_1', list(p))
    p[0] = [p[1]]


    # { list<perm_string>*tmp = new list<perm_string>;
    # 		  tmp->push_back(lex_strings.make(p[1]));
    # 		  p[0] = tmp;
    # 		  delete[]p[1];
    # 		}
()


def p_net_variable_list_2(p):
    '''net_variable_list : net_variable_list ',' net_variable '''
    if(parse_debug > 2):
        print('net_variable_list_2', list(p))
    p[0] = p[1]+[p[3]]


    # { list<perm_string>*tmp = p[1];
    # 		  tmp->push_back(lex_strings.make(p[3]));
    # 		  p[0] = tmp;
    # 		  delete[]p[3];
    # 		}
()


def p_event_variable_1(p):
    '''event_variable : IDENTIFIER dimensions_opt '''
    if(parse_debug):
        print('event_variable_1', list(p))


    # { if (p[2]) {
    # 	      yyerror(@2, "sorry: event arrays are not supported.");
    # 	      delete p[2];
    # 	}
    # 	p[0] = p[1];
    #       }
()


def p_event_variable_list_1(p):
    '''event_variable_list : event_variable '''
    if(parse_debug):
        print('event_variable_list_1', list(p))


    # { p[0] = list_from_identifier(p[1]); }
()


def p_event_variable_list_2(p):
    '''event_variable_list : event_variable_list ',' event_variable '''
    if(parse_debug):
        print('event_variable_list_2', list(p))


    # { p[0] = list_from_identifier(p[1], p[3]); }
()


def p_specify_item_1(p):
    '''specify_item : K_specparam specparam_decl ';' '''
    if(parse_debug):
        print('specify_item_1', list(p))


()


def p_specify_item_2(p):
    '''specify_item : specify_simple_path_decl ';' '''
    if(parse_debug):
        print('specify_item_2', list(p))


    # { pform_module_specify_path(p[1]);
    # 		}
()


def p_specify_item_3(p):
    '''specify_item : specify_edge_path_decl ';' '''
    if(parse_debug):
        print('specify_item_3', list(p))


    # { pform_module_specify_path(p[1]);
    # 		}
()


def p_specify_item_4(p):
    '''specify_item : K_if '(' expression ')' specify_simple_path_decl ';' '''
    if(parse_debug):
        print('specify_item_4', list(p))


    # { PSpecPath*tmp = p[5];
    # 		  if (tmp) {
    # 			tmp->conditional = true;
    # 			tmp->condition = p[3];
    # 		  }
    # 		  pform_module_specify_path(tmp);
    # 		}
()


def p_specify_item_5(p):
    '''specify_item : K_if '(' expression ')' specify_edge_path_decl ';' '''
    if(parse_debug):
        print('specify_item_5', list(p))


    # { PSpecPath*tmp = p[5];
    # 		  if (tmp) {
    # 			tmp->conditional = true;
    # 			tmp->condition = p[3];
    # 		  }
    # 		  pform_module_specify_path(tmp);
    # 		}
()


def p_specify_item_6(p):
    '''specify_item : K_ifnone specify_simple_path_decl ';' '''
    if(parse_debug):
        print('specify_item_6', list(p))


    # { PSpecPath*tmp = p[2];
    # 		  if (tmp) {
    # 			tmp->conditional = true;
    # 			tmp->condition = 0;
    # 		  }
    # 		  pform_module_specify_path(tmp);
    # 		}
()


def p_specify_item_7(p):
    '''specify_item : K_ifnone specify_edge_path_decl ';' '''
    if(parse_debug):
        print('specify_item_7', list(p))


    # { yyerror(@1, "Sorry: ifnone with an edge-sensitive path is "
    # 		              "not supported.");
    # 		  yyerrok;
    # 		}
()


def p_specify_item_8(p):
    '''specify_item : K_Sfullskew '(' spec_reference_event ',' spec_reference_event ',' delay_value ',' delay_value spec_notifier_opt ')' ';' '''
    if(parse_debug):
        print('specify_item_8', list(p))


    # { delete p[7];
    # 		  delete p[9];
    # 		}
()


def p_specify_item_9(p):
    '''specify_item : K_Shold '(' spec_reference_event ',' spec_reference_event ',' delay_value spec_notifier_opt ')' ';' '''
    if(parse_debug):
        print('specify_item_9', list(p))


    # { delete p[7];
    # 		}
()


def p_specify_item_10(p):
    '''specify_item : K_Snochange '(' spec_reference_event ',' spec_reference_event ',' delay_value ',' delay_value spec_notifier_opt ')' ';' '''
    if(parse_debug):
        print('specify_item_10', list(p))


    # { delete p[7];
    # 		  delete p[9];
    # 		}
()


def p_specify_item_11(p):
    '''specify_item : K_Speriod '(' spec_reference_event ',' delay_value spec_notifier_opt ')' ';' '''
    if(parse_debug):
        print('specify_item_11', list(p))


    # { delete p[5];
    # 		}
()


def p_specify_item_12(p):
    '''specify_item : K_Srecovery '(' spec_reference_event ',' spec_reference_event ',' delay_value spec_notifier_opt ')' ';' '''
    if(parse_debug):
        print('specify_item_12', list(p))


    # { delete p[7];
    # 		}
()


def p_specify_item_13(p):
    '''specify_item : K_Srecrem '(' spec_reference_event ',' spec_reference_event ',' delay_value ',' delay_value spec_notifier_opt ')' ';' '''
    if(parse_debug):
        print('specify_item_13', list(p))


    # { delete p[7];
    # 		  delete p[9];
    # 		}
()


def p_specify_item_14(p):
    '''specify_item : K_Sremoval '(' spec_reference_event ',' spec_reference_event ',' delay_value spec_notifier_opt ')' ';' '''
    if(parse_debug):
        print('specify_item_14', list(p))


    # { delete p[7];
    # 		}
()


def p_specify_item_15(p):
    '''specify_item : K_Ssetup '(' spec_reference_event ',' spec_reference_event ',' delay_value spec_notifier_opt ')' ';' '''
    if(parse_debug):
        print('specify_item_15', list(p))


    # { delete p[7];
    # 		}
()


def p_specify_item_16(p):
    '''specify_item : K_Ssetuphold '(' spec_reference_event ',' spec_reference_event ',' delay_value ',' delay_value spec_notifier_opt ')' ';' '''
    if(parse_debug):
        print('specify_item_16', list(p))


    # { delete p[7];
    # 		  delete p[9];
    # 		}
()


def p_specify_item_17(p):
    '''specify_item : K_Sskew '(' spec_reference_event ',' spec_reference_event ',' delay_value spec_notifier_opt ')' ';' '''
    if(parse_debug):
        print('specify_item_17', list(p))


    # { delete p[7];
    # 		}
()


def p_specify_item_18(p):
    '''specify_item : K_Stimeskew '(' spec_reference_event ',' spec_reference_event ',' delay_value spec_notifier_opt ')' ';' '''
    if(parse_debug):
        print('specify_item_18', list(p))


    # { delete p[7];
    # 		}
()


def p_specify_item_19(p):
    '''specify_item : K_Swidth '(' spec_reference_event ',' delay_value ',' expression spec_notifier_opt ')' ';' '''
    if(parse_debug):
        print('specify_item_19', list(p))


    # { delete p[5];
    # 		  delete p[7];
    # 		}
()


def p_specify_item_20(p):
    '''specify_item : K_Swidth '(' spec_reference_event ',' delay_value ')' ';' '''
    if(parse_debug):
        print('specify_item_20', list(p))


    # { delete p[5];
    # 		}
()


def p_specify_item_21(p):
    '''specify_item : K_pulsestyle_onevent specify_path_identifiers ';' '''
    if(parse_debug):
        print('specify_item_21', list(p))


    # { delete p[2];
    # 		}
()


def p_specify_item_22(p):
    '''specify_item : K_pulsestyle_ondetect specify_path_identifiers ';' '''
    if(parse_debug):
        print('specify_item_22', list(p))


    # { delete p[2];
    # 		}
()


def p_specify_item_23(p):
    '''specify_item : K_showcancelled specify_path_identifiers ';' '''
    if(parse_debug):
        print('specify_item_23', list(p))


    # { delete p[2];
    # 		}
()


def p_specify_item_24(p):
    '''specify_item : K_noshowcancelled specify_path_identifiers ';' '''
    if(parse_debug):
        print('specify_item_24', list(p))


    # { delete p[2];
    # 		}
()


def p_specify_item_list_1(p):
    '''specify_item_list : specify_item '''
    if(parse_debug):
        print('specify_item_list_1', list(p))


()


def p_specify_item_list_2(p):
    '''specify_item_list : specify_item_list specify_item '''
    if(parse_debug):
        print('specify_item_list_2', list(p))


()


def p_specify_item_list_opt_1(p):
    '''specify_item_list_opt :  '''
    if(parse_debug):
        print('specify_item_list_opt_1', list(p))


    # {  }
()


def p_specify_item_list_opt_2(p):
    '''specify_item_list_opt : specify_item_list '''
    if(parse_debug):
        print('specify_item_list_opt_2', list(p))


    # {  }
()


def p_specify_edge_path_decl_1(p):
    '''specify_edge_path_decl : specify_edge_path '=' '(' delay_value_list ')' '''
    if(parse_debug):
        print('specify_edge_path_decl_1', list(p))


    # { p[0] = pform_assign_path_delay(p[1], p[4]); }
()


def p_specify_edge_path_decl_2(p):
    '''specify_edge_path_decl : specify_edge_path '=' delay_value_simple '''
    if(parse_debug):
        print('specify_edge_path_decl_2', list(p))


    # { list<PExpr*>*tmp = new list<PExpr*>;
    # 		  tmp->push_back(p[3]);
    # 		  p[0] = pform_assign_path_delay(p[1], tmp);
    # 		}
()


def p_edge_operator_1(p):
    '''edge_operator : K_posedge '''
    if(parse_debug):
        print('edge_operator_1', list(p))
    p[0] = True


()


def p_edge_operator_2(p):
    '''edge_operator : K_negedge '''
    if(parse_debug):
        print('edge_operator_2', list(p))
    p[0] = False


()


def p_specify_edge_path_1(p):
    '''specify_edge_path : '(' specify_path_identifiers spec_polarity K_EG '(' specify_path_identifiers polarity_operator expression ')' ')' '''
    if(parse_debug):
        print('specify_edge_path_1', list(p))


    # { int edge_flag = 0;
    # 		      p[0] = pform_make_specify_edge_path(@1, edge_flag, p[2], p[3], false, p[6], p[8]); }
()


def p_specify_edge_path_2(p):
    '''specify_edge_path : '(' edge_operator specify_path_identifiers spec_polarity K_EG '(' specify_path_identifiers polarity_operator expression ')' ')' '''
    if(parse_debug):
        print('specify_edge_path_2', list(p))


    # { int edge_flag = p[2]? 1 : -1;
    # 		      p[0] = pform_make_specify_edge_path(@1, edge_flag, p[3], p[4], false, p[7], p[9]);}
()


def p_specify_edge_path_3(p):
    '''specify_edge_path : '(' specify_path_identifiers spec_polarity K_SG '(' specify_path_identifiers polarity_operator expression ')' ')' '''
    if(parse_debug):
        print('specify_edge_path_3', list(p))


    # { int edge_flag = 0;
    # 		      p[0] = pform_make_specify_edge_path(@1, edge_flag, p[2], p[3], true, p[6], p[8]); }
()


def p_specify_edge_path_4(p):
    '''specify_edge_path : '(' edge_operator specify_path_identifiers spec_polarity K_SG '(' specify_path_identifiers polarity_operator expression ')' ')' '''
    if(parse_debug):
        print('specify_edge_path_4', list(p))


    # { int edge_flag = p[2]? 1 : -1;
    # 		      p[0] = pform_make_specify_edge_path(@1, edge_flag, p[3], p[4], true, p[7], p[9]); }
()


def p_polarity_operator_1(p):
    '''polarity_operator : K_PO_POS '''
    if(parse_debug):
        print('polarity_operator_1', list(p))


()


def p_polarity_operator_2(p):
    '''polarity_operator : K_PO_NEG '''
    if(parse_debug):
        print('polarity_operator_2', list(p))


()


def p_polarity_operator_3(p):
    '''polarity_operator : ':' '''
    if(parse_debug):
        print('polarity_operator_3', list(p))


()


def p_specify_simple_path_decl_1(p):
    '''specify_simple_path_decl : specify_simple_path '=' '(' delay_value_list ')' '''
    if(parse_debug):
        print('specify_simple_path_decl_1', list(p))


    # { p[0] = pform_assign_path_delay(p[1], p[4]); }
()


def p_specify_simple_path_decl_2(p):
    '''specify_simple_path_decl : specify_simple_path '=' delay_value_simple '''
    if(parse_debug):
        print('specify_simple_path_decl_2', list(p))


    # { list<PExpr*>*tmp = new list<PExpr*>;
    # 		  tmp->push_back(p[3]);
    # 		  p[0] = pform_assign_path_delay(p[1], tmp);
    # 		}
()


def p_specify_simple_path_decl_3(p):
    '''specify_simple_path_decl : specify_simple_path '=' '(' error ')' '''
    if(parse_debug):
        print('specify_simple_path_decl_3', list(p))


    # { yyerror(@3, "Syntax error in delay value list.");
    # 		  yyerrok;
    # 		  p[0] = None
    # 		}
()


def p_specify_simple_path_1(p):
    '''specify_simple_path : '(' specify_path_identifiers spec_polarity K_EG specify_path_identifiers ')' '''
    if(parse_debug):
        print('specify_simple_path_1', list(p))


    # { p[0] = pform_make_specify_path(@1, p[2], p[3], false, p[5]); }
()


def p_specify_simple_path_2(p):
    '''specify_simple_path : '(' specify_path_identifiers spec_polarity K_SG specify_path_identifiers ')' '''
    if(parse_debug):
        print('specify_simple_path_2', list(p))


    # { p[0] = pform_make_specify_path(@1, p[2], p[3], true, p[5]); }
()


def p_specify_simple_path_3(p):
    '''specify_simple_path : '(' error ')' '''
    if(parse_debug):
        print('specify_simple_path_3', list(p))


    # { yyerror(@1, "Invalid simple path");
    # 		  yyerrok;
    # 		}
()


def p_specify_path_identifiers_1(p):
    '''specify_path_identifiers : IDENTIFIER '''
    if(parse_debug):
        print('specify_path_identifiers_1', list(p))


    # { list<perm_string>*tmp = new list<perm_string>;
    # 		  tmp->push_back(lex_strings.make(p[1]));
    # 		  p[0] = tmp;
    # 		  delete[]p[1];
    # 		}
()


def p_specify_path_identifiers_2(p):
    '''specify_path_identifiers : IDENTIFIER '[' expr_primary ']' '''
    if(parse_debug):
        print('specify_path_identifiers_2', list(p))


    # { if (gn_specify_blocks_flag) {
    # 			yywarn(@4, "Bit selects are not currently supported "
    # 				   "in path declarations. The declaration "
    # 				   "will be applied to the whole vector.");
    # 		  }
    # 		  list<perm_string>*tmp = new list<perm_string>;
    # 		  tmp->push_back(lex_strings.make(p[1]));
    # 		  p[0] = tmp;
    # 		  delete[]p[1];
    # 		}
()


def p_specify_path_identifiers_3(p):
    '''specify_path_identifiers : IDENTIFIER '[' expr_primary polarity_operator expr_primary ']' '''
    if(parse_debug):
        print('specify_path_identifiers_3', list(p))


    # { if (gn_specify_blocks_flag) {
    # 			yywarn(@4, "Part selects are not currently supported "
    # 				   "in path declarations. The declaration "
    # 				   "will be applied to the whole vector.");
    # 		  }
    # 		  list<perm_string>*tmp = new list<perm_string>;
    # 		  tmp->push_back(lex_strings.make(p[1]));
    # 		  p[0] = tmp;
    # 		  delete[]p[1];
    # 		}
()


def p_specify_path_identifiers_4(p):
    '''specify_path_identifiers : specify_path_identifiers ',' IDENTIFIER '''
    if(parse_debug):
        print('specify_path_identifiers_4', list(p))


    # { list<perm_string>*tmp = p[1];
    # 		  tmp->push_back(lex_strings.make(p[3]));
    # 		  p[0] = tmp;
    # 		  delete[]p[3];
    # 		}
()


def p_specify_path_identifiers_5(p):
    '''specify_path_identifiers : specify_path_identifiers ',' IDENTIFIER '[' expr_primary ']' '''
    if(parse_debug):
        print('specify_path_identifiers_5', list(p))


    # { if (gn_specify_blocks_flag) {
    # 			yywarn(@4, "Bit selects are not currently supported "
    # 				   "in path declarations. The declaration "
    # 				   "will be applied to the whole vector.");
    # 		  }
    # 		  list<perm_string>*tmp = p[1];
    # 		  tmp->push_back(lex_strings.make(p[3]));
    # 		  p[0] = tmp;
    # 		  delete[]p[3];
    # 		}
()


def p_specify_path_identifiers_6(p):
    '''specify_path_identifiers : specify_path_identifiers ',' IDENTIFIER '[' expr_primary polarity_operator expr_primary ']' '''
    if(parse_debug):
        print('specify_path_identifiers_6', list(p))


    # { if (gn_specify_blocks_flag) {
    # 			yywarn(@4, "Part selects are not currently supported "
    # 				   "in path declarations. The declaration "
    # 				   "will be applied to the whole vector.");
    # 		  }
    # 		  list<perm_string>*tmp = p[1];
    # 		  tmp->push_back(lex_strings.make(p[3]));
    # 		  p[0] = tmp;
    # 		  delete[]p[3];
    # 		}
()


def p_specparam_1(p):
    '''specparam : IDENTIFIER '=' expression '''
    if(parse_debug):
        print('specparam_1', list(p))


    # { PExpr*tmp = p[3];
    # 		  pform_set_specparam(@1, lex_strings.make(p[1]),
    # 		                      param_active_range, tmp);
    # 		  delete[]p[1];
    # 		}
()


def p_specparam_2(p):
    '''specparam : IDENTIFIER '=' expression ':' expression ':' expression '''
    if(parse_debug):
        print('specparam_2', list(p))


    # { PExpr*tmp = 0;
    # 		  switch (min_typ_max_flag) {
    # 		      case MIN:
    # 			tmp = p[3];
    # 			delete p[5];
    # 			delete p[7];
    # 			break;
    # 		      case TYP:
    # 			delete p[3];
    # 			tmp = p[5];
    # 			delete p[7];
    # 			break;
    # 		      case MAX:
    # 			delete p[3];
    # 			delete p[5];
    # 			tmp = p[7];
    # 			break;
    # 		  }
    # 		  if (min_typ_max_warn > 0) {
    # 		        cerr << tmp->get_fileline() << ": warning: choosing ";
    # 		        switch (min_typ_max_flag) {
    # 		            case MIN:
    # 		              cerr << "min";
    # 		              break;
    # 		            case TYP:
    # 		              cerr << "typ";
    # 		              break;
    # 		            case MAX:
    # 		              cerr << "max";
    # 		              break;
    # 		        }
    # 		        cerr << " expression." << endl;
    # 		        min_typ_max_warn -= 1;
    # 		  }
    # 		  pform_set_specparam(@1, lex_strings.make(p[1]),
    # 		                      param_active_range, tmp);
    # 		  delete[]p[1];
    # 		}
()


def p_specparam_3(p):
    '''specparam : PATHPULSE_IDENTIFIER '=' expression '''
    if(parse_debug):
        print('specparam_3', list(p))


    # { delete[]p[1];
    # 		  delete p[3];
    # 		}
()


def p_specparam_4(p):
    '''specparam : PATHPULSE_IDENTIFIER '=' '(' expression ',' expression ')' '''
    if(parse_debug):
        print('specparam_4', list(p))


    # { delete[]p[1];
    # 		  delete p[4];
    # 		  delete p[6];
    # 		}
()


def p_specparam_list_1(p):
    '''specparam_list : specparam '''
    if(parse_debug):
        print('specparam_list_1', list(p))


()


def p_specparam_list_2(p):
    '''specparam_list : specparam_list ',' specparam '''
    if(parse_debug):
        print('specparam_list_2', list(p))


()


def p_specparam_decl_1(p):
    '''specparam_decl : specparam_list '''
    if(parse_debug):
        print('specparam_decl_1', list(p))


()


def p_specparam_decl_2(p):
    '''specparam_decl : dimensions _embed0_specparam_decl specparam_list '''
    if(parse_debug):
        print('specparam_decl_2', list(p))


    # { param_active_range = 0; }
()


def p__embed0_specparam_decl(p):
    '''_embed0_specparam_decl : '''


    # { param_active_range = p[1]; }
()


def p_spec_polarity_1(p):
    '''spec_polarity : '+' '''
    if(parse_debug):
        print('spec_polarity_1', list(p))


    # { p[0] = '+'; }
()


def p_spec_polarity_2(p):
    '''spec_polarity : '-' '''
    if(parse_debug):
        print('spec_polarity_2', list(p))


    # { p[0] = '-'; }
()


def p_spec_polarity_3(p):
    '''spec_polarity :  '''
    if(parse_debug):
        print('spec_polarity_3', list(p))


    # { p[0] = None   }
()


def p_spec_reference_event_1(p):
    '''spec_reference_event : K_posedge expression '''
    if(parse_debug):
        print('spec_reference_event_1', list(p))


    # { delete p[2]; }
()


def p_spec_reference_event_2(p):
    '''spec_reference_event : K_negedge expression '''
    if(parse_debug):
        print('spec_reference_event_2', list(p))


    # { delete p[2]; }
()


def p_spec_reference_event_3(p):
    '''spec_reference_event : K_posedge expr_primary K_TAND expression '''
    if(parse_debug):
        print('spec_reference_event_3', list(p))


    # { delete p[2];
    #       delete p[4];
    #     }
()


def p_spec_reference_event_4(p):
    '''spec_reference_event : K_negedge expr_primary K_TAND expression '''
    if(parse_debug):
        print('spec_reference_event_4', list(p))


    # { delete p[2];
    #       delete p[4];
    #     }
()


def p_spec_reference_event_5(p):
    '''spec_reference_event : K_edge '[' edge_descriptor_list ']' expr_primary '''
    if(parse_debug):
        print('spec_reference_event_5', list(p))


    # { delete p[5]; }
()


def p_spec_reference_event_6(p):
    '''spec_reference_event : K_edge '[' edge_descriptor_list ']' expr_primary K_TAND expression '''
    if(parse_debug):
        print('spec_reference_event_6', list(p))


    # { delete p[5];
    #       delete p[7];
    #     }
()


def p_spec_reference_event_7(p):
    '''spec_reference_event : expr_primary K_TAND expression '''
    if(parse_debug):
        print('spec_reference_event_7', list(p))


    # { delete p[1];
    #       delete p[3];
    #     }
()


def p_spec_reference_event_8(p):
    '''spec_reference_event : expr_primary '''
    if(parse_debug):
        print('spec_reference_event_8', list(p))


    # { delete p[1]; }
()


def p_edge_descriptor_list_1(p):
    '''edge_descriptor_list : edge_descriptor_list ',' K_edge_descriptor '''
    if(parse_debug):
        print('edge_descriptor_list_1', list(p))


()


def p_edge_descriptor_list_2(p):
    '''edge_descriptor_list : K_edge_descriptor '''
    if(parse_debug):
        print('edge_descriptor_list_2', list(p))


()


def p_spec_notifier_opt_1(p):
    '''spec_notifier_opt :  '''
    if(parse_debug):
        print('spec_notifier_opt_1', list(p))


    # {  }
()


def p_spec_notifier_opt_2(p):
    '''spec_notifier_opt : spec_notifier '''
    if(parse_debug):
        print('spec_notifier_opt_2', list(p))


    # {  }
()


def p_spec_notifier_1(p):
    '''spec_notifier : ',' '''
    if(parse_debug):
        print('spec_notifier_1', list(p))


    # { args_after_notifier = 0; }
()


def p_spec_notifier_2(p):
    '''spec_notifier : ',' hierarchy_identifier '''
    if(parse_debug):
        print('spec_notifier_2', list(p))


    # { args_after_notifier = 0; delete p[2]; }
()


def p_spec_notifier_3(p):
    '''spec_notifier : spec_notifier ',' '''
    if(parse_debug):
        print('spec_notifier_3', list(p))


    # {  args_after_notifier += 1; }
()


def p_spec_notifier_4(p):
    '''spec_notifier : spec_notifier ',' hierarchy_identifier '''
    if(parse_debug):
        print('spec_notifier_4', list(p))


    # { args_after_notifier += 1;
    # 		  if (args_after_notifier >= 3)  {
    #                     cerr << @3 << ": warning: timing checks are not supported "
    # 		                  "and delayed signal \"" << *p[3]
    # 		         << "\" will not be driven." << endl;
    # 		  }
    #                   delete p[3]; }
()


def p_spec_notifier_5(p):
    '''spec_notifier : IDENTIFIER '''
    if(parse_debug):
        print('spec_notifier_5', list(p))


    # { args_after_notifier = 0; delete[]p[1]; }
()


def p_statement_item_1(p):
    '''statement_item : K_assign lpvalue '=' expression ';' '''
    if(parse_debug):
        print('statement_item_1', list(p))


    # { PCAssign*tmp = new PCAssign(p[2], p[4]);
    # 		  FILE_NAME(tmp, @1);
    # 		  p[0] = tmp;
    # 		}
()

# TODO: read all statement items


def p_statement_item_2(p):
    '''statement_item : K_deassign lpvalue ';' '''
    if(parse_debug):
        print('statement_item_2', list(p))


    # { PDeassign*tmp = new PDeassign(p[2]);
    # 		  FILE_NAME(tmp, @1);
    # 		  p[0] = tmp;
    # 		}
()


def p_statement_item_3(p):
    '''statement_item : K_force lpvalue '=' expression ';' '''
    if(parse_debug):
        print('statement_item_3', list(p))


    # { PForce*tmp = new PForce(p[2], p[4]);
    # 		  FILE_NAME(tmp, @1);
    # 		  p[0] = tmp;
    # 		}
()


def p_statement_item_4(p):
    '''statement_item : K_release lpvalue ';' '''
    if(parse_debug):
        print('statement_item_4', list(p))


    # { PRelease*tmp = new PRelease(p[2]);
    # 		  FILE_NAME(tmp, @1);
    # 		  p[0] = tmp;
    # 		}
()


def p_statement_item_5(p):
    '''statement_item : K_begin K_end '''
    if(parse_debug):
        print('statement_item_5', list(p))


    # { PBlock*tmp = new PBlock(PBlock::BL_SEQ);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_6(p):
    '''statement_item : K_begin _embed0_statement_item block_item_decls_opt _embed1_statement_item statement_or_null_list K_end '''
    if(parse_debug):
        print('statement_item_6', list(p))

        tmp = None
        if(p[3]):
            throw(Exception("assert(! current_block_stack.empty());"))
        else:
            tmp = p[5]

        p[0] = tmp


    # { PBlock*tmp;
    # 	if (p[3]) {
    # 	    pform_pop_scope();
    # 	    assert(! current_block_stack.empty());
    # 	    tmp = current_block_stack.top();
    # 	    current_block_stack.pop();
    # 	} else {
    # 	    tmp = new PBlock(PBlock::BL_SEQ);
    # 	    FILE_NAME(tmp, @1);
    # 	}
    # 	if (p[5]) tmp->set_statement(*p[5]);
    # 	delete p[5];
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_7(p):
    '''statement_item : K_begin ':' IDENTIFIER _embed2_statement_item block_item_decls_opt statement_or_null_list_opt K_end endlabel_opt '''
    if(parse_debug):
        print('statement_item_7', list(p))

    p[0] = list(p)


    # { pform_pop_scope();
    # 	assert(! current_block_stack.empty());
    # 	PBlock*tmp = current_block_stack.top();
    # 	current_block_stack.pop();
    # 	if (p[6]) tmp->set_statement(*p[6]);
    # 	delete p[6];
    # 	if (p[8]) {
    # 	      if (strcmp(p[3],p[8]) != 0) {
    # 		    yyerror(@8, "error: End label doesn't match begin name");
    # 	      }
    # 	      if (! gn_system_verilog()) {
    # 		    yyerror(@8, "error: Begin end labels require "
    # 		                "SystemVerilog.");
    # 	      }
    # 	      delete[]p[8];
    # 	}
    # 	delete[]p[3];
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_8(p):
    '''statement_item : K_fork join_keyword '''
    if(parse_debug):
        print('statement_item_8', list(p))


    # { PBlock*tmp = new PBlock(p[2]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_9(p):
    '''statement_item : K_fork _embed3_statement_item block_item_decls_opt _embed4_statement_item statement_or_null_list join_keyword '''
    if(parse_debug):
        print('statement_item_9', list(p))


    # { PBlock*tmp;
    # 	if (p[3]) {
    # 	    pform_pop_scope();
    # 	    assert(! current_block_stack.empty());
    # 	    tmp = current_block_stack.top();
    # 	    current_block_stack.pop();
    # 	    tmp->set_join_type(p[6]);
    # 	} else {
    # 	    tmp = new PBlock(p[6]);
    # 	    FILE_NAME(tmp, @1);
    # 	}
    # 	if (p[5]) tmp->set_statement(*p[5]);
    # 	delete p[5];
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_10(p):
    '''statement_item : K_fork ':' IDENTIFIER _embed5_statement_item block_item_decls_opt statement_or_null_list_opt join_keyword endlabel_opt '''
    if(parse_debug):
        print('statement_item_10', list(p))


    # { pform_pop_scope();
    #         assert(! current_block_stack.empty());
    # 	PBlock*tmp = current_block_stack.top();
    # 	current_block_stack.pop();
    # 	tmp->set_join_type(p[7]);
    # 	if (p[6]) tmp->set_statement(*p[6]);
    # 	delete p[6];
    # 	if (p[8]) {
    # 	      if (strcmp(p[3],p[8]) != 0) {
    # 		    yyerror(@8, "error: End label doesn't match fork name");
    # 	      }
    # 	      if (! gn_system_verilog()) {
    # 		    yyerror(@8, "error: Fork end labels require "
    # 		                "SystemVerilog.");
    # 	      }
    # 	      delete[]p[8];
    # 	}
    # 	delete[]p[3];
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_11(p):
    '''statement_item : K_disable hierarchy_identifier ';' '''
    if(parse_debug):
        print('statement_item_11', list(p))


    # { PDisable*tmp = new PDisable(*p[2]);
    # 		  FILE_NAME(tmp, @1);
    # 		  delete p[2];
    # 		  p[0] = tmp;
    # 		}
()


def p_statement_item_12(p):
    '''statement_item : K_disable K_fork ';' '''
    if(parse_debug):
        print('statement_item_12', list(p))


    # { pform_name_t tmp_name;
    # 		  PDisable*tmp = new PDisable(tmp_name);
    # 		  FILE_NAME(tmp, @1);
    # 		  p[0] = tmp;
    # 		}
()


def p_statement_item_13(p):
    '''statement_item : K_TRIGGER hierarchy_identifier ';' '''
    if(parse_debug):
        print('statement_item_13', list(p))


    # { PTrigger*tmp = new PTrigger(*p[2]);
    # 		  FILE_NAME(tmp, @1);
    # 		  delete p[2];
    # 		  p[0] = tmp;
    # 		}
()


def p_statement_item_14(p):
    '''statement_item : procedural_assertion_statement '''
    if(parse_debug):
        print('statement_item_14', list(p))
    p[0] = p[1]


()


def p_statement_item_15(p):
    '''statement_item : loop_statement '''
    if(parse_debug):
        print('statement_item_15', list(p))
    p[0] = p[1]


()


def p_statement_item_16(p):
    '''statement_item : jump_statement '''
    if(parse_debug):
        print('statement_item_16', list(p))
    p[0] = p[1]


()


def p_statement_item_17(p):
    '''statement_item : K_case '(' expression ')' case_items K_endcase '''
    if(parse_debug):
        print('statement_item_17', list(p))


    # { PCase*tmp = new PCase(NetCase::EQ, p[3], p[5]);
    # 		  FILE_NAME(tmp, @1);
    # 		  p[0] = tmp;
    # 		}
()


def p_statement_item_18(p):
    '''statement_item : K_casex '(' expression ')' case_items K_endcase '''
    if(parse_debug):
        print('statement_item_18', list(p))


    # { PCase*tmp = new PCase(NetCase::EQX, p[3], p[5]);
    # 		  FILE_NAME(tmp, @1);
    # 		  p[0] = tmp;
    # 		}
()


def p_statement_item_19(p):
    '''statement_item : K_casez '(' expression ')' case_items K_endcase '''
    if(parse_debug):
        print('statement_item_19', list(p))


    # { PCase*tmp = new PCase(NetCase::EQZ, p[3], p[5]);
    # 		  FILE_NAME(tmp, @1);
    # 		  p[0] = tmp;
    # 		}
()


def p_statement_item_20(p):
    '''statement_item : K_case '(' expression ')' error K_endcase '''
    if(parse_debug):
        print('statement_item_20', list(p))


    # { yyerrok; }
()


def p_statement_item_21(p):
    '''statement_item : K_casex '(' expression ')' error K_endcase '''
    if(parse_debug):
        print('statement_item_21', list(p))


    # { yyerrok; }
()


def p_statement_item_22(p):
    '''statement_item : K_casez '(' expression ')' error K_endcase '''
    if(parse_debug):
        print('statement_item_22', list(p))


    # { yyerrok; }
()


def p_statement_item_23(p):
    '''statement_item : K_if '(' expression ')' statement_or_null %prec less_than_K_else '''
    if(parse_debug):
        print('statement_item_23', list(p))

    p[0] = absyn.cond_statement3(p[3], p[5], None)


    # { PCondit*tmp = new PCondit(p[3], p[5], 0);
    # 		  FILE_NAME(tmp, @1);
    # 		  p[0] = tmp;
    # 		}
()


def p_statement_item_24(p):
    '''statement_item : K_if '(' expression ')' statement_or_null K_else statement_or_null '''
    if(parse_debug):
        print('statement_item_24', list(p))

        p[0] = absyn.cond_statement3(p[3], p[5], p[7])

    # { PCondit*tmp = new PCondit(p[3], p[5], p[7]);
    # 		  FILE_NAME(tmp, @1);
    # 		  p[0] = tmp;
    # 		}
()


def p_statement_item_25(p):
    '''statement_item : K_if '(' error ')' statement_or_null %prec less_than_K_else '''
    if(parse_debug):
        print('statement_item_25', list(p))


    # { yyerror(@1, "error: Malformed conditional expression.");
    # 		  p[0] = p[5];
    # 		}
()


def p_statement_item_26(p):
    '''statement_item : K_if '(' error ')' statement_or_null K_else statement_or_null '''
    if(parse_debug):
        print('statement_item_26', list(p))


    # { yyerror(@1, "error: Malformed conditional expression.");
    # 		  p[0] = p[5];
    # 		}
()


def p_statement_item_27(p):
    '''statement_item : compressed_statement ';' '''
    if(parse_debug):
        print('statement_item_27', list(p))
    p[0] = p[1]


()


def p_statement_item_28(p):
    '''statement_item : inc_or_dec_expression ';' '''
    if(parse_debug):
        print('statement_item_28', list(p))


    # { p[0] = pform_compressed_assign_from_inc_dec(@1, p[1]); }
()


def p_statement_item_29(p):
    '''statement_item : delay1 statement_or_null '''
    if(parse_debug):
        print('statement_item_29', list(p))


    # { PExpr*del = p[1]->front();
    # 	assert(p[1]->size() == 1);
    # 	delete p[1];
    # 	PDelayStatement*tmp = new PDelayStatement(del, p[2]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_30(p):
    '''statement_item : event_control statement_or_null '''
    if(parse_debug):
        print('statement_item_30', list(p))

    p[0] = [p[1], p[2]]


    # { PEventStatement*tmp = p[1];
    # 	if (tmp == 0) {
    # 	      yyerror(@1, "error: Invalid event control.");
    # 	      p[0] = None
    # 	} else {
    # 	      tmp->set_statement(p[2]);
    # 	      p[0] = tmp;
    # 	}
    #       }
()


def p_statement_item_31(p):
    '''statement_item : '@' '*' statement_or_null '''
    if(parse_debug):
        print('statement_item_31', list(p))


    # { PEventStatement*tmp = new PEventStatement;
    # 	FILE_NAME(tmp, @1);
    # 	tmp->set_statement(p[3]);
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_32(p):
    '''statement_item : '@' '(' '*' ')' statement_or_null '''
    if(parse_debug):
        print('statement_item_32', list(p))


    # { PEventStatement*tmp = new PEventStatement;
    # 	FILE_NAME(tmp, @1);
    # 	tmp->set_statement(p[5]);
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_33(p):
    '''statement_item : lpvalue '=' expression ';' '''
    """
    if(parse_debug):
        print('statement_item33', list(p))
    if p[3]:
        expr = Node(syms.expr_stmt, [p[1], Leaf(token.EQUAL, p[2]), p[3]])
        if(parse_debug):
            print("expr TODO", repr(expr))
    else:
        expr = Node(syms.expr_stmt, [p[1], Leaf(token.EQUAL, p[2]), ])
        if(parse_debug):
            print("expr", repr(expr))
    if(parse_debug):
        print("expr (python):'%s'" % expr)
    p[0] = expr
    """
    p[0] = absyn.assign3(p[1], p[2], p[3])


    # { PAssign*tmp = new PAssign(p[1],p[3]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_34(p):
    '''statement_item : error '=' expression ';' '''
    if(parse_debug):
        print('statement_item_34', list(p))


    # { yyerror(@2, "Syntax in assignment statement l-value.");
    # 	yyerrok;
    # 	p[0] = new PNoop;
    #       }
()


def p_statement_item_35(p):
    '''statement_item : lpvalue K_LE expression ';' '''
    if(parse_debug):
        print('statement_item_35', list(p))

    p[0] = absyn.assign3(p[1], p[2], p[3])


    # { PAssignNB*tmp = new PAssignNB(p[1],p[3]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_36(p):
    '''statement_item : error K_LE expression ';' '''
    if(parse_debug):
        print('statement_item_36', list(p))


    # { yyerror(@2, "Syntax in assignment statement l-value.");
    # 	yyerrok;
    # 	p[0] = new PNoop;
    #       }
()


def p_statement_item_37(p):
    '''statement_item : lpvalue '=' delay1 expression ';' '''
    if(parse_debug):
        print('statement_item_37', list(p))


    # { PExpr*del = p[3]->front(); p[3]->pop_front();
    # 	assert(p[3]->empty());
    # 	PAssign*tmp = new PAssign(p[1],del,p[4]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_38(p):
    '''statement_item : lpvalue K_LE delay1 expression ';' '''
    if(parse_debug):
        print('statement_item_38', list(p))


    # { PExpr*del = p[3]->front(); p[3]->pop_front();
    # 	assert(p[3]->empty());
    # 	PAssignNB*tmp = new PAssignNB(p[1],del,p[4]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_39(p):
    '''statement_item : lpvalue '=' event_control expression ';' '''
    if(parse_debug):
        print('statement_item_39', list(p))


    # { PAssign*tmp = new PAssign(p[1],0,p[3],p[4]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_40(p):
    '''statement_item : lpvalue '=' K_repeat '(' expression ')' event_control expression ';' '''
    if(parse_debug):
        print('statement_item_40', list(p))


    # { PAssign*tmp = new PAssign(p[1],p[5],p[7],p[8]);
    # 	FILE_NAME(tmp,@1);
    # 	tmp->set_lineno(@1.first_line);
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_41(p):
    '''statement_item : lpvalue K_LE event_control expression ';' '''
    if(parse_debug):
        print('statement_item_41', list(p))


    # { PAssignNB*tmp = new PAssignNB(p[1],0,p[3],p[4]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_42(p):
    '''statement_item : lpvalue K_LE K_repeat '(' expression ')' event_control expression ';' '''
    if(parse_debug):
        print('statement_item_42', list(p))


    # { PAssignNB*tmp = new PAssignNB(p[1],p[5],p[7],p[8]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_43(p):
    '''statement_item : lpvalue '=' dynamic_array_new ';' '''
    if(parse_debug):
        print('statement_item_43', list(p))


    # { PAssign*tmp = new PAssign(p[1],p[3]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_44(p):
    '''statement_item : lpvalue '=' class_new ';' '''
    if(parse_debug):
        print('statement_item_44', list(p))


    # { PAssign*tmp = new PAssign(p[1],p[3]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_45(p):
    '''statement_item : K_wait '(' expression ')' statement_or_null '''
    if(parse_debug):
        print('statement_item_45', list(p))


    # { PEventStatement*tmp;
    # 		  PEEvent*etmp = new PEEvent(PEEvent::POSITIVE, p[3]);
    # 		  tmp = new PEventStatement(etmp);
    # 		  FILE_NAME(tmp,@1);
    # 		  tmp->set_statement(p[5]);
    # 		  p[0] = tmp;
    # 		}
()


def p_statement_item_46(p):
    '''statement_item : K_wait K_fork ';' '''
    if(parse_debug):
        print('statement_item_46', list(p))


    # { PEventStatement*tmp = new PEventStatement((PEEvent*)0);
    # 		  FILE_NAME(tmp,@1);
    # 		  p[0] = tmp;
    # 		}
()


def p_statement_item_47(p):
    '''statement_item : SYSTEM_IDENTIFIER '(' expression_list_with_nuls ')' ';' '''
    if(parse_debug):
        print('statement_item_47', list(p))


    # { PCallTask*tmp = new PCallTask(lex_strings.make(p[1]), *p[3]);
    # 		  FILE_NAME(tmp,@1);
    # 		  delete[]p[1];
    # 		  delete p[3];
    # 		  p[0] = tmp;
    # 		}
()


def p_statement_item_48(p):
    '''statement_item : SYSTEM_IDENTIFIER ';' '''
    if(parse_debug):
        print('statement_item_48', list(p))


    # { list<PExpr*>pt;
    # 		  PCallTask*tmp = new PCallTask(lex_strings.make(p[1]), pt);
    # 		  FILE_NAME(tmp,@1);
    # 		  delete[]p[1];
    # 		  p[0] = tmp;
    # 		}
()


def p_statement_item_49(p):
    '''statement_item : hierarchy_identifier '(' expression_list_with_nuls ')' ';' '''
    if(parse_debug):
        print('statement_item_49', list(p))


    # { PCallTask*tmp = pform_make_call_task(@1, *p[1], *p[3]);
    # 	delete p[1];
    # 	delete p[3];
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_50(p):
    '''statement_item : hierarchy_identifier K_with '{' constraint_block_item_list_opt '}' ';' '''
    if(parse_debug):
        print('statement_item_50', list(p))


    # { /* ....randomize with { <constraints> } */
    # 	if (p[1] && peek_tail_name(*p[1]) == "randomize") {
    # 	      if (!gn_system_verilog())
    # 		    yyerror(@2, "error: Randomize with constraint requires SystemVerilog.");
    # 	      else
    # 		    yyerror(@2, "sorry: Randomize with constraint not supported.");
    # 	} else {
    # 	      yyerror(@2, "error: Constraint block can only be applied to randomize method.");
    # 	}
    # 	list<PExpr*>pt;
    # 	PCallTask*tmp = new PCallTask(*p[1], pt);
    # 	FILE_NAME(tmp, @1);
    # 	delete p[1];
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_51(p):
    '''statement_item : implicit_class_handle '.' hierarchy_identifier '(' expression_list_with_nuls ')' ';' '''
    if(parse_debug):
        print('statement_item_51', list(p))


    # { pform_name_t*t_name = p[1];
    # 	while (! p[3]->empty()) {
    # 	      t_name->push_back(p[3]->front());
    # 	      p[3]->pop_front();
    # 	}
    # 	PCallTask*tmp = new PCallTask(*t_name, *p[5]);
    # 	FILE_NAME(tmp, @1);
    # 	delete p[1];
    # 	delete p[3];
    # 	delete p[5];
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_52(p):
    '''statement_item : hierarchy_identifier ';' '''
    if(parse_debug):
        print('statement_item_52', list(p))


    # { list<PExpr*>pt;
    # 	PCallTask*tmp = pform_make_call_task(@1, *p[1], pt);
    # 	delete p[1];
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_53(p):
    '''statement_item : implicit_class_handle '.' K_new '(' expression_list_with_nuls ')' ';' '''
    if(parse_debug):
        print('statement_item_53', list(p))


    # { PChainConstructor*tmp = new PChainConstructor(*p[5]);
    # 	FILE_NAME(tmp, @3);
    # 	delete p[1];
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_54(p):
    '''statement_item : hierarchy_identifier '(' error ')' ';' '''
    if(parse_debug):
        print('statement_item_54', list(p))


    # { yyerror(@3, "error: Syntax error in task arguments.");
    # 	list<PExpr*>pt;
    # 	PCallTask*tmp = pform_make_call_task(@1, *p[1], pt);
    # 	delete p[1];
    # 	p[0] = tmp;
    #       }
()


def p_statement_item_55(p):
    '''statement_item : error ';' '''
    if(parse_debug):
        print('statement_item_55', list(p))


    # { yyerror(@2, "error: malformed statement");
    # 	yyerrok;
    # 	p[0] = new PNoop;
    #       }
()


def p__embed0_statement_item(p):
    '''_embed0_statement_item : '''


    # { PBlock*tmp = pform_push_block_scope(0, PBlock::BL_SEQ);
    # 	FILE_NAME(tmp, @1);
    # 	current_block_stack.push(tmp);
    #       }
()


def p__embed1_statement_item(p):
    '''_embed1_statement_item : '''


    # { if (p[3]) {
    # 	    if (! gn_system_verilog()) {
    # 		  yyerror("error: Variable declaration in unnamed block "
    # 		          "requires SystemVerilog.");
    # 	    }
    # 	} else {
    # 	    /* If there are no declarations in the scope then just delete it. */
    # 	    pform_pop_scope();
    # 	    assert(! current_block_stack.empty());
    # 	    PBlock*tmp = current_block_stack.top();
    # 	    current_block_stack.pop();
    # 	    delete tmp;
    # 	}
    #       }
()


def p__embed2_statement_item(p):
    '''_embed2_statement_item : '''


    # { PBlock*tmp = pform_push_block_scope(p[3], PBlock::BL_SEQ);
    # 	FILE_NAME(tmp, @1);
    # 	current_block_stack.push(tmp);
    #       }
()


def p__embed3_statement_item(p):
    '''_embed3_statement_item : '''


    # { PBlock*tmp = pform_push_block_scope(0, PBlock::BL_PAR);
    # 	FILE_NAME(tmp, @1);
    # 	current_block_stack.push(tmp);
    #       }
()


def p__embed4_statement_item(p):
    '''_embed4_statement_item : '''


    # { if (p[3]) {
    # 	    if (! gn_system_verilog()) {
    # 		  yyerror("error: Variable declaration in unnamed block "
    # 		          "requires SystemVerilog.");
    # 	    }
    # 	} else {
    # 	    /* If there are no declarations in the scope then just delete it. */
    # 	    pform_pop_scope();
    # 	    assert(! current_block_stack.empty());
    # 	    PBlock*tmp = current_block_stack.top();
    # 	    current_block_stack.pop();
    # 	    delete tmp;
    # 	}
    #       }
()


def p__embed5_statement_item(p):
    '''_embed5_statement_item : '''


    # { PBlock*tmp = pform_push_block_scope(p[3], PBlock::BL_PAR);
    # 	FILE_NAME(tmp, @1);
    # 	current_block_stack.push(tmp);
    #       }
()


def p_compressed_statement_1(p):
    '''compressed_statement : lpvalue K_PLUS_EQ expression '''
    if(parse_debug):
        print('compressed_statement_1', list(p))


    # { PAssign*tmp = new PAssign(p[1], '+', p[3]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_compressed_statement_2(p):
    '''compressed_statement : lpvalue K_MINUS_EQ expression '''
    if(parse_debug):
        print('compressed_statement_2', list(p))


    # { PAssign*tmp = new PAssign(p[1], '-', p[3]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_compressed_statement_3(p):
    '''compressed_statement : lpvalue K_MUL_EQ expression '''
    if(parse_debug):
        print('compressed_statement_3', list(p))


    # { PAssign*tmp = new PAssign(p[1], '*', p[3]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_compressed_statement_4(p):
    '''compressed_statement : lpvalue K_DIV_EQ expression '''
    if(parse_debug):
        print('compressed_statement_4', list(p))


    # { PAssign*tmp = new PAssign(p[1], '/', p[3]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_compressed_statement_5(p):
    '''compressed_statement : lpvalue K_MOD_EQ expression '''
    if(parse_debug):
        print('compressed_statement_5', list(p))


    # { PAssign*tmp = new PAssign(p[1], '%', p[3]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_compressed_statement_6(p):
    '''compressed_statement : lpvalue K_AND_EQ expression '''
    if(parse_debug):
        print('compressed_statement_6', list(p))


    # { PAssign*tmp = new PAssign(p[1], '&', p[3]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_compressed_statement_7(p):
    '''compressed_statement : lpvalue K_OR_EQ expression '''
    if(parse_debug):
        print('compressed_statement_7', list(p))


    # { PAssign*tmp = new PAssign(p[1], '|', p[3]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_compressed_statement_8(p):
    '''compressed_statement : lpvalue K_XOR_EQ expression '''
    if(parse_debug):
        print('compressed_statement_8', list(p))


    # { PAssign*tmp = new PAssign(p[1], '^', p[3]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_compressed_statement_9(p):
    '''compressed_statement : lpvalue K_LS_EQ expression '''
    if(parse_debug):
        print('compressed_statement_9', list(p))


    # { PAssign  *tmp = new PAssign(p[1], 'l', p[3]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_compressed_statement_10(p):
    '''compressed_statement : lpvalue K_RS_EQ expression '''
    if(parse_debug):
        print('compressed_statement_10', list(p))


    # { PAssign*tmp = new PAssign(p[1], 'r', p[3]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_compressed_statement_11(p):
    '''compressed_statement : lpvalue K_RSS_EQ expression '''
    if(parse_debug):
        print('compressed_statement_11', list(p))


    # { PAssign  *tmp = new PAssign(p[1], 'R', p[3]);
    # 	FILE_NAME(tmp, @1);
    # 	p[0] = tmp;
    #       }
()


def p_statement_or_null_list_opt_1(p):
    '''statement_or_null_list_opt : statement_or_null_list '''
    if(parse_debug):
        print('statement_or_null_list_opt_1', list(p))
    p[0] = p[1]


()


def p_statement_or_null_list_opt_2(p):
    '''statement_or_null_list_opt :  '''
    if(parse_debug):
        print('statement_or_null_list_opt_2', list(p))


    # { p[0] = None }
()


def p_statement_or_null_list_1(p):
    '''statement_or_null_list : statement_or_null_list statement_or_null '''
    if(parse_debug):
        print('statement_or_null_list_1', list(p))
        print(p[1])

    tmp = p[1]
    if(tmp is None):
        tmp = StatementList()
    if (p[2]):
        tmp.add_statement(p[2])
    p[0] = tmp

    # { vector<Statement*>*tmp = p[1];
    # 	if (p[2]) tmp->push_back(p[2]);
    # 	p[0] = tmp;
    #       }
()


def p_statement_or_null_list_2(p):
    '''statement_or_null_list : statement_or_null '''
    if(parse_debug):
        print('statement_or_null_list_2', list(p))

    tmp = StatementList()
    if (p[1]):
        tmp.add_statement(p[1])
    p[0] = tmp


    # { vector<Statement*>*tmp = new vector<Statement*>(0);
    # 	if (p[1]) tmp->push_back(p[1]);
    # 	p[0] = tmp;
    #       }
()


def p_analog_statement_1(p):
    '''analog_statement : branch_probe_expression K_CONTRIBUTE expression ';' '''
    if(parse_debug):
        print('analog_statement_1', list(p))


    # { p[0] = pform_contribution_statement(@2, p[1], p[3]); }
()


def p_task_item_1(p):
    '''task_item : block_item_decl '''
    if(parse_debug):
        print('task_item_1', list(p))


    # { p[0] = new vector<pform_tf_port_t>(0); }
()


def p_task_item_2(p):
    '''task_item : tf_port_declaration '''
    if(parse_debug):
        print('task_item_2', list(p))
    p[0] = p[1]


()


def p_task_item_list_1(p):
    '''task_item_list : task_item_list task_item '''
    if(parse_debug):
        print('task_item_list_1', list(p))


    # { vector<pform_tf_port_t>*tmp = p[1];
    # 	size_t s1 = tmp->size();
    # 	tmp->resize(s1 + p[2]->size());
    # 	for (size_t idx = 0 ; idx < p[2]->size() ; idx += 1)
    # 	      tmp->at(s1 + idx) = p[2]->at(idx);
    # 	delete p[2];
    # 	p[0] = tmp;
    #       }
()


def p_task_item_list_2(p):
    '''task_item_list : task_item '''
    if(parse_debug):
        print('task_item_list_2', list(p))
    p[0] = p[1]


()


def p_task_item_list_opt_1(p):
    '''task_item_list_opt : task_item_list '''
    if(parse_debug):
        print('task_item_list_opt_1', list(p))
    p[0] = p[1]


()


def p_task_item_list_opt_2(p):
    '''task_item_list_opt :  '''
    if(parse_debug):
        print('task_item_list_opt_2', list(p))


    # { p[0] = None }
()


def p_tf_port_list_opt_1(p):
    '''tf_port_list_opt : tf_port_list '''
    if(parse_debug):
        print('tf_port_list_opt_1', list(p))
    p[0] = p[1]


()


def p_tf_port_list_opt_2(p):
    '''tf_port_list_opt :  '''
    if(parse_debug):
        print('tf_port_list_opt_2', list(p))


    # { p[0] = None }
()


def p_udp_body_1(p):
    '''udp_body : K_table udp_entry_list K_endtable '''
    if(parse_debug):
        print('udp_body_1', list(p))


    # { lex_end_table();
    # 	p[0] = p[2];
    #       }
()


def p_udp_body_2(p):
    '''udp_body : K_table K_endtable '''
    if(parse_debug):
        print('udp_body_2', list(p))


    # { lex_end_table();
    # 	yyerror(@1, "error: Empty UDP table.");
    # 	p[0] = None
    #       }
()


def p_udp_body_3(p):
    '''udp_body : K_table error K_endtable '''
    if(parse_debug):
        print('udp_body_3', list(p))


    # { lex_end_table();
    # 	yyerror(@2, "Errors in UDP table");
    # 	yyerrok;
    # 	p[0] = None
    #       }
()


def p_udp_entry_list_1(p):
    '''udp_entry_list : udp_comb_entry_list '''
    if(parse_debug):
        print('udp_entry_list_1', list(p))


()


def p_udp_entry_list_2(p):
    '''udp_entry_list : udp_sequ_entry_list '''
    if(parse_debug):
        print('udp_entry_list_2', list(p))


()


def p_udp_comb_entry_1(p):
    '''udp_comb_entry : udp_input_list ':' udp_output_sym ';' '''
    if(parse_debug):
        print('udp_comb_entry_1', list(p))


    # { char*tmp = new char[strlen(p[1])+3];
    # 		  strcpy(tmp, p[1]);
    # 		  char*tp = tmp+strlen(tmp);
    # 		  *tp++ = ':';
    # 		  *tp++ = p[3];
    # 		  *tp++ = 0;
    # 		  delete[]p[1];
    # 		  p[0] = tmp;
    # 		}
()


def p_udp_comb_entry_list_1(p):
    '''udp_comb_entry_list : udp_comb_entry '''
    if(parse_debug):
        print('udp_comb_entry_list_1', list(p))


    # { list<string>*tmp = new list<string>;
    # 		  tmp->push_back(p[1]);
    # 		  delete[]p[1];
    # 		  p[0] = tmp;
    # 		}
()


def p_udp_comb_entry_list_2(p):
    '''udp_comb_entry_list : udp_comb_entry_list udp_comb_entry '''
    if(parse_debug):
        print('udp_comb_entry_list_2', list(p))


    # { list<string>*tmp = p[1];
    # 		  tmp->push_back(p[2]);
    # 		  delete[]p[2];
    # 		  p[0] = tmp;
    # 		}
()


def p_udp_sequ_entry_list_1(p):
    '''udp_sequ_entry_list : udp_sequ_entry '''
    if(parse_debug):
        print('udp_sequ_entry_list_1', list(p))


    # { list<string>*tmp = new list<string>;
    # 		  tmp->push_back(p[1]);
    # 		  delete[]p[1];
    # 		  p[0] = tmp;
    # 		}
()


def p_udp_sequ_entry_list_2(p):
    '''udp_sequ_entry_list : udp_sequ_entry_list udp_sequ_entry '''
    if(parse_debug):
        print('udp_sequ_entry_list_2', list(p))


    # { list<string>*tmp = p[1];
    # 		  tmp->push_back(p[2]);
    # 		  delete[]p[2];
    # 		  p[0] = tmp;
    # 		}
()


def p_udp_sequ_entry_1(p):
    '''udp_sequ_entry : udp_input_list ':' udp_input_sym ':' udp_output_sym ';' '''
    if(parse_debug):
        print('udp_sequ_entry_1', list(p))


    # { char*tmp = new char[strlen(p[1])+5];
    # 		  strcpy(tmp, p[1]);
    # 		  char*tp = tmp+strlen(tmp);
    # 		  *tp++ = ':';
    # 		  *tp++ = p[3];
    # 		  *tp++ = ':';
    # 		  *tp++ = p[5];
    # 		  *tp++ = 0;
    # 		  p[0] = tmp;
    # 		}
()


def p_udp_initial_1(p):
    '''udp_initial : K_initial IDENTIFIER '=' number ';' '''
    if(parse_debug):
        print('udp_initial_1', list(p))


    # { PExpr*etmp = new PENumber(p[4]);
    # 		  PEIdent*itmp = new PEIdent(lex_strings.make(p[2]));
    # 		  PAssign*atmp = new PAssign(itmp, etmp);
    # 		  FILE_NAME(atmp, @2);
    # 		  delete[]p[2];
    # 		  p[0] = atmp;
    # 		}
()


def p_udp_init_opt_1(p):
    '''udp_init_opt : udp_initial '''
    if(parse_debug):
        print('udp_init_opt_1', list(p))
    p[0] = p[1]


()


def p_udp_init_opt_2(p):
    '''udp_init_opt :  '''
    if(parse_debug):
        print('udp_init_opt_2', list(p))


    # { p[0] = None }
()


def p_udp_input_list_1(p):
    '''udp_input_list : udp_input_sym '''
    if(parse_debug):
        print('udp_input_list_1', list(p))


    # { char*tmp = new char[2];
    # 		  tmp[0] = p[1];
    # 		  tmp[1] = 0;
    # 		  p[0] = tmp;
    # 		}
()


def p_udp_input_list_2(p):
    '''udp_input_list : udp_input_list udp_input_sym '''
    if(parse_debug):
        print('udp_input_list_2', list(p))


    # { char*tmp = new char[strlen(p[1])+2];
    # 		  strcpy(tmp, p[1]);
    # 		  char*tp = tmp+strlen(tmp);
    # 		  *tp++ = p[2];
    # 		  *tp++ = 0;
    # 		  delete[]p[1];
    # 		  p[0] = tmp;
    # 		}
()


def p_udp_input_sym_1(p):
    '''udp_input_sym : '0' '''
    if(parse_debug):
        print('udp_input_sym_1', list(p))


    # { p[0] = '0'; }
()


def p_udp_input_sym_2(p):
    '''udp_input_sym : '1' '''
    if(parse_debug):
        print('udp_input_sym_2', list(p))


    # { p[0] = '1'; }
()


def p_udp_input_sym_3(p):
    '''udp_input_sym : 'x' '''
    if(parse_debug):
        print('udp_input_sym_3', list(p))


    # { p[0] = 'x'; }
()


def p_udp_input_sym_4(p):
    '''udp_input_sym : '?' '''
    if(parse_debug):
        print('udp_input_sym_4', list(p))


    # { p[0] = '?'; }
()


def p_udp_input_sym_5(p):
    '''udp_input_sym : 'b' '''
    if(parse_debug):
        print('udp_input_sym_5', list(p))


    # { p[0] = 'b'; }
()


def p_udp_input_sym_6(p):
    '''udp_input_sym : '*' '''
    if(parse_debug):
        print('udp_input_sym_6', list(p))


    # { p[0] = '*'; }
()


def p_udp_input_sym_7(p):
    '''udp_input_sym : '%' '''
    if(parse_debug):
        print('udp_input_sym_7', list(p))


    # { p[0] = '%'; }
()


def p_udp_input_sym_8(p):
    '''udp_input_sym : 'f' '''
    if(parse_debug):
        print('udp_input_sym_8', list(p))


    # { p[0] = 'f'; }
()


def p_udp_input_sym_9(p):
    '''udp_input_sym : 'F' '''
    if(parse_debug):
        print('udp_input_sym_9', list(p))


    # { p[0] = 'F'; }
()


def p_udp_input_sym_10(p):
    '''udp_input_sym : 'l' '''
    if(parse_debug):
        print('udp_input_sym_10', list(p))


    # { p[0] = 'l'; }
()


def p_udp_input_sym_11(p):
    '''udp_input_sym : 'h' '''
    if(parse_debug):
        print('udp_input_sym_11', list(p))


    # { p[0] = 'h'; }
()


def p_udp_input_sym_12(p):
    '''udp_input_sym : 'B' '''
    if(parse_debug):
        print('udp_input_sym_12', list(p))


    # { p[0] = 'B'; }
()


def p_udp_input_sym_13(p):
    '''udp_input_sym : 'r' '''
    if(parse_debug):
        print('udp_input_sym_13', list(p))


    # { p[0] = 'r'; }
()


def p_udp_input_sym_14(p):
    '''udp_input_sym : 'R' '''
    if(parse_debug):
        print('udp_input_sym_14', list(p))


    # { p[0] = 'R'; }
()


def p_udp_input_sym_15(p):
    '''udp_input_sym : 'M' '''
    if(parse_debug):
        print('udp_input_sym_15', list(p))


    # { p[0] = 'M'; }
()


def p_udp_input_sym_16(p):
    '''udp_input_sym : 'n' '''
    if(parse_debug):
        print('udp_input_sym_16', list(p))


    # { p[0] = 'n'; }
()


def p_udp_input_sym_17(p):
    '''udp_input_sym : 'N' '''
    if(parse_debug):
        print('udp_input_sym_17', list(p))


    # { p[0] = 'N'; }
()


def p_udp_input_sym_18(p):
    '''udp_input_sym : 'p' '''
    if(parse_debug):
        print('udp_input_sym_18', list(p))


    # { p[0] = 'p'; }
()


def p_udp_input_sym_19(p):
    '''udp_input_sym : 'P' '''
    if(parse_debug):
        print('udp_input_sym_19', list(p))


    # { p[0] = 'P'; }
()


def p_udp_input_sym_20(p):
    '''udp_input_sym : 'Q' '''
    if(parse_debug):
        print('udp_input_sym_20', list(p))


    # { p[0] = 'Q'; }
()


def p_udp_input_sym_21(p):
    '''udp_input_sym : 'q' '''
    if(parse_debug):
        print('udp_input_sym_21', list(p))


    # { p[0] = 'q'; }
()


def p_udp_input_sym_22(p):
    '''udp_input_sym : '_' '''
    if(parse_debug):
        print('udp_input_sym_22', list(p))


    # { p[0] = '_'; }
()


def p_udp_input_sym_23(p):
    '''udp_input_sym : '+' '''
    if(parse_debug):
        print('udp_input_sym_23', list(p))


    # { p[0] = '+'; }
()


def p_udp_input_sym_24(p):
    '''udp_input_sym : DEC_NUMBER '''
    if(parse_debug):
        print('udp_input_sym_24', list(p))


    # { yyerror(@1, "internal error: Input digits parse as decimal number!"); p[0] = '0'; }
()


def p_udp_output_sym_1(p):
    '''udp_output_sym : '0' '''
    if(parse_debug):
        print('udp_output_sym_1', list(p))


    # { p[0] = '0'; }
()


def p_udp_output_sym_2(p):
    '''udp_output_sym : '1' '''
    if(parse_debug):
        print('udp_output_sym_2', list(p))


    # { p[0] = '1'; }
()


def p_udp_output_sym_3(p):
    '''udp_output_sym : 'x' '''
    if(parse_debug):
        print('udp_output_sym_3', list(p))


    # { p[0] = 'x'; }
()


def p_udp_output_sym_4(p):
    '''udp_output_sym : '-' '''
    if(parse_debug):
        print('udp_output_sym_4', list(p))


    # { p[0] = '-'; }
()


def p_udp_output_sym_5(p):
    '''udp_output_sym : DEC_NUMBER '''
    if(parse_debug):
        print('udp_output_sym_5', list(p))


    # { yyerror(@1, "internal error: Output digits parse as decimal number!"); p[0] = '0'; }
()


def p_udp_port_decl_1(p):
    '''udp_port_decl : K_input list_of_identifiers ';' '''
    if(parse_debug):
        print('udp_port_decl_1', list(p))


    # { p[0] = pform_make_udp_input_ports(p[2]); }
()


def p_udp_port_decl_2(p):
    '''udp_port_decl : K_output IDENTIFIER ';' '''
    if(parse_debug):
        print('udp_port_decl_2', list(p))


    # { perm_string pname = lex_strings.make(p[2]);
    # 	PWire*pp = new PWire(pname, NetNet::IMPLICIT, NetNet::POUTPUT, IVL_VT_LOGIC);
    # 	vector<PWire*>*tmp = new vector<PWire*>(1);
    # 	(*tmp)[0] = pp;
    # 	p[0] = tmp;
    # 	delete[]p[2];
    #       }
()


def p_udp_port_decl_3(p):
    '''udp_port_decl : K_reg IDENTIFIER ';' '''
    if(parse_debug):
        print('udp_port_decl_3', list(p))


    # { perm_string pname = lex_strings.make(p[2]);
    # 	PWire*pp = new PWire(pname, NetNet::REG, NetNet::PIMPLICIT, IVL_VT_LOGIC);
    # 	vector<PWire*>*tmp = new vector<PWire*>(1);
    # 	(*tmp)[0] = pp;
    # 	p[0] = tmp;
    # 	delete[]p[2];
    #       }
()


def p_udp_port_decl_4(p):
    '''udp_port_decl : K_reg K_output IDENTIFIER ';' '''
    if(parse_debug):
        print('udp_port_decl_4', list(p))


    # { perm_string pname = lex_strings.make(p[3]);
    # 	PWire*pp = new PWire(pname, NetNet::REG, NetNet::POUTPUT, IVL_VT_LOGIC);
    # 	vector<PWire*>*tmp = new vector<PWire*>(1);
    # 	(*tmp)[0] = pp;
    # 	p[0] = tmp;
    # 	delete[]p[3];
    #       }
()


def p_udp_port_decls_1(p):
    '''udp_port_decls : udp_port_decl '''
    if(parse_debug):
        print('udp_port_decls_1', list(p))
    p[0] = p[1]


()


def p_udp_port_decls_2(p):
    '''udp_port_decls : udp_port_decls udp_port_decl '''
    if(parse_debug):
        print('udp_port_decls_2', list(p))


    # { vector<PWire*>*tmp = p[1];
    # 	size_t s1 = p[1]->size();
    # 	tmp->resize(s1+p[2]->size());
    # 	for (size_t idx = 0 ; idx < p[2]->size() ; idx += 1)
    # 	      tmp->at(s1+idx) = p[2]->at(idx);
    # 	p[0] = tmp;
    # 	delete p[2];
    #       }
()


def p_udp_port_list_1(p):
    '''udp_port_list : IDENTIFIER '''
    if(parse_debug):
        print('udp_port_list_1', list(p))


    # { list<perm_string>*tmp = new list<perm_string>;
    # 	tmp->push_back(lex_strings.make(p[1]));
    # 	delete[]p[1];
    # 	p[0] = tmp;
    #       }
()


def p_udp_port_list_2(p):
    '''udp_port_list : udp_port_list ',' IDENTIFIER '''
    if(parse_debug):
        print('udp_port_list_2', list(p))


    # { list<perm_string>*tmp = p[1];
    # 	tmp->push_back(lex_strings.make(p[3]));
    # 	delete[]p[3];
    # 	p[0] = tmp;
    #       }
()


def p_udp_reg_opt_1(p):
    '''udp_reg_opt : K_reg '''
    if(parse_debug):
        print('udp_reg_opt_1', list(p))
    p[0] = True


()


def p_udp_reg_opt_2(p):
    '''udp_reg_opt :  '''
    if(parse_debug):
        print('udp_reg_opt_2', list(p))
    p[0] = False


()


def p_udp_initial_expr_opt_1(p):
    '''udp_initial_expr_opt : '=' expression '''
    if(parse_debug):
        print('udp_initial_expr_opt_1', list(p))
    p[0] = p[2]


()


def p_udp_initial_expr_opt_2(p):
    '''udp_initial_expr_opt :  '''
    if(parse_debug):
        print('udp_initial_expr_opt_2', list(p))


    # { p[0] = None }
()


def p_udp_input_declaration_list_1(p):
    '''udp_input_declaration_list : K_input IDENTIFIER '''
    if(parse_debug):
        print('udp_input_declaration_list_1', list(p))


    # { list<perm_string>*tmp = new list<perm_string>;
    # 		  tmp->push_back(lex_strings.make(p[2]));
    # 		  p[0] = tmp;
    # 		  delete[]p[2];
    # 		}
()


def p_udp_input_declaration_list_2(p):
    '''udp_input_declaration_list : udp_input_declaration_list ',' K_input IDENTIFIER '''
    if(parse_debug):
        print('udp_input_declaration_list_2', list(p))


    # { list<perm_string>*tmp = p[1];
    # 		  tmp->push_back(lex_strings.make(p[4]));
    # 		  p[0] = tmp;
    # 		  delete[]p[4];
    # 		}
()


def p_udp_primitive_1(p):
    '''udp_primitive : K_primitive IDENTIFIER '(' udp_port_list ')' ';' udp_port_decls udp_init_opt udp_body K_endprimitive endlabel_opt '''
    if(parse_debug):
        print('udp_primitive_1', list(p))


    # { perm_string tmp2 = lex_strings.make(p[2]);
    # 		  pform_make_udp(tmp2, p[4], p[7], p[9], p[8],
    # 				 @2.text, @2.first_line);
    # 		  if (p[11]) {
    # 			if (strcmp(p[2],p[11]) != 0) {
    # 			      yyerror(@11, "error: End label doesn't match "
    # 			                   "primitive name");
    # 			}
    # 			if (! gn_system_verilog()) {
    # 			      yyerror(@11, "error: Primitive end labels "
    # 			                   "require SystemVerilog.");
    # 			}
    # 			delete[]p[11];
    # 		  }
    # 		  delete[]p[2];
    # 		}
()


def p_udp_primitive_2(p):
    '''udp_primitive : K_primitive IDENTIFIER '(' K_output udp_reg_opt IDENTIFIER udp_initial_expr_opt ',' udp_input_declaration_list ')' ';' udp_body K_endprimitive endlabel_opt '''
    if(parse_debug):
        print('udp_primitive_2', list(p))


    # { perm_string tmp2 = lex_strings.make(p[2]);
    # 		  perm_string tmp6 = lex_strings.make(p[6]);
    # 		  pform_make_udp(tmp2, p[5], tmp6, p[7], p[9], p[12],
    # 				 @2.text, @2.first_line);
    # 		  if (p[14]) {
    # 			if (strcmp(p[2],p[14]) != 0) {
    # 			      yyerror(@14, "error: End label doesn't match "
    # 			                   "primitive name");
    # 			}
    # 			if (! gn_system_verilog()) {
    # 			      yyerror(@14, "error: Primitive end labels "
    # 			                   "require SystemVerilog.");
    # 			}
    # 			delete[]p[14];
    # 		  }
    # 		  delete[]p[2];
    # 		  delete[]p[6];
    # 		}
()


def p_K_packed_opt_1(p):
    '''K_packed_opt : K_packed '''
    if(parse_debug):
        print('K_packed_opt', list(p))
    p[0] = True


()


def p_K_packed_opt_2(p):
    '''K_packed_opt :  '''
    if(parse_debug):
        print('K_packed_opt', list(p))
    p[0] = False


()


def p_K_reg_opt_1(p):
    '''K_reg_opt : K_reg '''
    if(parse_debug):
        print('K_reg_opt', list(p))
    p[0] = True


()


def p_K_reg_opt_2(p):
    '''K_reg_opt :  '''
    if(parse_debug):
        print('K_reg_opt', list(p))
    p[0] = False


()


def p_K_static_opt_1(p):
    '''K_static_opt : K_static '''
    if(parse_debug):
        print('K_static_opt', list(p))
    p[0] = True


()


def p_K_static_opt_2(p):
    '''K_static_opt :  '''
    if(parse_debug):
        print('K_static_opt', list(p))
    p[0] = False


()


def p_K_virtual_opt_1(p):
    '''K_virtual_opt : K_virtual '''
    if(parse_debug):
        print(p)
    p[0] = True


()


def p_K_virtual_opt_2(p):
    '''K_virtual_opt :  '''
    if(parse_debug):
        print(p)
    p[0] = False


()


def p_error(p):
    if(parse_debug):
        print("error", p)
    exit(0)


yacc.yacc(debug=0)
