module assignment(
    output o,
    input i
);
wire x,y;
wire [15:0] z;
assign x = i;
assign o = x;

assign plusexp = x + y;
assign minusexp = x - y;
assign multexp = x * y;
assign divexp = x / y;
assign modexp = x % y;

assign gtexp = x > y;
assign ltexp = x < y;
assign geexp = x >= y;
assign leexp = x <= y;

assign eq_short = x == y;
assign neq_short = x != y;
assign eq_long = x === y;
assign neq_long = x !== y;


assign and2 = x && y;
assign or2 = x || y;

assign andexp = x & y;
assign orexp = x | y;
assign nandexp = x ~& y;
assign norexp = x ~| y;

assign xorexp = x ^ y;
endmodule
