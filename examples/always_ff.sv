module always_comb_test(
    input clk,
    input a,
    output b
);

always_ff @(posedge clk) begin
  a <= b;
end
  
endmodule
