import sys
quote = "\""


class Preprocessor:
    def __init__(self):
        self.docstrings = []

    def removeComments(self, data):
        # print(data)
        ret = ""
        in_docstring = False
        docstring = ""
        for line in data.split("\n"):
            docstring_end = ("#docstring_end" in line)
            if "#docstring_begin" in line:
                ret += "//DOCSTRING_PLACEHOLDER\n"
                in_docstring = True
                docstring = ""
            if(in_docstring == False):
                ret += line + "\n"
            else:
                if(not docstring_end):
                    docstring += line + "\n"
            if(docstring_end):
                in_docstring = False
                self.docstrings += [docstring]
        return ret

    def insertDocstrings(self, data):
        ret = ""
        docstring_counter = 0
        for line in data.split("\n"):
            if("//DOCSTRING_PLACEHOLDER" in line):
                ret += quote*3
                ret += self.docstrings[docstring_counter]
                ret += quote*3
                ret += "\n"
                docstring_counter += 1
            else:
                ret += "#"+line + "\n"
        return ret
